/**
 * 
 */
package ar.edu.unju.fi.tpfinal.app;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.apache.log4j.Logger;

import ar.edu.unju.fi.tpfinal.exepciones.CurrencyException;

/**
 * The Class CurrencyApi.
 *
 * @author TEAM 4
 */
public class CurrencyApi {

	/** The log. */
	private static Logger log = Logger.getLogger(CurrencyApi.class);

	/** The Constant CURRENCY_API_URL. */
	private static final String CURRENCY_API_URL = "http://currencies.apps.grandtrunk.net/getlatest/";

	/** The Constant DOLAR_TO_PESOS. */
	private static final String DOLAR_TO_PESOS = "USD/ARS";

	/** The client. */
	private static HttpClient client = HttpClient.newHttpClient();

	/**
	 * Convierte un importe de pesos a dolares.
	 *
	 * @param importe en pesos
	 * @return importe en dolares
	 * @throws CurrencyException the currency exception
	 */
	public static BigDecimal convertirDolarAPesos(BigDecimal importe) throws CurrencyException {
		log.info("Se inicia el cálculo para el paso de dolares a pesos");
		BigDecimal dolar = CurrencyApi.getEquivalenciaDesdeWebService();
		log.debug(dolar);
		return dolar.multiply(importe, MathContext.DECIMAL128);
	}

	/**
	 * Obtiene desde el servidor la diferencia de dolares y pesos.
	 *
	 * @return the equivalencia desde web service
	 * @throws CurrencyException the currency exception
	 */
	public static BigDecimal getEquivalenciaDesdeWebService() throws CurrencyException {
		log.info("Obteniendo equivalencia de la moneda.");
		HttpResponse<String> response;
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(CURRENCY_API_URL + DOLAR_TO_PESOS)).build();
		try {
			response = client.send(request, BodyHandlers.ofString());
		} catch (IOException | InterruptedException e) {
			throw new CurrencyException();
		}
		log.debug(response.body());
		return new BigDecimal(response.body());
	}
}
