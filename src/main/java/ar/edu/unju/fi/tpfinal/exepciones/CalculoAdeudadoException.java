package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class CalculoAdeudadoException.
 */
public class CalculoAdeudadoException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 348747845L;

	/**
	 * Instantiates a new calculo adeudado exception.
	 */
	public CalculoAdeudadoException() {
		super("No se pudo calcular el importe de la venta selecionada");
	}
}
