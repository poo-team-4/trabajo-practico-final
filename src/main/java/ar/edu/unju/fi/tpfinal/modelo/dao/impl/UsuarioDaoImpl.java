package ar.edu.unju.fi.tpfinal.modelo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpfinal.app.JPAUtil;
import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dao.UsuarioDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Usuario;

/**
 * The Class UsuarioDaoImpl.
 */
@Repository
public class UsuarioDaoImpl implements UsuarioDao {

	/** The log. */
	private static Logger log = Logger.getLogger(UsuarioDaoImpl.class);

	/** The entity. */
	private EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	/**
	 * Gets the.
	 *
	 * @return una lista de usuarios.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public List<Usuario> get() throws ObtenerException, Exception {
		log.info("Iniciando la recolección de una lista de Usuarios.");
		List<Usuario> usuarios = new ArrayList<Usuario>();
		CriteriaQuery<Usuario> criteria = entity.getCriteriaBuilder().createQuery(Usuario.class);
		criteria.select(criteria.from(Usuario.class));

		try {
			usuarios = entity.createQuery(criteria).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}

		log.debug(usuarios);
		return usuarios;
	}

	/**
	 * Gets the.
	 *
	 * @param usuario the usuario
	 * @return un usuario si existe.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Usuario get(Usuario usuario) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de un Usuario.");
		Usuario usuarioEncontrado = entity.find(Usuario.class, usuario.getId());

		if (usuarioEncontrado == null) {
			throw new ObtenerException();
		}

		log.debug(usuarioEncontrado);
		return usuarioEncontrado;
	}

	/**
	 * Gets the.
	 *
	 * @param nombreUsuario the nombre usuario
	 * @return un Usuario si existe.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Usuario get(String nombreUsuario) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de un Usuario por su nombre: " + nombreUsuario);
		Usuario usuarioEncontrado = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteriaQuery = criteriaBuilder.createQuery(Usuario.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Usuario.class).get("user"), nombreUsuario));

		try {
			usuarioEncontrado = entity.createQuery(criteriaQuery).getSingleResult();

		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(usuarioEncontrado);
		return usuarioEncontrado;
	}

	/**
	 * Insert.
	 *
	 * @param usuario the usuario
	 * @return the usuario
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	public Usuario insert(Usuario usuario) throws InsertarException, Exception {
		log.info("Añadiendo un nuevo Usuario a la base de datos.");
		log.debug(usuario);
		entity.getTransaction().begin();
		try {
			entity.persist(usuario);
			entity.flush();
			log.info("Usuario añadido con éxito.");
		} catch (Exception e) {
			log.error("No se pudo agregar el Usuario a la base de datos: " + e.getMessage());
			throw new InsertarException(e.getMessage());
		}
		entity.getTransaction().commit();
		return usuario;
	}

	/**
	 * Update.
	 *
	 * @param usuario the usuario
	 * @return the usuario
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	public Usuario update(Usuario usuario) throws UpdateException, Exception {
		log.info("Actualizando un Usuario.");
		log.debug(usuario);
		entity.getTransaction().begin();
		try {
			entity.merge(usuario);
			entity.flush();
			log.info("Usuario actualizado con éxito.");
		} catch (Exception e) {
			log.error("No se pudo actualizar el Usuario en la base de datos: " + e.getMessage());
			throw new UpdateException(e.getMessage());
		}
		entity.getTransaction().commit();
		return usuario;
	}

	/**
	 * Delete.
	 *
	 * @param usuario the usuario
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	public void delete(Usuario usuario) throws DeleteException, Exception {
		log.info("Eliminando un Usuario.");
		log.debug(usuario);
		entity.getTransaction().begin();
		try {
			entity.remove(
					entity.contains(usuario) ? usuario : entity.getReference(usuario.getClass(), usuario.getId()));
			log.info("Usuario eliminado con éxito.");
		} catch (Exception e) {
			log.error("No se pudo eliminar el Usuario de la base de datos: " + e.getMessage());
			throw new DeleteException(e.getMessage());
		}
		entity.getTransaction().commit();
	}

}
