package ar.edu.unju.fi.tpfinal.util;

/**
 * The Enum TipoInstrumento.
 */
public enum TipoInstrumento {

	/** The Guitarra. */
	Guitarra,
	/** The Bajo. */
	Bajo,
	/** The Violin. */
	Violin,
	/** The Teclado. */
	Teclado,
	/** The Bateria. */
	Bateria,
	/** The Charango. */
	Charango,
	/** The Flauta. */
	Flauta;
}
