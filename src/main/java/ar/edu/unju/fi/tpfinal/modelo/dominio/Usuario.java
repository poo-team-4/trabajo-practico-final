package ar.edu.unju.fi.tpfinal.modelo.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.edu.unju.fi.tpfinal.util.EstadoUsuario;

/**
 * The Class Usuario.
 */
@Entity
@Table(name = "usuarios")
public class Usuario {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer id;

	/** The user. */
	@Column(name = "\"user\"")
	private String user;

	/** The password. */
	@Column(name = "\"password\"")
	private String password;

	/** The nombre apellido. */
	@Column(name = "nombre_apellido")
	private String nombreApellido;

	/** The email. */
	@Column(name = "e_mail")
	private String email;

	/** The estado. */
	@Enumerated(EnumType.STRING)
	@Column(length = 8)
	private EstadoUsuario estado;

	/** The rol. */
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "rol_id")
	private Rol rol;

	/**
	 * Instantiates a new usuario.
	 */
	public Usuario() {

	}

	/**
	 * Instantiates a new usuario.
	 *
	 * @param user           the user
	 * @param password       the password
	 * @param nombreApellido the nombre apellido
	 * @param email          the email
	 * @param estado         the estado
	 * @param rol            the rol
	 */
	public Usuario(String user, String password, String nombreApellido, String email, EstadoUsuario estado, Rol rol) {
		this.user = user;
		this.password = password;
		this.nombreApellido = nombreApellido;
		this.email = email;
		this.estado = estado;
		this.rol = rol;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the nombre apellido.
	 *
	 * @return the nombreApellido
	 */
	public String getNombreApellido() {
		return nombreApellido;
	}

	/**
	 * Sets the nombre apellido.
	 *
	 * @param nombreApellido the nombreApellido to set
	 */
	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public EstadoUsuario getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoUsuario estado) {
		this.estado = estado;
	}

	/**
	 * Gets the rol.
	 *
	 * @return the rol
	 */
	public Rol getRol() {
		return rol;
	}

	/**
	 * Sets the rol.
	 *
	 * @param rol the rol to set
	 */
	public void setRol(Rol rol) {
		this.rol = rol;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", user=" + user + ", password=" + password + ", nombreApellido=" + nombreApellido
				+ ", email=" + email + ", estado=" + estado + ", rol=" + rol + "]";
	}

}
