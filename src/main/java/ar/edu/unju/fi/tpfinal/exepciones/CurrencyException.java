/**
 * 
 */
package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class CurrencyException.
 *
 * @author TEAM 4
 */
public class CurrencyException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4679139691813883067L;

	/**
	 * Instantiates a new currency exception.
	 */
	public CurrencyException() {
		super("No se pudo conectar al servidor para obtener la equivalencia de moneda.");
	}

}
