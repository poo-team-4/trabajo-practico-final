/**
 * 
 */
package ar.edu.unju.fi.tpfinal.modelo.factory.impl;

import java.util.List;

import javax.inject.Inject;

import ar.edu.unju.fi.tpfinal.modelo.dto.VentaInformeDTO;
import ar.edu.unju.fi.tpfinal.util.ObjectMapperUtils;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpfinal.exepciones.InformeException;
import ar.edu.unju.fi.tpfinal.modelo.dao.VentaDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;
import ar.edu.unju.fi.tpfinal.modelo.factory.FabricaInformes;
import ar.edu.unju.fi.tpfinal.modelo.factory.InformeExcel;

/**
 * The Class FabricaInformesImpl.
 *
 * @author TEAM 4
 */
@Service
public class FabricaInformesImpl implements FabricaInformes {

	/** The venta dao. */
	@Inject
	private VentaDao ventaDao;

	/**
	 * Exportar venta excel.
	 *
	 * @param ventaId the venta id
	 * @throws InformeException the informe exception
	 */
	@Override
	public void exportarVentaExcel(Integer ventaId) throws InformeException {
		InformeExcel informe = new InformeExcelImpl();
		try {
			informe.generarExcel(ObjectMapperUtils.mapConFormato(ventaDao.get(ventaId), new VentaInformeDTO()));
			informe.guardarEnDisco();
		} catch (Exception e) {
			e.printStackTrace();
			throw new InformeException();
		}
	}

	/**
	 * Exportar venta PDF.
	 *
	 * @param venta the venta
	 * @throws InformeException the informe exception
	 */
	@Override
	public void exportarVentaPDF(VentaDTO venta) throws InformeException {
		InformePDFImpl informeFactory = new InformePDFImpl();
		try {
			informeFactory.generarPDF(ventaDao.get(venta.getId()));
		} catch (Exception e) {
			throw new InformeException();
		}
	}

	/**
	 * Exportar ventas no canceladas en excel.
	 *
	 * @throws InformeException the informe exception
	 */
	@Override
	public void exportarVentasAdeudadasEnExcel() throws InformeException {
		List<Venta> ventasAdeudadas = null;
		InformeExcel informe = new InformeExcelImpl();
		try {
			ventasAdeudadas = ventaDao.getAdeudadas();
		} catch (Exception e) {
			throw new InformeException(e.getMessage());
		}
		for (Venta venta : ventasAdeudadas) {
			informe.generarExcel(ObjectMapperUtils.mapConFormato(venta, new VentaInformeDTO()));
		}
		informe.guardarEnDisco();
	}

}
