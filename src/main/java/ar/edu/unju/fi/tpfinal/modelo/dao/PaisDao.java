package ar.edu.unju.fi.tpfinal.modelo.dao;

import java.util.List;

import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Pais;

/**
 * The Interface PaisDao.
 *
 * @author TEAM 4
 */
public interface PaisDao {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Pais> get() throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param pais the pais
	 * @return the pais
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Pais get(Pais pais) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param nombrePais the nombre pais
	 * @return the pais
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Pais get(String nombrePais) throws ObtenerException, Exception;

	/**
	 * Insert.
	 *
	 * @param pais the pais
	 * @return the pais
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	Pais insert(Pais pais) throws InsertarException, Exception;

	/**
	 * Update.
	 *
	 * @param pais the pais
	 * @return the pais
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	Pais update(Pais pais) throws UpdateException, Exception;

	/**
	 * Delete.
	 *
	 * @param pais the pais
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	void delete(Pais pais) throws DeleteException, Exception;

}
