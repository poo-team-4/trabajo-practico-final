package ar.edu.unju.fi.tpfinal.modelo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpfinal.app.JPAUtil;
import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dao.RolDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Rol;

/**
 * The Class RolDaoImpl.
 */
@Repository
public class RolDaoImpl implements RolDao {

	/** The log. */
	private static Logger log = Logger.getLogger(RolDaoImpl.class);

	/** The entity. */
	private EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	/**
	 * Gets the.
	 *
	 * @return una lista de roles.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public List<Rol> get() throws ObtenerException, Exception {
		log.info("Iniciando la recolección de una lista de Roles de Usuarios.");
		List<Rol> roles = new ArrayList<Rol>();
		CriteriaQuery<Rol> criteria = entity.getCriteriaBuilder().createQuery(Rol.class);
		criteria.select(criteria.from(Rol.class));
		try {
			roles = entity.createQuery(criteria).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}

		log.debug(roles);
		return roles;
	}

	/**
	 * Gets the.
	 *
	 * @param rol the rol
	 * @return an Rol si existe.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Rol get(Rol rol) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de un Rol.");
		Rol rolEncontrado = entity.find(Rol.class, rol.getId());
		if (rolEncontrado == null) {
			throw new ObtenerException();
		}

		log.debug(rolEncontrado);
		return rolEncontrado;
	}

	/**
	 * Gets the.
	 *
	 * @param nombreRol the nombre rol
	 * @return an Rol si existe.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Rol get(String nombreRol) throws ObtenerException, Exception {
		log.info("Iniciando la busqueda de una Rol por su nombre: " + nombreRol);
		Rol rolEncontrado = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Rol> criteriaQuery = criteriaBuilder.createQuery(Rol.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Rol.class).get("descripcion"), nombreRol));

		try {
			rolEncontrado = entity.createQuery(criteriaQuery).getSingleResult();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}

		log.debug(rolEncontrado);
		return rolEncontrado;
	}

	/**
	 * Insert.
	 *
	 * @param rol the rol
	 * @return the rol
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	public Rol insert(Rol rol) throws InsertarException, Exception {
		log.info("Añadiendo un nuevo Rol a la base de datos.");
		log.debug(rol);
		entity.getTransaction().begin();
		try {
			entity.persist(rol);
			entity.flush();
			log.info("Rol añadido con éxito.");
		} catch (Exception e) {
			log.error("No se pudo agregar el Rol a la base de datos: " + e.getMessage());
			throw new InsertarException(e.getMessage());
		}
		entity.getTransaction().commit();
		return rol;
	}

	/**
	 * Update.
	 *
	 * @param rol the rol
	 * @return the rol
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	public Rol update(Rol rol) throws UpdateException, Exception {
		log.info("Actualizando un Rol.");
		log.debug(rol);
		entity.getTransaction().begin();
		try {
			entity.merge(rol);
			entity.flush();
			log.info("Rol actualizado con exito.");
		} catch (Exception e) {
			log.error("No se pudo actualizar el Rol en la base de datos: " + e.getMessage());
			throw new UpdateException(e.getMessage());
		}
		entity.getTransaction().commit();
		return rol;
	}

	/**
	 * Delete.
	 *
	 * @param rol the rol
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	public void delete(Rol rol) throws DeleteException, Exception {
		log.info("Eliminando un Rol.");
		log.debug(rol);
		entity.getTransaction().begin();
		try {
			entity.remove(rol);
			log.info("Rol eliminado con éxito.");
		} catch (Exception e) {
			log.error("No se pudo eliminar el Rol de la base de datos: " + e.getMessage());
			throw new DeleteException(e.getMessage());
		}
		entity.getTransaction().commit();
	}

}
