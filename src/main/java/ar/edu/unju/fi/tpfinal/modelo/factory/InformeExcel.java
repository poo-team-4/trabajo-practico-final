package ar.edu.unju.fi.tpfinal.modelo.factory;

import ar.edu.unju.fi.tpfinal.exepciones.InformeException;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaInformeDTO;

/**
 * The Interface InformeExcel.
 */
public interface InformeExcel {

	/**
	 * Generar excel.
	 *
	 * @param venta the venta
	 * @throws InformeException the informe exception
	 */
	public void generarExcel(VentaInformeDTO venta) throws InformeException;

	/**
	 * Guardar en disco.
	 *
	 * @throws InformeException the informe exception
	 */
	void guardarEnDisco() throws InformeException;

}
