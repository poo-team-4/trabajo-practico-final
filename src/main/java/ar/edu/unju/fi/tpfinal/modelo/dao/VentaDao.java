package ar.edu.unju.fi.tpfinal.modelo.dao;

import java.math.BigDecimal;
import java.util.List;

import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Usuario;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;

/**
 * The Interface VentaDao.
 */
public interface VentaDao {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Venta> get() throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param id the id
	 * @return the venta
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Venta get(Integer id) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param venta the venta
	 * @return the venta
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Venta get(Venta venta) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param cliente the cliente
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Venta> get(Usuario cliente) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param instrumento the instrumento
	 * @return the venta
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Venta get(Instrumento instrumento) throws ObtenerException, Exception;

	/**
	 * Gets the adeudadas.
	 *
	 * @return the adeudadas
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Venta> getAdeudadas() throws ObtenerException, Exception;

	/**
	 * Calcular adeudado de una venta.
	 *
	 * @param idVenta the id venta
	 * @return the big decimal
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	BigDecimal calcularAdeudadoDeUnaVenta(Integer idVenta) throws ObtenerException, Exception;

	/**
	 * Calcular adeudado de todas las ventas.
	 *
	 * @return the big decimal
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	BigDecimal calcularAdeudadoDeTodasLasVentas() throws ObtenerException, Exception;

	/**
	 * Insert.
	 *
	 * @param venta the venta
	 * @return the venta
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	Venta insert(Venta venta) throws InsertarException, Exception;

	/**
	 * Update.
	 *
	 * @param venta the venta
	 * @return the venta
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	Venta update(Venta venta) throws UpdateException, Exception;

	/**
	 * Delete.
	 *
	 * @param venta the venta
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	void delete(Venta venta) throws DeleteException, Exception;

}
