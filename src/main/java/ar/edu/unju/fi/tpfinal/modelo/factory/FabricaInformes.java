package ar.edu.unju.fi.tpfinal.modelo.factory;

import ar.edu.unju.fi.tpfinal.exepciones.InformeException;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;

/**
 * The Interface FabricaInformes.
 *
 * @author TEAM 4
 */
public interface FabricaInformes {

	/**
	 * Exportar venta excel.
	 *
	 * @param ventaId the venta id
	 * @throws InformeException the informe exception
	 */
	public void exportarVentaExcel(Integer ventaId) throws InformeException;

	/**
	 * Exportar venta PDF.
	 *
	 * @param venta the venta
	 * @throws InformeException the informe exception
	 */
	public void exportarVentaPDF(VentaDTO venta) throws InformeException;

	/**
	 * Exportar ventas no canceladas en excel.
	 *
	 * @throws InformeException the informe exception
	 */
	public void exportarVentasAdeudadasEnExcel() throws InformeException;

}
