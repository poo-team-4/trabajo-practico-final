package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class ArchivoException.
 */
public class ArchivoException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 734874853948L;

	/**
	 * Instantiates a new archivo exception.
	 */
	public ArchivoException() {
		super("No se encontró el archivo de configuración");
	}
}
