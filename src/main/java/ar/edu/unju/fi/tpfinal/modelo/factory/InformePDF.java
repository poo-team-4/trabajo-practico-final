package ar.edu.unju.fi.tpfinal.modelo.factory;

import ar.edu.unju.fi.tpfinal.exepciones.InformeException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;

/**
 * The Interface InformePDF.
 */
public interface InformePDF {

	/**
	 * Generar PDF.
	 *
	 * @param venta the venta
	 * @throws InformeException the informe exception
	 */
	public void generarPDF(Venta venta) throws InformeException;

}
