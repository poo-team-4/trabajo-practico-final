/**
 * 
 */
package ar.edu.unju.fi.tpfinal.app;

import java.io.InputStream;
import java.util.Properties;

import ar.edu.unju.fi.tpfinal.exepciones.ArchivoException;
import ar.edu.unju.fi.tpfinal.exepciones.DescuentoException;
import ar.edu.unju.fi.tpfinal.exepciones.InteresCuotaException;

/**
 * The Class AppSetting.
 *
 * @author TEAM 4
 */
public class AppSetting {

	/** The instance. */
	private static AppSetting instance;

	/** The prop. */
	private static Properties prop = new Properties();

	/** The Constant CONFIG_FILE. */
	private static final String CONFIG_FILE = "configurationSystem.cfg";

	/**
	 * Se carga el archivo de configuración en memoria.
	 *
	 * @throws ArchivoException the archivo exception
	 */
	private AppSetting() throws ArchivoException {
		try (InputStream input = AppSetting.class.getClassLoader().getResourceAsStream(CONFIG_FILE)) {
			if (input == null) {
				throw new ArchivoException();
			}
			prop.load(input);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Obtiene una instancia del AppSetting.
	 *
	 * @return AppSetting Instance
	 * @throws Exception the exception
	 */
	public static AppSetting getInstance() throws Exception {
		if (instance == null)
			instance = new AppSetting();
		return instance;
	}

	/**
	 * Se obtiene el valor del descuento.
	 *
	 * @return Double
	 * @throws DescuentoException the descuento exception
	 */
	public Double getDescuentoContado() throws DescuentoException {
		Double descuento = null;
		try {
			descuento = Double.valueOf(prop.getProperty("descuento_contado"));
		} catch (Exception e) {
		}
		if (descuento == null)
			throw new DescuentoException();
		return descuento;
	}

	/**
	 * Se obtiene el interés según la cantidad de cuotas.
	 *
	 * @param cuota the cuota
	 * @return Double
	 * @throws InteresCuotaException the interes cuota exception
	 */
	public Double getInteresPorCuota(Integer cuota) throws InteresCuotaException {
		Double interes = null;
		try {
			interes = Double.valueOf(prop.getProperty("interes_" + cuota.toString() + "_cuotas"));
		} catch (Exception e) {
			throw new InteresCuotaException();

		}

		if (interes == null)
			throw new InteresCuotaException();

		return interes;
	}

}
