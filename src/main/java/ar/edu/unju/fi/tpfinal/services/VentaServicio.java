package ar.edu.unju.fi.tpfinal.services;

import java.math.BigDecimal;
import java.util.List;

import ar.edu.unju.fi.tpfinal.modelo.dto.InstrumentoDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.UsuarioDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;

/**
 * The Interface VentaServicio.
 */
public interface VentaServicio {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	List<VentaDTO> get() throws Exception;

	/**
	 * Gets the.
	 *
	 * @param ventaId the venta id
	 * @return the venta DTO
	 * @throws Exception the exception
	 */
	VentaDTO get(Integer ventaId) throws Exception;

	/**
	 * Gets the.
	 *
	 * @param instrumento the instrumento
	 * @return the venta DTO
	 * @throws Exception the exception
	 */
	VentaDTO get(InstrumentoDTO instrumento) throws Exception;

	/**
	 * Gets the.
	 *
	 * @param usuario the usuario
	 * @return the list
	 * @throws Exception the exception
	 */
	List<VentaDTO> get(UsuarioDTO usuario) throws Exception;

	/**
	 * Gets the deuda de una venta.
	 *
	 * @param ventaId the venta id
	 * @return the deuda de una venta
	 * @throws Exception the exception
	 */
	BigDecimal getDeudaDeUnaVenta(Integer ventaId) throws Exception;

	/**
	 * Gets the deuda total.
	 *
	 * @return the deuda total
	 * @throws Exception the exception
	 */
	BigDecimal getDeudaTotal() throws Exception;

	/**
	 * Insert.
	 *
	 * @param venta the venta
	 * @return the venta DTO
	 * @throws Exception the exception
	 */
	VentaDTO insert(VentaDTO venta) throws Exception;

	/**
	 * Update.
	 *
	 * @param venta the venta
	 * @return the venta DTO
	 * @throws Exception the exception
	 */
	VentaDTO update(VentaDTO venta) throws Exception;

	/**
	 * Delete.
	 *
	 * @param venta the venta
	 * @throws Exception the exception
	 */
	void delete(VentaDTO venta) throws Exception;

}
