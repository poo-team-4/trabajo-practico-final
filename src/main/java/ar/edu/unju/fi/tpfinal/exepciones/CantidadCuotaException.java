package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class CantidadCuotaException.
 */
public class CantidadCuotaException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 738436465738L;

	/**
	 * Instantiates a new cantidad cuota exception.
	 */
	public CantidadCuotaException() {
		super("Error: se requiere una cantidad de cuotas mayor o igual a uno (1). ");
	}

}
