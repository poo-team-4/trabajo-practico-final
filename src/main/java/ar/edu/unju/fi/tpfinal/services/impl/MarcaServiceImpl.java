/**
 * 
 */
package ar.edu.unju.fi.tpfinal.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpfinal.modelo.dao.MarcaDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Marca;
import ar.edu.unju.fi.tpfinal.modelo.dto.MarcaDTO;
import ar.edu.unju.fi.tpfinal.services.MarcaService;
import ar.edu.unju.fi.tpfinal.util.ObjectMapperUtils;

/**
 * The Class MarcaServiceImpl.
 *
 * @author TEAM 4
 */

@Service
public class MarcaServiceImpl implements MarcaService {

	/** The marca dao. */
	@Inject
	private MarcaDao marcaDao;

	/** The mapper. */
	private ModelMapper mapper = new ModelMapper();

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<MarcaDTO> get() throws Exception {
		return ObjectMapperUtils.mapAll(marcaDao.get(), MarcaDTO.class);
	}

	/**
	 * Gets the.
	 *
	 * @param nombre the nombre
	 * @return the marca DTO
	 * @throws Exception the exception
	 */
	public MarcaDTO get(String nombre) throws Exception {
		Marca unaMarca = marcaDao.get(nombre);
		MarcaDTO marcaDTO = new MarcaDTO();
		mapper.map(unaMarca, marcaDTO);
		return marcaDTO;
	}

	/**
	 * Insert.
	 *
	 * @param marca the marca
	 * @return the marca DTO
	 * @throws Exception the exception
	 */
	public MarcaDTO insert(MarcaDTO marca) throws Exception {
		Marca unaMarca = new Marca();
		mapper.map(marca, unaMarca);
		marcaDao.insert(unaMarca);
		return marca;
	}

	/**
	 * Update.
	 *
	 * @param marca the marca
	 * @return the marca DTO
	 * @throws Exception the exception
	 */
	public MarcaDTO update(MarcaDTO marca) throws Exception {
		Marca unaMarca = new Marca();
		mapper.map(marca, unaMarca);
		marcaDao.update(unaMarca);
		return marca;
	}

	/**
	 * Delete.
	 *
	 * @param marca the marca
	 * @return the marca DTO
	 * @throws Exception the exception
	 */
	public MarcaDTO delete(MarcaDTO marca) throws Exception {
		Marca unaMarca = new Marca();
		mapper.map(marca, unaMarca);
		marcaDao.delete(unaMarca);
		return marca;
	}

}
