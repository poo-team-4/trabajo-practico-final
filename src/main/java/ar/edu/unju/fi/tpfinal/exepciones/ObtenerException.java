package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class ObtenerException.
 */
public class ObtenerException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 48594033283L;

	/**
	 * Instantiates a new obtener exception.
	 *
	 * @param mensaje the mensaje
	 */
	public ObtenerException(String mensaje) {
		super("no se pudo  obtener el objeto de la base de datos" + mensaje);
	}

	/**
	 * Instantiates a new obtener exception.
	 */
	public ObtenerException() {
		super("no se pudo  obtener el objeto de la base de datos");
	}

}
