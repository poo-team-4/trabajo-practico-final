package ar.edu.unju.fi.tpfinal.modelo.dao;

import java.util.List;

import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;

/**
 * The Interface CuotaDao.
 */
public interface CuotaDao {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Cuota> get() throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param cuota the cuota
	 * @return the cuota
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Cuota get(Cuota cuota) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param numeroDeCuota the numero de cuota
	 * @return the cuota
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Cuota get(Integer numeroDeCuota) throws ObtenerException, Exception;

	/**
	 * Gets the by venta.
	 *
	 * @param idVenta the id venta
	 * @return the by venta
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Cuota> getByVenta(Integer idVenta) throws ObtenerException, Exception;

	/**
	 * Gets the cuotas adeudadas.
	 *
	 * @param venta the venta
	 * @return the cuotas adeudadas
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Cuota> getCuotasAdeudadas(Venta venta) throws ObtenerException, Exception;

	/**
	 * Insert.
	 *
	 * @param cuota the cuota
	 * @return the cuota
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	Cuota insert(Cuota cuota) throws InsertarException, Exception;

	/**
	 * Update.
	 *
	 * @param cuota the cuota
	 * @return the cuota
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	Cuota update(Cuota cuota) throws UpdateException, Exception;

	/**
	 * Delete.
	 *
	 * @param cuota the cuota
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	void delete(Cuota cuota) throws DeleteException, Exception;

}
