/**
 * 
 */
package ar.edu.unju.fi.tpfinal.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpfinal.modelo.dao.InstrumentoDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.MarcaDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.PaisDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;
import ar.edu.unju.fi.tpfinal.modelo.dto.InstrumentoDTO;
import ar.edu.unju.fi.tpfinal.services.InstrumentoService;
import ar.edu.unju.fi.tpfinal.util.ObjectMapperUtils;

/**
 * The Class InstrumentoServiceImpl.
 *
 * @author TEAM 4
 */

@Service
public class InstrumentoServiceImpl implements InstrumentoService {

	/** The instrumento dao. */
	@Inject
	private InstrumentoDao instrumentoDao;

	/** The pais dao. */
	@Inject
	private PaisDao paisDao;

	/** The marca dao. */
	@Inject
	private MarcaDao marcaDao;

	/** The mapper. */
	private ModelMapper mapper = new ModelMapper();

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<InstrumentoDTO> get() throws Exception {
		return ObjectMapperUtils.mapAll(instrumentoDao.get(), InstrumentoDTO.class);
	}

	/**
	 * Gets the.
	 *
	 * @param numeroSerie the numero serie
	 * @return the instrumento DTO
	 * @throws Exception the exception
	 */
	public InstrumentoDTO get(String numeroSerie) throws Exception {
		Instrumento unInstrumento = instrumentoDao.get(numeroSerie);
		InstrumentoDTO instrumentoDTO = new InstrumentoDTO();
		mapper.map(unInstrumento, instrumentoDTO);
		return instrumentoDTO;
	}

	/**
	 * Insert.
	 *
	 * @param nuevoInstrumento the nuevo instrumento
	 * @return the instrumento DTO
	 * @throws Exception the exception
	 */
	public InstrumentoDTO insert(InstrumentoDTO nuevoInstrumento) throws Exception {
		Instrumento unInstrumento = new Instrumento();
		mapper.map(nuevoInstrumento, unInstrumento);

		unInstrumento.setPais(paisDao.get(nuevoInstrumento.getPaisNombre()));
		unInstrumento.setMarca(marcaDao.get(nuevoInstrumento.getMarcaNombre()));

		instrumentoDao.insert(unInstrumento);
		return nuevoInstrumento;
	}

	/**
	 * Update.
	 *
	 * @param instrumentoModificado the instrumento modificado
	 * @return the instrumento DTO
	 * @throws Exception the exception
	 */
	public InstrumentoDTO update(InstrumentoDTO instrumentoModificado) throws Exception {
		Instrumento unInstrumento = new Instrumento();
		mapper.map(instrumentoModificado, unInstrumento);

		if (instrumentoModificado.getPaisNombre() != null && !instrumentoModificado.getPaisNombre().isEmpty())
			unInstrumento.setPais(paisDao.get(instrumentoModificado.getPaisNombre()));

		if (instrumentoModificado.getMarcaNombre() != null && !instrumentoModificado.getMarcaNombre().isEmpty())
			unInstrumento.setMarca(marcaDao.get(instrumentoModificado.getMarcaNombre()));

		instrumentoDao.update(unInstrumento);
		return instrumentoModificado;
	}

	/**
	 * Delete.
	 *
	 * @param instrumentoABorrar the instrumento A borrar
	 * @throws Exception the exception
	 */
	public void delete(InstrumentoDTO instrumentoABorrar) throws Exception {
		Instrumento unInstrumento = new Instrumento();
		mapper.map(instrumentoABorrar, unInstrumento);
		instrumentoDao.delete(unInstrumento);
	}

}
