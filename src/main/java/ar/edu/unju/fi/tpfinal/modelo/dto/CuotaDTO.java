package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ar.edu.unju.fi.tpfinal.util.EstadoCuota;

/**
 * The Class CuotaDTO.
 */
public class CuotaDTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The venta id. */
	private Integer ventaId;

	/** The numero cuota. */
	private Integer numeroCuota;

	/** The importe. */
	private BigDecimal importe;

	/** The fecha pago. */
	private Date fechaPago;

	/** The fecha vencimiento. */
	private Date fechaVencimiento;

	/** The estado. */
	private EstadoCuota estado;

	/**
	 * Instantiates a new cuota DTO.
	 */
	public CuotaDTO() {
		super();
	}

	/**
	 * Instantiates a new cuota DTO.
	 *
	 * @param numeroCuota the numero cuota
	 * @param importe the importe
	 * @param fechaPago the fecha pago
	 * @param fechaVencimiento the fecha vencimiento
	 * @param estado the estado
	 */
	public CuotaDTO(Integer numeroCuota, BigDecimal importe, Date fechaPago, Date fechaVencimiento,
			EstadoCuota estado) {
		super();

		this.numeroCuota = numeroCuota;
		this.importe = importe;
		this.fechaPago = fechaPago;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = estado;
	}

	/**
	 * Instantiates a new cuota DTO.
	 *
	 * @param ventaId the venta id
	 * @param numeroCuota the numero cuota
	 * @param importe the importe
	 * @param fechaPago the fecha pago
	 * @param fechaVencimiento the fecha vencimiento
	 * @param estado the estado
	 */
	public CuotaDTO(Integer ventaId, Integer numeroCuota, BigDecimal importe, Date fechaPago, Date fechaVencimiento,
			EstadoCuota estado) {
		super();
		this.ventaId = ventaId;
		this.numeroCuota = numeroCuota;
		this.importe = importe;
		this.fechaPago = fechaPago;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = estado;
	}

	/**
	 * Instantiates a new cuota DTO.
	 *
	 * @param id the id
	 * @param ventaId the venta id
	 * @param numeroCuota the numero cuota
	 * @param importe the importe
	 * @param fechaPago the fecha pago
	 * @param fechaVencimiento the fecha vencimiento
	 * @param estado the estado
	 */
	public CuotaDTO(Integer id, Integer ventaId, Integer numeroCuota, BigDecimal importe, Date fechaPago,
			Date fechaVencimiento, EstadoCuota estado) {
		super();
		this.id = id;
		this.ventaId = ventaId;
		this.numeroCuota = numeroCuota;
		this.importe = importe;
		this.fechaPago = fechaPago;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = estado;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the numero cuota.
	 *
	 * @return the numero cuota
	 */
	public Integer getNumeroCuota() {
		return numeroCuota;
	}

	/**
	 * Sets the numero cuota.
	 *
	 * @param numeroCuota the new numero cuota
	 */
	public void setNumeroCuota(Integer numeroCuota) {
		this.numeroCuota = numeroCuota;
	}

	/**
	 * Gets the importe.
	 *
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * Sets the importe.
	 *
	 * @param importe the new importe
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * Gets the fecha pago.
	 *
	 * @return the fecha pago
	 */
	public Date getFechaPago() {
		return fechaPago;
	}

	/**
	 * Sets the fecha pago.
	 *
	 * @param fechaPago the new fecha pago
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	/**
	 * Gets the fecha vencimiento.
	 *
	 * @return the fecha vencimiento
	 */
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	/**
	 * Sets the fecha vencimiento.
	 *
	 * @param fechaVencimiento the new fecha vencimiento
	 */
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public EstadoCuota getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the new estado
	 */
	public void setEstado(EstadoCuota estado) {
		this.estado = estado;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Gets the venta id.
	 *
	 * @return the ventaId
	 */
	public Integer getVentaId() {
		return ventaId;
	}

	/**
	 * Sets the venta id.
	 *
	 * @param ventaId the ventaId to set
	 */
	public void setVentaId(Integer ventaId) {
		this.ventaId = ventaId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "CuotaDTO [id=" + id + ", ventaId=" + ventaId + ", numeroCuota=" + numeroCuota + ", importe=" + importe
				+ ", fechaPago=" + fechaPago + ", fechaVencimiento=" + fechaVencimiento + ", estado=" + estado + "]";
	}

}
