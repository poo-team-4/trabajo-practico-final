package ar.edu.unju.fi.tpfinal.util;

/**
 * The Enum EstadoInstrumento.
 */
public enum EstadoInstrumento {

	/** The disponible. */
	DISPONIBLE,
	/** The vendido. */
	VENDIDO
}
