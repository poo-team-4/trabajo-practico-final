package ar.edu.unju.fi.tpfinal.util;

/**
 * The Enum EstadoCuota.
 */
public enum EstadoCuota {

	/** The adeudada. */
	ADEUDADA,
	/** The pagada. */
	PAGADA
}
