package ar.edu.unju.fi.tpfinal.modelo.dominio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class Financiada.
 */
@Entity
@DiscriminatorValue(value = "financiada")
public class Financiada extends Venta {

	/** The interes. */
	@Column(name = "interes")
	private Double interes;

	/**
	 * Instantiates a new financiada.
	 */
	public Financiada() {
		super();

	}

	/**
	 * Instantiates a new financiada.
	 *
	 * @param instrumento the instrumento
	 * @param precio      the precio
	 * @param fecha       the fecha
	 * @param cliente     the cliente
	 * @param cuota       the cuota
	 * @param interes     the interes
	 */

	public Financiada(Instrumento instrumento, BigDecimal precio, Date fecha, Usuario cliente, List<Cuota> cuota,
			Double interes) {
		super(instrumento, precio, fecha, cliente, cuota);
		this.interes = interes;
	}

	/**
	 * Gets the interes.
	 *
	 * @return the interes
	 */
	public Double getInteres() {
		return interes;
	}

	/**
	 * Sets the interes.
	 *
	 * @param interes the interes to set
	 */
	public void setInteres(Double interes) {
		this.interes = interes;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Financiada [getInteres()=" + getInteres() + ", getId()=" + getId() + ", getInstrumento()="
				+ getInstrumento() + ", getPrecio()=" + getPrecio() + ", getFecha()=" + getFecha() + ", getCliente()="
				+ getCliente() + ", getCuota()=" + getCuota() + ", getEstado()=" + getEstado() + "]";
	}

}
