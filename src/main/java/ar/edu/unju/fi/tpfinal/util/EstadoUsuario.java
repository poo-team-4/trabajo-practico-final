package ar.edu.unju.fi.tpfinal.util;

/**
 * The Enum EstadoUsuario.
 */
public enum EstadoUsuario {

	/** The activo. */
	ACTIVO,
	/** The inactivo. */
	INACTIVO
}
