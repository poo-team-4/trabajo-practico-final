package ar.edu.unju.fi.tpfinal.util;

/**
 * The Enum TipoVenta.
 */
public enum TipoVenta {

	/** The contado. */
	CONTADO,
	/** The financiada. */
	FINANCIADA
}
