/**
 * 
 */
package ar.edu.unju.fi.tpfinal.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ar.edu.unju.fi.tpfinal.app.AppSetting;
import ar.edu.unju.fi.tpfinal.app.CurrencyApi;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;

/**
 * The Class VentaUtils.
 *
 * @author TEAM 4
 */
public class VentaUtils {

	/**
	 * Genera cuotas a partir de un ventaDto y una Venta.
	 *
	 * @param ventaDto     the venta dto
	 * @param ventaDestino the venta destino
	 * @return the venta
	 */
	public static Venta generarCuotas(final VentaDTO ventaDto, Venta ventaDestino) {
		// TODO: Hacer un refactor para que dependa de un solo parámetro
		List<Cuota> cuotasGeneradas = new ArrayList<Cuota>();
		if (VentaUtils.esVentaContado(ventaDto)) {
			cuotasGeneradas.add(new Cuota(1, ventaDestino.getPrecio().divide(new BigDecimal(1)), new Date(), new Date(),
					EstadoCuota.PAGADA));
		} else {
			for (int i = 1; i <= ventaDto.getCuotas(); i++) {
				cuotasGeneradas.add(new Cuota(i,
						ventaDestino.getPrecio().divide(new BigDecimal(ventaDto.getCuotas()), MathContext.DECIMAL128),
						null, VentaUtils.calcularFechaDeNumeroDeCuota(i)));
			}
		}
		ventaDestino.setCuota(cuotasGeneradas);
		return ventaDestino;
	}

	/**
	 * Se calcula el precio final que tiene una venta segun sus cuotas y el precio
	 * en pesos.
	 * 
	 *
	 * @param venta             the venta
	 * @param precioInstrumento the precio instrumento
	 * @throws Exception the exception
	 */
	public static void calcularPrecioDeVenta(VentaDTO venta, BigDecimal precioInstrumento) throws Exception {
		// TODO: hacer un refactor para que dependa de un solo parámetro
		precioInstrumento = CurrencyApi.convertirDolarAPesos(precioInstrumento);
		if (VentaUtils.esVentaContado(venta)) {
			venta.setPrecio(precioInstrumento.subtract(
					precioInstrumento.multiply(new BigDecimal(AppSetting.getInstance().getDescuentoContado()))));
			venta.setDescuento(AppSetting.getInstance().getDescuentoContado());
		} else {
			venta.setPrecio(precioInstrumento.add(precioInstrumento
					.multiply(new BigDecimal(AppSetting.getInstance().getInteresPorCuota(venta.getCuotas())))));
			venta.setInteres(AppSetting.getInstance().getInteresPorCuota(venta.getCuotas()));
		}
	}

	/**
	 * Se calcula la fecha segun el número de la cuota.
	 *
	 * @param numeroDeCuota the numero de cuota
	 * @return Date
	 */
	private static Date calcularFechaDeNumeroDeCuota(final Integer numeroDeCuota) {
		Calendar date = Calendar.getInstance();

		// Se agrega 30 días por cuota según lo solicitado en el TP
		date.add(Calendar.DATE, 30 * numeroDeCuota);

		// Creo que iría mejor con un 1 mes en vez de 30 días.
		// date.add(Calendar.MONTH, numeroDeCuota);
		return date.getTime();
	}

	/**
	 * Es venta contado.
	 *
	 * @param ventaDto the venta dto
	 * @return the boolean
	 */
	private static Boolean esVentaContado(final VentaDTO ventaDto) {
		return ventaDto.getCuotas().equals(1);
	}

	/**
	 * Format date to string.
	 *
	 * @param dateAFormatear the date A formatear
	 * @return the string
	 */
	public static String formatDateToString(final Date dateAFormatear) {
		String dateFormat = "dd-MM-yyyy";
		return (dateAFormatear != null) ? new SimpleDateFormat(dateFormat).format(dateAFormatear).toString() : "";
	}

	/**
	 * Format importe.
	 *
	 * @param importe the importe
	 * @return the string
	 */
	public static String formatImporte(final BigDecimal importe) {
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("es", "AR"));
		return numberFormat.format(importe.doubleValue());
	}

	/**
	 * Format importe dolar.
	 *
	 * @param importe the importe
	 * @return the string
	 */
	public static String formatImporteDolar(final BigDecimal importe) {
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("en", "US"));
		return numberFormat.format(importe.doubleValue());
	}

	/**
	 * Format numero.
	 *
	 * @param numero the numero
	 * @return the string
	 */
	public static String formatNumero(final BigDecimal numero) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		return numberFormat.format(numero.doubleValue()).replace(".", "");
	}

	/**
	 * Format porcentaje.
	 *
	 * @param porcentaje the porcentaje
	 * @return the string
	 */
	public static String formatPorcentaje(final Double porcentaje) {
		NumberFormat defaultFormat = NumberFormat.getPercentInstance();
		return defaultFormat.format(porcentaje);
	}

}
