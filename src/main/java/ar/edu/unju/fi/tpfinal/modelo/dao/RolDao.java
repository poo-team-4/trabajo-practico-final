package ar.edu.unju.fi.tpfinal.modelo.dao;

import java.util.List;

import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Rol;

/**
 * The Interface RolDao.
 */
public interface RolDao {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Rol> get() throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param rol the rol
	 * @return the rol
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Rol get(Rol rol) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param nombreRol the nombre rol
	 * @return the rol
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Rol get(String nombreRol) throws ObtenerException, Exception;

	/**
	 * Insert.
	 *
	 * @param rol the rol
	 * @return the rol
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	Rol insert(Rol rol) throws InsertarException, Exception;

	/**
	 * Update.
	 *
	 * @param rol the rol
	 * @return the rol
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	Rol update(Rol rol) throws UpdateException, Exception;

	/**
	 * Delete.
	 *
	 * @param rol the rol
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	void delete(Rol rol) throws DeleteException, Exception;

}
