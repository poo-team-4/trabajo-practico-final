package ar.edu.unju.fi.tpfinal.util;

/**
 * The Enum EstadoVenta.
 */
public enum EstadoVenta {
	
	/** The adeudada. */
	ADEUDADA, 
 /** The finalizada. */
 FINALIZADA

}
