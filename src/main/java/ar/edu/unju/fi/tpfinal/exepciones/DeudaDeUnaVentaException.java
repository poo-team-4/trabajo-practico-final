package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class DeudaDeUnaVentaException.
 */
public class DeudaDeUnaVentaException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3274849293L;

	/**
	 * Instantiates a new deuda de una venta exception.
	 *
	 * @param mensaje the mensaje
	 */
	public DeudaDeUnaVentaException(String mensaje) {
		super("No existe la venta con id" + mensaje);
	}
}
