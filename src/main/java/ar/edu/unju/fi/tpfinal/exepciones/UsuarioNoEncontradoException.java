package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class UsuarioNoEncontradoException.
 */
public class UsuarioNoEncontradoException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 84585947585L;

	/**
	 * Instantiates a new usuario no encontrado exception.
	 */
	public UsuarioNoEncontradoException() {

		super("No se encontró al usuario.");
	}

}
