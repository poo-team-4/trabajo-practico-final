package ar.edu.unju.fi.tpfinal.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpfinal.exepciones.LoginPasswordException;
import ar.edu.unju.fi.tpfinal.exepciones.UsuarioInactivoException;
import ar.edu.unju.fi.tpfinal.exepciones.UsuarioNoEncontradoException;
import ar.edu.unju.fi.tpfinal.modelo.dao.RolDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.UsuarioDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Usuario;
import ar.edu.unju.fi.tpfinal.modelo.dto.UsuarioCredencialDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.UsuarioDTO;
import ar.edu.unju.fi.tpfinal.services.UsuarioService;
import ar.edu.unju.fi.tpfinal.util.EstadoUsuario;
import ar.edu.unju.fi.tpfinal.util.ObjectMapperUtils;

/**
 * The Class UsuarioServiceImpl.
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

	/** The usuario dao. */
	@Inject
	private UsuarioDao usuarioDao;

	/** The rol dao. */
	@Inject
	private RolDao rolDao;

	/** The login failsc count. */
	private Integer loginFails = 0;

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	@Override
	public List<UsuarioDTO> get() throws Exception {
		return ObjectMapperUtils.mapAll(usuarioDao.get(), UsuarioDTO.class);
	}

	/**
	 * Gets the.
	 *
	 * @param nombreUsuario the nombre usuario
	 * @return the usuario DTO
	 * @throws Exception the exception
	 */
	@Override
	public UsuarioDTO get(String nombreUsuario) throws Exception {
		Usuario unUsuario = usuarioDao.get(nombreUsuario);
		return ObjectMapperUtils.map(unUsuario, new UsuarioDTO());
	}

	/**
	 * Insert.
	 *
	 * @param nuevoUsuario the nuevo usuario
	 * @return the usuario DTO
	 * @throws Exception the exception
	 */
	@Override
	public UsuarioDTO insert(UsuarioDTO nuevoUsuario) throws Exception {
		Usuario unUsuario = new Usuario();
		ObjectMapperUtils.map(nuevoUsuario, unUsuario);

		unUsuario.setRol(rolDao.get(nuevoUsuario.getRolDescripcion()));

		return ObjectMapperUtils.map(usuarioDao.insert(unUsuario), nuevoUsuario);
	}

	/**
	 * Update.
	 *
	 * @param usuarioModificado the usuario modificado
	 * @return the usuario DTO
	 * @throws Exception the exception
	 */
	@Override
	public UsuarioDTO update(UsuarioDTO usuarioModificado) throws Exception {
		Usuario unUsuario = new Usuario();
		ObjectMapperUtils.map(usuarioModificado, unUsuario);
		if (usuarioModificado.getRolDescripcion() != null) {
			unUsuario.setRol(rolDao.get(usuarioModificado.getRolDescripcion()));
		}
		usuarioDao.update(unUsuario);
		return usuarioModificado;
	}

	/**
	 * Delete.
	 *
	 * @param usuarioAEliminar the usuario A eliminar
	 * @throws Exception the exception
	 */
	@Override
	public void delete(UsuarioDTO usuarioAEliminar) throws Exception {
		usuarioDao.delete(ObjectMapperUtils.map(usuarioAEliminar, new Usuario()));
	}

	/**
	 * Login.
	 *
	 * @param credencial the credencial
	 * @return the usuario DTO
	 * @throws UsuarioNoEncontradoException the usuario no encontrado exception
	 * @throws UsuarioInactivoException the usuario inactivo exception
	 * @throws LoginPasswordException   the login password exception
	 * @throws Exception                the exception
	 */
	@Override
	public UsuarioDTO login(UsuarioCredencialDTO credencial)
			throws UsuarioNoEncontradoException, UsuarioInactivoException, LoginPasswordException, Exception {
		Usuario usuarioEncontrado = null;
		try {
			usuarioEncontrado = usuarioDao.get(credencial.getUser());
		} catch (Exception e) {
			throw new UsuarioNoEncontradoException();
		}
		if (usuarioEncontrado.getEstado().equals(EstadoUsuario.INACTIVO)) {
			throw new UsuarioInactivoException();
		}
		if (!usuarioEncontrado.getPassword().equals(credencial.getPassword())) {
			if (this.loginFails > 2) {
				usuarioEncontrado.setEstado(EstadoUsuario.INACTIVO);
				usuarioDao.update(usuarioEncontrado);
			}
			this.loginFails++;
			throw new LoginPasswordException();
		}

		return ObjectMapperUtils.map(usuarioEncontrado, new UsuarioDTO());
	}

}
