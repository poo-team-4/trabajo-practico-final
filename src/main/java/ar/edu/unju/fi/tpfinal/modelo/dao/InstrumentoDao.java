package ar.edu.unju.fi.tpfinal.modelo.dao;

import java.util.List;

import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;

/**
 * The Interface InstrumentoDao.
 *
 * @author TEAM 4
 */
public interface InstrumentoDao {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Instrumento> get() throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param instrumento the instrumento
	 * @return the instrumento
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Instrumento get(Instrumento instrumento) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param numeroSerie the numero serie
	 * @return the instrumento
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Instrumento get(String numeroSerie) throws ObtenerException, Exception;

	/**
	 * Insert.
	 *
	 * @param instrumento the instrumento
	 * @return the instrumento
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	Instrumento insert(Instrumento instrumento) throws InsertarException, Exception;

	/**
	 * Update.
	 *
	 * @param instrumento the instrumento
	 * @return the instrumento
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	Instrumento update(Instrumento instrumento) throws UpdateException, Exception;

	/**
	 * Delete.
	 *
	 * @param instrumento the instrumento
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	void delete(Instrumento instrumento) throws DeleteException, Exception;

}
