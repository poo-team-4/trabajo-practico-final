package ar.edu.unju.fi.tpfinal.services;

import java.util.List;

import ar.edu.unju.fi.tpfinal.modelo.dto.UsuarioCredencialDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.UsuarioDTO;

/**
 * The Interface UsuarioService.
 */
public interface UsuarioService {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	List<UsuarioDTO> get() throws Exception;

	/**
	 * Gets the.
	 *
	 * @param nombreUsuario the nombre usuario
	 * @return the usuario DTO
	 * @throws Exception the exception
	 */
	UsuarioDTO get(String nombreUsuario) throws Exception;

	/**
	 * Insert.
	 *
	 * @param nuevoUsuario the nuevo usuario
	 * @return the usuario DTO
	 * @throws Exception the exception
	 */
	UsuarioDTO insert(UsuarioDTO nuevoUsuario) throws Exception;

	/**
	 * Update.
	 *
	 * @param usuarioModificado the usuario modificado
	 * @return the usuario DTO
	 * @throws Exception the exception
	 */
	UsuarioDTO update(UsuarioDTO usuarioModificado) throws Exception;

	/**
	 * Delete.
	 *
	 * @param usuarioAEliminar the usuario A eliminar
	 * @throws Exception the exception
	 */
	void delete(UsuarioDTO usuarioAEliminar) throws Exception;

	/**
	 * Login.
	 *
	 * @param credencial the credencial
	 * @return the usuario DTO
	 * @throws Exception the exception
	 */
	UsuarioDTO login(UsuarioCredencialDTO credencial) throws Exception;

}
