/**
 * 
 */
package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;

/**
 * The Class UsuarioCredencialDTO.
 *
 * @author TEAM 4
 */
public class UsuarioCredencialDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user. */
	private String user;

	/** The password. */
	private String password;

	/**
	 * Instantiates a new usuario credencial DTO.
	 */
	public UsuarioCredencialDTO() {
	}

	/**
	 * Instantiates a new usuario credencial DTO.
	 *
	 * @param user     the user
	 * @param password the password
	 */
	public UsuarioCredencialDTO(String user, String password) {
		this.user = user;
		this.password = password;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "UsuarioCredencialDTO [user=" + user + ", password=" + password + "]";
	}

}
