package ar.edu.unju.fi.tpfinal.modelo.dominio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import ar.edu.unju.fi.tpfinal.util.EstadoVenta;

/**
 * The Class Venta.
 */
@Entity
@Table(name = "ventas")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo_venta")
public abstract class Venta {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer id;

	/** The instrumento. */
	@OneToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "instrumento")
	private Instrumento instrumento;

	/** The precio. */
	@Column(name = "importe")
	private BigDecimal precio;

	/** The fecha. */
	@Column(name = "fecha")
	private Date fecha;

	/** The cliente. */
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "cliente")
	private Usuario cliente;

	/** The cuota. */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_venta")
	@OrderBy
	private List<Cuota> cuota;

	/** The estado. */
	@Enumerated(EnumType.STRING)
	@Column(length = 10)
	private EstadoVenta estado;

	/**
	 * Instantiates a new venta.
	 */
	public Venta() {

	}

	/**
	 * Instantiates a new venta.
	 *
	 * @param instrumento the instrumento
	 * @param precio      the precio
	 * @param fecha       the fecha
	 * @param cliente     the cliente
	 * @param cuota       the cuota
	 */
	public Venta(Instrumento instrumento, BigDecimal precio, Date fecha, Usuario cliente, List<Cuota> cuota) {
		super();
		this.instrumento = instrumento;
		this.precio = precio;
		this.fecha = fecha;
		this.cliente = cliente;
		this.cuota = cuota;
	}

	/**
	 * Instantiates a new venta.
	 *
	 * @param id          the id
	 * @param instrumento the instrumento
	 * @param precio      the precio
	 * @param fecha       the fecha
	 * @param cliente     the cliente
	 * @param cuota       the cuota
	 */
	public Venta(Integer id, Instrumento instrumento, BigDecimal precio, Date fecha, Usuario cliente,
			List<Cuota> cuota) {
		this.id = id;
		this.instrumento = instrumento;
		this.precio = precio;
		this.fecha = fecha;
		this.cliente = cliente;
		this.cuota = cuota;
	}

	/**
	 * Instantiates a new venta.
	 *
	 * @param instrumento the instrumento
	 * @param precio the precio
	 * @param fecha the fecha
	 * @param cliente the cliente
	 * @param cuota the cuota
	 * @param estado the estado
	 */
	public Venta(Instrumento instrumento, BigDecimal precio, Date fecha, Usuario cliente, List<Cuota> cuota,
			EstadoVenta estado) {
		super();
		this.instrumento = instrumento;
		this.precio = precio;
		this.fecha = fecha;
		this.cliente = cliente;
		this.cuota = cuota;
		this.estado = estado;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the instrumento.
	 *
	 * @return instrumento
	 */
	public Instrumento getInstrumento() {
		return instrumento;
	}

	/**
	 * Sets the instrumento.
	 *
	 * @param instrumento the instrumento to set
	 */
	public void setInstrumento(Instrumento instrumento) {
		this.instrumento = instrumento;
	}

	/**
	 * Gets the precio.
	 *
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}

	/**
	 * Sets the precio.
	 *
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * Gets the fecha.
	 *
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * Sets the fecha.
	 *
	 * @param fecha the id to fecha
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	public Usuario getCliente() {
		return cliente;
	}

	/**
	 * Sets the cliente.
	 *
	 * @param cliente the cliente to set
	 */
	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}

	/**
	 * Gets the cuota.
	 *
	 * @return the cuota list
	 */
	public List<Cuota> getCuota() {
		return cuota;
	}

	/**
	 * Sets the cuota.
	 *
	 * @param cuota list to set
	 */
	public void setCuota(List<Cuota> cuota) {
		this.cuota = cuota;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public EstadoVenta getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoVenta estado) {
		this.estado = estado;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Venta [id=" + id + ", instrumento=" + instrumento + ", precio=" + precio + ", fecha=" + fecha
				+ ", cliente=" + cliente + ", cuota=" + cuota + ", estado=" + estado + "]";
	}

}
