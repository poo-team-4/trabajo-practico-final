/**
 * 
 */
package ar.edu.unju.fi.tpfinal.services.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpfinal.exepciones.CuotaDeVentaException;
import ar.edu.unju.fi.tpfinal.exepciones.PagarCuotaException;
import ar.edu.unju.fi.tpfinal.modelo.dao.CuotaDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.VentaDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Financiada;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.modelo.dto.CuotaDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;
import ar.edu.unju.fi.tpfinal.services.CuotaService;
import ar.edu.unju.fi.tpfinal.util.EstadoCuota;
import ar.edu.unju.fi.tpfinal.util.EstadoVenta;
import ar.edu.unju.fi.tpfinal.util.ObjectMapperUtils;

/**
 * The Class CuotaServiceImpl.
 *
 * @author TEAM 4
 */
@Service
public class CuotaServiceImpl implements CuotaService {

	/** The cuota dao. */
	@Inject
	private CuotaDao cuotaDao;
	
	/** The venta dao. */
	@Inject
	private VentaDao ventaDao;

	/**
	 * Pagar cuota.
	 *
	 * @param cuotaAPagar the cuota A pagar
	 * @return the cuota DTO
	 * @throws PagarCuotaException the pagar cuota exception
	 * @throws Exception the exception
	 */
	@Override
	public CuotaDTO pagarCuota(CuotaDTO cuotaAPagar) throws PagarCuotaException, Exception {
		Cuota cuota = ObjectMapperUtils.map(cuotaAPagar, new Cuota());

		if (cuota.getEstado().equals(EstadoCuota.PAGADA))
			throw new PagarCuotaException();

		cuota.setEstado(EstadoCuota.PAGADA);
		cuota.setFechaPago(new Date());

		ObjectMapperUtils.map(cuotaDao.update(cuota), cuotaAPagar);

		Venta venta = ventaDao.get(cuota.getVentaId());
		if (cuotaDao.getCuotasAdeudadas(venta).size() == 0) {
			venta.setEstado(EstadoVenta.FINALIZADA);
			ventaDao.update(venta);
		}

		return cuotaAPagar;
	}

	/**
	 * Obtener proxima cuota de venta.
	 *
	 * @param venta the venta
	 * @return the cuota DTO
	 * @throws CuotaDeVentaException the cuota de venta exception
	 * @throws Exception the exception
	 */
	@Override
	public CuotaDTO obtenerProximaCuotaDeVenta(VentaDTO venta) throws CuotaDeVentaException, Exception {
		Venta ventaFinanciada = ObjectMapperUtils.map(venta, new Financiada());

		List<Cuota> cuotasDeVenta = cuotaDao.getCuotasAdeudadas(ventaFinanciada);

		if (cuotasDeVenta.size() == 0) {
			throw new CuotaDeVentaException();
		}

		return ObjectMapperUtils.map(cuotasDeVenta.get(0), new CuotaDTO());
	}

}
