package ar.edu.unju.fi.tpfinal.modelo.dominio;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.edu.unju.fi.tpfinal.util.EstadoCuota;

/**
 * The Class Cuota.
 */
@Entity
@Table(name = "cuotas")
public class Cuota {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer id;

	/** The numero cuota. */
	@Column(name = "numero_de_cuota")
	private Integer numeroCuota;

	/** The importe. */
	@Column
	private BigDecimal importe;

	/** The fecha pago. */
	@Column(name = "fecha_de_pago")
	private Date fechaPago;

	/** The fecha vencimiento. */
	@Column(name = "fecha_de_vencimiento")
	private Date fechaVencimiento;

	/** The estado. */
	@Enumerated(EnumType.STRING)
	@Column(length = 10)
	private EstadoCuota estado;

	/** The venta id. */
	@Column(name = "id_venta")
	private Integer ventaId;

	/**
	 * Instantiates a new cuota.
	 */
	public Cuota() {

	}

	/**
	 * Instantiates a new cuota.
	 *
	 * @param numeroCuota      the numero cuota
	 * @param importe          the importe
	 * @param fechaPago        the fecha pago
	 * @param fechaVencimiento the fecha vencimiento
	 */
	public Cuota(Integer numeroCuota, BigDecimal importe, Date fechaPago, Date fechaVencimiento) {
		super();
		this.numeroCuota = numeroCuota;
		this.importe = importe;
		this.fechaPago = fechaPago;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = EstadoCuota.ADEUDADA;
	}

	/**
	 * Instantiates a new cuota.
	 *
	 * @param numeroCuota      the numero cuota
	 * @param importe          the importe
	 * @param fechaPago        the fecha pago
	 * @param fechaVencimiento the fecha vencimiento
	 * @param estado           the estado
	 */
	public Cuota(Integer numeroCuota, BigDecimal importe, Date fechaPago, Date fechaVencimiento, EstadoCuota estado) {
		super();
		this.numeroCuota = numeroCuota;
		this.importe = importe;
		this.fechaPago = fechaPago;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = estado;
	}

	/**
	 * Instantiates a new cuota.
	 *
	 * @param id               the id
	 * @param numeroCuota      the numero cuota
	 * @param importe          the importe
	 * @param fechaPago        the fecha pago
	 * @param fechaVencimiento the fecha vencimiento
	 * @param estado           the estado
	 */
	public Cuota(Integer id, Integer numeroCuota, BigDecimal importe, Date fechaPago, Date fechaVencimiento,
			EstadoCuota estado) {
		this.id = id;
		this.numeroCuota = numeroCuota;
		this.importe = importe;
		this.fechaPago = fechaPago;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = estado;
	}

	/**
	 * Instantiates a new cuota.
	 *
	 * @param id               the id
	 * @param numeroCuota      the numero cuota
	 * @param importe          the importe
	 * @param fechaPago        the fecha pago
	 * @param fechaVencimiento the fecha vencimiento
	 * @param estado           the estado
	 * @param ventaId          the venta id
	 */
	public Cuota(Integer id, Integer numeroCuota, BigDecimal importe, Date fechaPago, Date fechaVencimiento,
			EstadoCuota estado, Integer ventaId) {
		super();
		this.id = id;
		this.numeroCuota = numeroCuota;
		this.importe = importe;
		this.fechaPago = fechaPago;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = estado;
		this.ventaId = ventaId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the numero cuota.
	 *
	 * @return the numeroCuota
	 */
	public Integer getNumeroCuota() {
		return numeroCuota;
	}

	/**
	 * Sets the numero cuota.
	 *
	 * @param numeroCuota the numeroCuota to set
	 */
	public void setNumeroCuota(Integer numeroCuota) {
		this.numeroCuota = numeroCuota;
	}

	/**
	 * Gets the importe.
	 *
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * Sets the importe.
	 *
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * Gets the fecha pago.
	 *
	 * @return the fechaPago
	 */
	public Date getFechaPago() {
		return fechaPago;
	}

	/**
	 * Sets the fecha pago.
	 *
	 * @param fechaPago the fechaPago to set
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	/**
	 * Gets the fecha vencimiento.
	 *
	 * @return the fechaVencimiento
	 */
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	/**
	 * Sets the fecha vencimiento.
	 *
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public EstadoCuota getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoCuota estado) {
		this.estado = estado;
	}

	/**
	 * Gets the venta id.
	 *
	 * @return the ventaId
	 */
	public Integer getVentaId() {
		return ventaId;
	}

	/**
	 * Sets the venta id.
	 *
	 * @param ventaId the ventaId to set
	 */
	public void setVentaId(Integer ventaId) {
		this.ventaId = ventaId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Cuota [id=" + id + ", numeroCuota=" + numeroCuota + ", importe=" + importe + ", fechaPago=" + fechaPago
				+ ", fechaVencimiento=" + fechaVencimiento + ", estado=" + estado + ", ventaId=" + ventaId + "]";
	}

}
