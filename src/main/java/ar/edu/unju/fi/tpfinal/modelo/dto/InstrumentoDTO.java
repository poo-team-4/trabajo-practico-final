/**
 * 
 */
package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class InstrumentoDTO.
 *
 * @author TEAM 4
 */
public class InstrumentoDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The numero serie. */
	private String numeroSerie;

	/** The descripcion. */
	private String descripcion;

	/** The precio. */
	private BigDecimal precio;

	/** The anio. */
	private Integer anio;

	/** The pais nombre. */
	private String paisNombre;

	/** The marca nombre. */
	private String marcaNombre;

	/** The tipo instrumento. */
	private String tipoInstrumento;

	/** The estado. */
	private String estado;

	/**
	 * Instantiates a new instrumento DTO.
	 */
	public InstrumentoDTO() {

	}

	/**
	 * Instantiates a new instrumento DTO.
	 *
	 * @param numeroSerie     the numero serie
	 * @param descripcion     the descripcion
	 * @param precio          the precio
	 * @param anio            the anio
	 * @param paisNombre      the pais nombre
	 * @param marcaNombre     the marca nombre
	 * @param tipoInstrumento the tipo instrumento
	 * @param estado          the estado
	 */
	public InstrumentoDTO(String numeroSerie, String descripcion, BigDecimal precio, Integer anio, String paisNombre,
			String marcaNombre, String tipoInstrumento, String estado) {
		this.numeroSerie = numeroSerie;
		this.descripcion = descripcion;
		this.precio = precio;
		this.anio = anio;
		this.paisNombre = paisNombre;
		this.marcaNombre = marcaNombre;
		this.tipoInstrumento = tipoInstrumento;
		this.estado = estado;
	}

	/**
	 * Instantiates a new instrumento DTO.
	 *
	 * @param id              the id
	 * @param numeroSerie     the numero serie
	 * @param descripcion     the descripcion
	 * @param precio          the precio
	 * @param anio            the anio
	 * @param paisNombre      the pais nombre
	 * @param marcaNombre     the marca nombre
	 * @param tipoInstrumento the tipo instrumento
	 * @param estado          the estado
	 */
	public InstrumentoDTO(Integer id, String numeroSerie, String descripcion, BigDecimal precio, Integer anio,
			String paisNombre, String marcaNombre, String tipoInstrumento, String estado) {
		this.id = id;
		this.numeroSerie = numeroSerie;
		this.descripcion = descripcion;
		this.precio = precio;
		this.anio = anio;
		this.paisNombre = paisNombre;
		this.marcaNombre = marcaNombre;
		this.tipoInstrumento = tipoInstrumento;
		this.estado = estado;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the numero serie.
	 *
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}

	/**
	 * Sets the numero serie.
	 *
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the precio.
	 *
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}

	/**
	 * Sets the precio.
	 *
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * Gets the anio.
	 *
	 * @return the anio
	 */
	public Integer getAnio() {
		return anio;
	}

	/**
	 * Sets the anio.
	 *
	 * @param anio the anio to set
	 */
	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	/**
	 * Gets the pais nombre.
	 *
	 * @return the paisNombre
	 */
	public String getPaisNombre() {
		return paisNombre;
	}

	/**
	 * Sets the pais nombre.
	 *
	 * @param paisNombre the new pais nombre
	 */
	public void setPaisNombre(String paisNombre) {
		this.paisNombre = paisNombre;
	}

	/**
	 * Gets the marca nombre.
	 *
	 * @return the marcaNombre
	 */
	public String getMarcaNombre() {
		return marcaNombre;
	}

	/**
	 * Sets the marca nombre.
	 *
	 * @param marcaNombre the marcaNombre to set
	 */
	public void setMarcaNombre(String marcaNombre) {
		this.marcaNombre = marcaNombre;
	}

	/**
	 * Gets the tipo instrumento.
	 *
	 * @return the tipoInstrumento
	 */
	public String getTipoInstrumento() {
		return tipoInstrumento;
	}

	/**
	 * Sets the tipo instrumento.
	 *
	 * @param tipoInstrumento the tipoInstrumento to set
	 */
	public void setTipoInstrumento(String tipoInstrumento) {
		this.tipoInstrumento = tipoInstrumento;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "InstrumentoDTO [id=" + id + ", numeroSerie=" + numeroSerie + ", descripcion=" + descripcion
				+ ", precio=" + precio + ", anio=" + anio + ", paisNombre=" + paisNombre + ", marcaNombre="
				+ marcaNombre + ", tipoInstrumento=" + tipoInstrumento + ", estado=" + estado + "]";
	}

}
