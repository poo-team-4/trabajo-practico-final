package ar.edu.unju.fi.tpfinal.modelo.dto;

/**
 * The Class PaisDTO.
 *
 * @author TEAM 4
 */
public class PaisDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The nombre. */
	private String nombre;

	/**
	 * Instantiates a new pais DTO.
	 */
	public PaisDTO() {

	}

	/**
	 * Instantiates a new pais DTO.
	 *
	 * @param nombre the nombre
	 */
	public PaisDTO(String nombre) {
		super();
		this.nombre = nombre;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "PaisDTO [id=" + id + ", nombre=" + nombre + "]";
	}

}
