/**
 * 
 */
package ar.edu.unju.fi.tpfinal.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpfinal.modelo.dao.PaisDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Pais;
import ar.edu.unju.fi.tpfinal.modelo.dto.PaisDTO;
import ar.edu.unju.fi.tpfinal.services.PaisService;
import ar.edu.unju.fi.tpfinal.util.ObjectMapperUtils;

/**
 * The Class PaisServiceImpl.
 *
 * @author TEAM 4
 */

@Service
public class PaisServiceImpl implements PaisService {

	/** The pais dao. */
	@Inject
	private PaisDao paisDao;

	/** The mapper. */
	private ModelMapper mapper = new ModelMapper();

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<PaisDTO> get() throws Exception {
		return ObjectMapperUtils.mapAll(paisDao.get(), PaisDTO.class);
	}

	/**
	 * Gets the.
	 *
	 * @param nombre the nombre
	 * @return the pais DTO
	 * @throws Exception the exception
	 */
	public PaisDTO get(String nombre) throws Exception {
		Pais unPais = paisDao.get(nombre);
		PaisDTO paisDTO = new PaisDTO();
		mapper.map(unPais, paisDTO);
		return paisDTO;
	}

	/**
	 * Insert.
	 *
	 * @param pais the pais
	 * @return the pais DTO
	 * @throws Exception the exception
	 */
	public PaisDTO insert(PaisDTO pais) throws Exception {
		Pais unPais = new Pais();
		mapper.map(pais, unPais);
		paisDao.insert(unPais);
		return pais;
	}

	/**
	 * Update.
	 *
	 * @param pais the pais
	 * @return the pais DTO
	 * @throws Exception the exception
	 */
	public PaisDTO update(PaisDTO pais) throws Exception {
		Pais unPais = new Pais();
		mapper.map(pais, unPais);
		paisDao.update(unPais);
		return pais;
	}

	/**
	 * Delete.
	 *
	 * @param pais the pais
	 * @return the pais DTO
	 * @throws Exception the exception
	 */
	public PaisDTO delete(PaisDTO pais) throws Exception {
		Pais unPais = new Pais();
		mapper.map(pais, unPais);
		paisDao.delete(unPais);
		return pais;
	}

}
