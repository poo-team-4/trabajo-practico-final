package ar.edu.unju.fi.tpfinal.services.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tpfinal.exepciones.CantidadCuotaException;
import ar.edu.unju.fi.tpfinal.exepciones.InstrumentoVendidoException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.modelo.dao.InstrumentoDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.UsuarioDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.VentaDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Contado;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Financiada;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Usuario;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.modelo.dto.InstrumentoDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.UsuarioDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;
import ar.edu.unju.fi.tpfinal.services.VentaServicio;
import ar.edu.unju.fi.tpfinal.util.EstadoInstrumento;
import ar.edu.unju.fi.tpfinal.util.EstadoVenta;
import ar.edu.unju.fi.tpfinal.util.ObjectMapperUtils;
import ar.edu.unju.fi.tpfinal.util.VentaUtils;

/**
 * The Class VentaServicioImpl.
 */
@Service
public class VentaServicioImpl implements VentaServicio {

	// TODO IMPLEMENTAR LOGS
	// private static Logger log = Logger.getLogger(VentaServicioImpl.class);

	/** The venta dao. */
	@Inject
	private VentaDao ventaDao;

	/** The instrumento dao. */
	@Inject
	private InstrumentoDao instrumentoDao;

	/** The usuario dao. */
	@Inject
	private UsuarioDao usuarioDao;

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<VentaDTO> get() throws Exception {
		return ObjectMapperUtils.mapAll(ventaDao.get(), VentaDTO.class);
	}

	/**
	 * Gets the.
	 *
	 * @param ventaId the venta id
	 * @return the venta DTO
	 * @throws Exception the exception
	 */
	@Override
	public VentaDTO get(Integer ventaId) throws Exception {
		return ObjectMapperUtils.map(ventaDao.get(ventaId), new VentaDTO());
	}

	/**
	 * Gets the.
	 *
	 * @param usuario the usuario
	 * @return the list
	 * @throws Exception the exception
	 */
	@Override
	public List<VentaDTO> get(UsuarioDTO usuario) throws Exception {
		return ObjectMapperUtils.mapAll(ventaDao.get(ObjectMapperUtils.map(usuario, new Usuario())), VentaDTO.class);
	}

	/**
	 * Gets the.
	 *
	 * @param instrumento the instrumento
	 * @return the venta DTO
	 * @throws Exception the exception
	 */
	@Override
	public VentaDTO get(InstrumentoDTO instrumento) throws Exception {
		// TODO Auto-generated method stub
		return ObjectMapperUtils.map(ventaDao.get(ObjectMapperUtils.map(instrumento, new Instrumento())),
				VentaDTO.class);
	}

	/**
	 * Gets the deuda de una venta.
	 *
	 * @param ventaId the venta id
	 * @return the deuda de una venta
	 * @throws Exception the exception
	 */
	@Override
	public BigDecimal getDeudaDeUnaVenta(Integer ventaId) throws Exception {
		try {
			this.get(ventaId);
		} catch (Exception e) {
			throw new ObtenerException();
		}
		return ventaDao.calcularAdeudadoDeUnaVenta(ventaId);

	}

	/**
	 * Gets the deuda total.
	 *
	 * @return the deuda total
	 * @throws Exception the exception
	 */
	@Override
	public BigDecimal getDeudaTotal() throws Exception {
		return ventaDao.calcularAdeudadoDeTodasLasVentas();
	}

	/**
	 * Insert.
	 *
	 * @param nuevaVenta the nueva venta
	 * @return the venta DTO
	 * @throws Exception the exception
	 */
	@Override
	public VentaDTO insert(VentaDTO nuevaVenta) throws Exception {
		if (nuevaVenta.getCuotas() < 1) {
			throw new CantidadCuotaException();
		}

		Instrumento instrumento = instrumentoDao.get(nuevaVenta.getInstrumentoNumeroSerie());

		if (instrumento.getEstado().equals(EstadoInstrumento.VENDIDO))
			throw new InstrumentoVendidoException();

		VentaUtils.calcularPrecioDeVenta(nuevaVenta, instrumento.getPrecio());

		Venta unaVenta = ObjectMapperUtils.map(nuevaVenta,
				(nuevaVenta.getCuotas().equals(1)) ? new Contado() : new Financiada());

		unaVenta.setInstrumento(instrumento);
		unaVenta.setCliente(usuarioDao.get(nuevaVenta.getClienteUser()));
		unaVenta.setEstado(nuevaVenta.getCuotas().equals(1) ? EstadoVenta.FINALIZADA : EstadoVenta.ADEUDADA);
		VentaUtils.generarCuotas(nuevaVenta, unaVenta);

		nuevaVenta = ObjectMapperUtils.map(ventaDao.insert(unaVenta), nuevaVenta);
		instrumento.setEstado(EstadoInstrumento.VENDIDO);
		instrumentoDao.update(instrumento);
		return nuevaVenta;
	}

	/**
	 * Update.
	 *
	 * @param nuevaVenta the nueva venta
	 * @return the venta DTO
	 * @throws Exception the exception
	 */
	@Override
	public VentaDTO update(VentaDTO nuevaVenta) throws Exception {

		if (nuevaVenta.getCuotas() < 1) {

			throw new CantidadCuotaException();
		}

		Venta unaVenta = ObjectMapperUtils.map(nuevaVenta,
				(nuevaVenta.getCuotas().equals(1)) ? new Contado() : new Financiada());
		ventaDao.get(1);
		Instrumento instrumento = instrumentoDao.get(nuevaVenta.getInstrumentoNumeroSerie());
		unaVenta.setInstrumento(instrumento);
		unaVenta.setPrecio(instrumento.getPrecio());
		ventaDao.update(unaVenta);
		return nuevaVenta;
	}

	/**
	 * Delete.
	 *
	 * @param ventaAEliminar the venta A eliminar
	 * @throws Exception the exception
	 */
	@Override
	public void delete(VentaDTO ventaAEliminar) throws Exception {
		Venta unaVenta = ObjectMapperUtils.map(ventaAEliminar, new Contado());

		Instrumento instrumento = instrumentoDao.get(ventaAEliminar.getInstrumentoNumeroSerie());
		instrumento.setEstado(EstadoInstrumento.DISPONIBLE);
		instrumentoDao.update(instrumento);
		ventaDao.delete(unaVenta);

	}

}
