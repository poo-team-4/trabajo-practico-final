package ar.edu.unju.fi.tpfinal.modelo.factory.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;

import org.apache.log4j.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import ar.edu.unju.fi.tpfinal.exepciones.InformeException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Contado;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Financiada;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.modelo.factory.Informe;
import ar.edu.unju.fi.tpfinal.modelo.factory.InformePDF;
import ar.edu.unju.fi.tpfinal.util.EstadoCuota;
import ar.edu.unju.fi.tpfinal.util.VentaUtils;

/**
 * The Class InformePDFImpl.
 */
public class InformePDFImpl extends Informe implements InformePDF {

	/** The Constant COLOR_BLUE. */
	private static final BaseColor COLOR_BLUE = new BaseColor(47, 117, 181);
	
	/** The Constant FONT_HEADER. */
	private static final Font FONT_HEADER = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD, COLOR_BLUE);
	
	/** The Constant FONT_TABLE_HEADER. */
	private static final Font FONT_TABLE_HEADER = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD, BaseColor.WHITE);
	
	/** The Constant FONT_NORMAL. */
	private static final Font FONT_NORMAL = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);

	/** The log. */
	private static Logger log = Logger.getLogger(InformePDFImpl.class);
	
	/** The documento pdf. */
	private Document documentoPdf = null;
	
	/** The venta. */
	private Venta venta;
	
	/** The pagado. */
	private BigDecimal pagado = new BigDecimal(0.0);

	/**
	 * Instantiates a new informe PDF impl.
	 *
	 * @throws InformeException the informe exception
	 */
	public InformePDFImpl() throws InformeException {
		documentoPdf = new Document();
		try {
			PdfWriter.getInstance(documentoPdf,
					new FileOutputStream(new File(System.getProperty("user.dir"), this.getNombreDelArchivo() + ".pdf")));
		} catch (Exception e) {
			throw new InformeException("No se pudo inicializar el archivo pdf");
		}
		documentoPdf.open();
	}

	/**
	 * Generar PDF.
	 *
	 * @param ventaAImprimir the venta A imprimir
	 * @throws InformeException the informe exception
	 */
	@Override
	public void generarPDF(Venta ventaAImprimir) throws InformeException {
		log.info("Iniciando la descarga del archivo pdf.");
		venta = ventaAImprimir;
		this.cagarCabecera();
		this.cargarInformacionGeneralDeVenta();
		this.cargarInformacionGeneralDelCliente();
		this.cargarInformacionGeneralDelInstrumento();
		this.cargarInformacionDeCuotas();
		this.cargarEstadoDeVenta();
		this.cargarFooter();
		documentoPdf.close();
		log.info("Fin la descarga del archivo pdf.");
	}

	/**
	 * Cagar cabecera.
	 *
	 * @throws InformeException the informe exception
	 */
	private void cagarCabecera() throws InformeException {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.addCell(createCelda("UNJU POO FINAL", FONT_HEADER));
		table.addCell(createCelda("VENTA", FONT_HEADER, Element.ALIGN_RIGHT));
		table.setSpacingAfter(20);
		try {
			documentoPdf.add(table);
		} catch (Exception e) {
			throw new InformeException("No se pudo cargar la cabecera en el documento PDF");
		}
	}

	/**
	 * Cargar informacion general de venta.
	 *
	 * @throws InformeException the informe exception
	 */
	private void cargarInformacionGeneralDeVenta() throws InformeException {
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.addCell(createCelda("Informe de venta generada con POI", FONT_NORMAL));
		table.addCell(createCeldaHeader("# VENTA", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("F. COMPRA", Element.ALIGN_CENTER));
		table.addCell(createCelda("Team 4", FONT_NORMAL));
		table.addCell(createCeldaValor(String.format("%07d", venta.getId()), Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(VentaUtils.formatDateToString(venta.getFecha()), Element.ALIGN_CENTER));
		table.setSpacingAfter(20);
		try {
			documentoPdf.add(table);
		} catch (Exception e) {
			throw new InformeException("No se pudo cargar la información general de la venta.");
		}
	}

	/**
	 * Cargar informacion general del cliente.
	 *
	 * @throws InformeException the informe exception
	 */
	private void cargarInformacionGeneralDelCliente() throws InformeException {
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.addCell(createCelda("VENDIDO A", FONT_NORMAL));
		table.addCell(createCeldaHeader("# CLIENTE", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("CONDICIÓN", Element.ALIGN_CENTER));
		table.addCell(createCelda(venta.getCliente().getNombreApellido(), FONT_NORMAL));
		table.addCell(createCeldaValor(String.format("%04d", venta.getCliente().getId()), Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(venta.getCuota().size() > 1 ? "FINANCIADA" : "CONTADO", Element.ALIGN_CENTER));
		table.addCell(createCelda(venta.getCliente().getEmail(), FONT_NORMAL));
		table.addCell(createCelda("", FONT_NORMAL));
		table.addCell(createCelda("", FONT_NORMAL));
		table.setSpacingAfter(20);
		try {
			documentoPdf.add(table);
		} catch (Exception e) {
			throw new InformeException("No se pudo cargar la información del cliente.");
		}
	}

	/**
	 * Cargar informacion general del instrumento.
	 *
	 * @throws InformeException the informe exception
	 */
	private void cargarInformacionGeneralDelInstrumento() throws InformeException {

		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.addCell(createCeldaHeader("INSTRUMENTO", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader(venta.getCuota().size() > 1 ? "INTERES" : "DESCUENTO", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("PRECIO USD", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("PRECIO FINAL ARS", Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(venta.getInstrumento().getDescripcion(), Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(VentaUtils.formatPorcentaje(
				venta.getCuota().size() > 1 ? ((Financiada) venta).getInteres() : ((Contado) venta).getDescuento()),
				Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(VentaUtils.formatImporteDolar(venta.getInstrumento().getPrecio()),
				Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(VentaUtils.formatImporte(venta.getPrecio()), Element.ALIGN_CENTER));
		table.setSpacingAfter(20);
		try {
			documentoPdf.add(table);
		} catch (Exception e) {
			throw new InformeException("No se pudo cargar la información del instrumento.");
		}
	}

	/**
	 * Cargar estado de venta.
	 *
	 * @throws InformeException the informe exception
	 */
	private void cargarEstadoDeVenta() throws InformeException {
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.addCell(createCeldaHeader("ESTADO DE VENTA", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("DEUDA", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("PAGADO", Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(venta.getEstado().toString(), Element.ALIGN_CENTER));
		table.addCell(
				createCeldaValor(VentaUtils.formatImporte(venta.getPrecio().subtract(pagado)), Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(VentaUtils.formatImporte(pagado), Element.ALIGN_CENTER));
		try {
			documentoPdf.add(table);
		} catch (Exception e) {
			throw new InformeException("No se pudo cargar el stado de la venta.");
		}
	}

	/**
	 * Cargar informacion de cuotas.
	 *
	 * @throws InformeException the informe exception
	 */
	private void cargarInformacionDeCuotas() throws InformeException {
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100);
		table.addCell(createCeldaHeader("# CUOTA", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("F. VTO", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("F. PAGO", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("ESTADO", Element.ALIGN_CENTER));
		table.addCell(createCeldaHeader("IMPORTE", Element.ALIGN_CENTER));
		for (Cuota cuota : venta.getCuota())
			this.cargarInformacionDeUnaCuota(cuota, table);
		table.setSpacingAfter(20);
		try {
			documentoPdf.add(table);
		} catch (Exception e) {
			throw new InformeException("No se pudo cargar información de las cuotas.");
		}
	}

	/**
	 * Cargar informacion de una cuota.
	 *
	 * @param cuota the cuota
	 * @param table the table
	 * @throws InformeException the informe exception
	 */
	private void cargarInformacionDeUnaCuota(Cuota cuota, PdfPTable table) throws InformeException {
		table.addCell(createCeldaValor(cuota.getNumeroCuota().toString(), Element.ALIGN_CENTER));
		table.addCell(
				createCeldaValor(VentaUtils.formatDateToString(cuota.getFechaVencimiento()), Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(VentaUtils.formatDateToString(cuota.getFechaPago()), Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(cuota.getEstado().toString(), Element.ALIGN_CENTER));
		table.addCell(createCeldaValor(VentaUtils.formatImporte(cuota.getImporte()), Element.ALIGN_CENTER));
		if (cuota.getEstado().equals(EstadoCuota.PAGADA))
			pagado = pagado.add(cuota.getImporte());
	}

	/**
	 * Cargar footer.
	 *
	 * @throws InformeException the informe exception
	 */
	private void cargarFooter() throws InformeException {
		PdfPTable table = new PdfPTable(1);
		table.setSpacingBefore(50);
		table.setWidthPercentage(100);
		table.addCell(createCeldaValor("Informe generado con ♥ de parte del", Element.ALIGN_CENTER));
		table.addCell(createCeldaValor("TEAM 4", Element.ALIGN_CENTER));
		try {
			documentoPdf.add(table);
		} catch (Exception e) {
			throw new InformeException("No se pudo cargar el footer.");
		}
	}

	/**
	 * Creates the celda header.
	 *
	 * @param texto the texto
	 * @param alineado the alineado
	 * @return the pdf P cell
	 */
	private PdfPCell createCeldaHeader(String texto, int alineado) {
		return createCelda(texto, FONT_TABLE_HEADER, alineado, COLOR_BLUE);
	}

	/**
	 * Creates the celda valor.
	 *
	 * @param texto the texto
	 * @param alineado the alineado
	 * @return the pdf P cell
	 */
	private PdfPCell createCeldaValor(String texto, int alineado) {
		return this.createCelda(texto, FONT_NORMAL, alineado, BaseColor.WHITE);
	}

	/**
	 * Creates the celda.
	 *
	 * @param texto the texto
	 * @param fuente the fuente
	 * @return the pdf P cell
	 */
	private PdfPCell createCelda(String texto, Font fuente) {
		return createCelda(texto, fuente, Element.ALIGN_LEFT, BaseColor.WHITE);
	}

	/**
	 * Creates the celda.
	 *
	 * @param texto the texto
	 * @param fuente the fuente
	 * @param alineado the alineado
	 * @return the pdf P cell
	 */
	private PdfPCell createCelda(String texto, Font fuente, int alineado) {
		return createCelda(texto, fuente, alineado, BaseColor.WHITE);
	}

	/**
	 * Creates the celda.
	 *
	 * @param texto the texto
	 * @param fuente the fuente
	 * @param alineado the alineado
	 * @param backgroundColor the background color
	 * @return the pdf P cell
	 */
	private PdfPCell createCelda(String texto, Font fuente, int alineado, BaseColor backgroundColor) {
		PdfPCell nuevaCelda = new PdfPCell(new Phrase(texto, fuente));
		nuevaCelda.setBorder(Rectangle.NO_BORDER);
		nuevaCelda.setHorizontalAlignment(alineado);
		nuevaCelda.setBackgroundColor(backgroundColor);
		;
		return nuevaCelda;
	}

}
