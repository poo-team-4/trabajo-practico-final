package ar.edu.unju.fi.tpfinal.modelo.dao;

import java.util.List;

import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Usuario;

/**
 * The Interface UsuarioDao.
 */
public interface UsuarioDao {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Usuario> get() throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param usuario the usuario
	 * @return the usuario
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Usuario get(Usuario usuario) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param nombreUsuario the nombre usuario
	 * @return the usuario
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Usuario get(String nombreUsuario) throws ObtenerException, Exception;

	/**
	 * Insert.
	 *
	 * @param usuario the usuario
	 * @return the usuario
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	Usuario insert(Usuario usuario) throws InsertarException, Exception;

	/**
	 * Update.
	 *
	 * @param usuario the usuario
	 * @return the usuario
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	Usuario update(Usuario usuario) throws UpdateException, Exception;

	/**
	 * Delete.
	 *
	 * @param usuario the usuario
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	void delete(Usuario usuario) throws DeleteException, Exception;

}
