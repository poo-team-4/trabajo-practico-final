package ar.edu.unju.fi.tpfinal.modelo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpfinal.app.JPAUtil;
import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dao.MarcaDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Marca;

/**
 * The Class MarcaDaoImpl.
 *
 * @author TEAM 4
 */
@Repository
public class MarcaDaoImpl implements MarcaDao {

	/** The log. */
	private static Logger log = Logger.getLogger(MarcaDaoImpl.class);

	/** The entity. */
	private EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	/**
	 * Gets the.
	 *
	 * @return a list of Marcas
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public List<Marca> get() throws ObtenerException, Exception {
		log.info("Iniciando la recolección de una lista de Marcas de instrumentos.");
		List<Marca> marcas = new ArrayList<Marca>();
		CriteriaQuery<Marca> criteria = entity.getCriteriaBuilder().createQuery(Marca.class);
		criteria.select(criteria.from(Marca.class));

		try {
			marcas = entity.createQuery(criteria).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}

		log.debug(marcas);
		return marcas;
	}

	/**
	 * Gets the.
	 *
	 * @param marca the marca
	 * @return an Marca if it exists.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Marca get(Marca marca) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de una Marca.");
		Marca marcaEncontrada = entity.find(Marca.class, marca.getId());
		if (marcaEncontrada == null) {
			throw new ObtenerException();
		}

		log.debug(marcaEncontrada);
		return marcaEncontrada;
	}

	/**
	 * Gets the.
	 *
	 * @param nombreMarca the nombre marca
	 * @return an Marca if it exists.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */

	public Marca get(String nombreMarca) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de una Marca por su nombre: " + nombreMarca);
		Marca marcaEncontrada = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Marca> criteriaQuery = criteriaBuilder.createQuery(Marca.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Marca.class).get("nombre"), nombreMarca));
		try {
			marcaEncontrada = entity.createQuery(criteriaQuery).getSingleResult();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}

		log.debug(marcaEncontrada);
		return marcaEncontrada;
	}

	/**
	 * Insert.
	 *
	 * @param marca the marca
	 * @return the marca
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	public Marca insert(Marca marca) throws InsertarException, Exception {
		log.info("Añadiendo una nueva marca a la base de datos.");
		log.debug(marca);
		entity.getTransaction().begin();
		try {
			entity.persist(marca);
			entity.flush();
			log.info("Marca añadida con éxito.");
		} catch (Exception e) {
			log.error("No se pudo agregar la marca a la base de datos: " + e.getMessage());
			throw new InsertarException(e.getMessage());
		}
		entity.getTransaction().commit();
		return marca;
	}

	/**
	 * Update.
	 *
	 * @param marca the marca
	 * @return the marca
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	public Marca update(Marca marca) throws UpdateException, Exception {
		log.info("Actualizando una marca.");
		log.debug(marca);
		entity.getTransaction().begin();
		try {
			entity.merge(marca);
			entity.flush();
			log.info("Marca actualizada con éxito.");
		} catch (Exception e) {
			log.error("No se pudo actualizar la marca en la base de datos.: " + e.getMessage());
			throw new UpdateException(e.getMessage());
		}
		entity.getTransaction().commit();
		return marca;
	}

	/**
	 * Delete.
	 *
	 * @param marca the marca
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	public void delete(Marca marca) throws DeleteException, Exception {
		log.info("Eliminando una marca.");
		log.debug(marca);
		entity.getTransaction().begin();
		try {
			entity.remove(marca);
			log.info("Marca eliminada con éxito.");
		} catch (Exception e) {
			log.error("No se pudo eliminar la marca de la base de datos: " + e.getMessage());
			throw new DeleteException(e.getMessage());
		}
		entity.getTransaction().commit();

	}

}
