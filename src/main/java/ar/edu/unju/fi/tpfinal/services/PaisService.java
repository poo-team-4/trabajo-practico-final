/**
 * 
 */
package ar.edu.unju.fi.tpfinal.services;

import java.util.List;

import ar.edu.unju.fi.tpfinal.modelo.dto.PaisDTO;

/**
 * The Interface PaisService.
 *
 * @author TEAM 4
 */
public interface PaisService {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	List<PaisDTO> get() throws Exception;

	/**
	 * Gets the.
	 *
	 * @param nombre the nombre
	 * @return the pais DTO
	 * @throws Exception the exception
	 */
	PaisDTO get(String nombre) throws Exception;

	/**
	 * Insert.
	 *
	 * @param pais the pais
	 * @return the pais DTO
	 * @throws Exception the exception
	 */
	PaisDTO insert(PaisDTO pais) throws Exception;

	/**
	 * Update.
	 *
	 * @param pais the pais
	 * @return the pais DTO
	 * @throws Exception the exception
	 */
	PaisDTO update(PaisDTO pais) throws Exception;

	/**
	 * Delete.
	 *
	 * @param pais the pais
	 * @return the pais DTO
	 * @throws Exception the exception
	 */
	PaisDTO delete(PaisDTO pais) throws Exception;

}
