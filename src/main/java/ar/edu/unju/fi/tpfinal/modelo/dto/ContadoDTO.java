package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;

/**
 * The Class ContadoDTO.
 */
public class ContadoDTO extends VentaDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The descuento. */
	private Double descuento;

	/**
	 * Instantiates a new contado DTO.
	 */
	public ContadoDTO() {
	}

	/**
	 * Instantiates a new contado DTO.
	 *
	 * @param descuento the descuento
	 */
	public ContadoDTO(Double descuento) {
		super();
		this.descuento = descuento;
	}

	/**
	 * Gets the descuento.
	 *
	 * @return the descuento
	 */
	public Double getDescuento() {
		return descuento;
	}

	/**
	 * Sets the descuento.
	 *
	 * @param descuento the new descuento
	 */
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ContadoDTO [descuento=" + descuento + "]";
	}

}
