package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;

/**
 * The Class FinanciadaDTO.
 */
public class FinanciadaDTO extends VentaDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The interes. */
	private Double interes;

	/**
	 * Instantiates a new financiada DTO.
	 */
	public FinanciadaDTO() {
	}

	/**
	 * Instantiates a new financiada DTO.
	 *
	 * @param interes the interes
	 */
	public FinanciadaDTO(Double interes) {
		super();
		this.interes = interes;
	}

	/**
	 * Gets the descuento.
	 *
	 * @return the descuento
	 */
	public Double getDescuento() {
		return interes;
	}

	/**
	 * Sets the descuento.
	 *
	 * @param descuento the new descuento
	 */
	public void setDescuento(Double descuento) {
		this.interes = descuento;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ContadoDTO [descuento=" + interes + "]";
	}

}
