/**
 * 
 */
package ar.edu.unju.fi.tpfinal.modelo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpfinal.app.JPAUtil;
import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dao.PaisDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Pais;

/**
 * The Class PaisDaoImpl.
 *
 * @author TEAM 4
 */
@Repository
public class PaisDaoImpl implements PaisDao {

	/** The log. */
	private static Logger log = Logger.getLogger(PaisDaoImpl.class);

	/** The entity. */
	private EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	/**
	 * Gets the.
	 *
	 * @return a list of Paises
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public List<Pais> get() throws ObtenerException, Exception {
		log.info("Iniciando la recolección de una lista de Paises de instrumentos.");
		List<Pais> paises = new ArrayList<Pais>();
		CriteriaQuery<Pais> criteria = entity.getCriteriaBuilder().createQuery(Pais.class);
		criteria.select(criteria.from(Pais.class));
		try {
			paises = entity.createQuery(criteria).getResultList();

		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(paises);
		return paises;
	}

	/**
	 * Gets the.
	 *
	 * @param pais the pais
	 * @return an Pais if it exists.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Pais get(Pais pais) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de una Pais.");
		Pais paisEncontrado = entity.find(Pais.class, pais.getId());
		if (paisEncontrado == null) {
			throw new ObtenerException();
		}

		log.debug(paisEncontrado);
		return paisEncontrado;
	}

	/**
	 * Gets the.
	 *
	 * @param nombrePais the nombre pais
	 * @return an Pais if it exists.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Pais get(String nombrePais) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de una Pais por su nombre: " + nombrePais);
		Pais paisEncontrado = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Pais> criteriaQuery = criteriaBuilder.createQuery(Pais.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Pais.class).get("nombre"), nombrePais));

		try {
			paisEncontrado = entity.createQuery(criteriaQuery).getSingleResult();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(paisEncontrado);
		return paisEncontrado;
	}

	/**
	 * Insert.
	 *
	 * @param pais the pais
	 * @return the pais
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	public Pais insert(Pais pais) throws InsertarException, Exception {
		log.info("Añadiendo un nuevo país a la base de datos.");
		log.debug(pais);
		entity.getTransaction().begin();
		try {
			entity.persist(pais);
			entity.flush();
			log.info("País añadido con éxito.");
		} catch (Exception e) {
			log.error("No se pudo agregar el país a la base de datos: " + e.getMessage());
			throw new InsertarException(e.getMessage());
		}
		entity.getTransaction().commit();
		return pais;
	}

	/**
	 * Update.
	 *
	 * @param pais the pais
	 * @return the pais
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	public Pais update(Pais pais) throws UpdateException, Exception {
		log.info("Actualizando un país.");
		log.debug(pais);
		entity.getTransaction().begin();
		try {
			entity.merge(pais);
			entity.flush();
			log.info("País actualizado con éxito.");
		} catch (Exception e) {
			log.error("No se pudo actualizar el país en la base de datos: " + e.getMessage());
			throw new UpdateException(e.getMessage());
		}
		entity.getTransaction().commit();
		return pais;
	}

	/**
	 * Delete.
	 *
	 * @param pais the pais
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	public void delete(Pais pais) throws DeleteException, Exception {
		log.info("Eliminando un país.");
		log.debug(pais);
		entity.getTransaction().begin();
		try {
			entity.remove(pais);
			log.info("País eliminado con éxito.");
		} catch (Exception e) {
			log.error("No se pudo eliminar el país de la base de datos: " + e.getMessage());
			throw new DeleteException(e.getMessage());
		}
		entity.getTransaction().commit();

	}

}
