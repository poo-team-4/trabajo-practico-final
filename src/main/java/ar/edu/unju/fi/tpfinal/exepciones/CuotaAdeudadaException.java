package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class CuotaAdeudadaException.
 */
public class CuotaAdeudadaException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 843893439930L;

	/**
	 * Instantiates a new cuota adeudada exception.
	 */
	public CuotaAdeudadaException() {
		super("No se encontraron cuotas adeudadas.");
	}
}
