package ar.edu.unju.fi.tpfinal.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Obtiene una instancia de un Bean del contexto de Spring Framework.
 *
 * @author TEAM 4
 */
public class AppBean {

	/** The context. */
	private static ApplicationContext context;

	/**
	 * Gets the bean.
	 *
	 * @param <T>  the generic type
	 * @param type the type
	 * @return the bean
	 */
	public static <T> T getBean(Class<T> type) {
		context = new ClassPathXmlApplicationContext(new String[] { "Spring-AutoScan.xml" });
		return context.getBean(type);
	}
}
