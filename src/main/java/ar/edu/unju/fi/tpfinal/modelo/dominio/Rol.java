package ar.edu.unju.fi.tpfinal.modelo.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Rol.
 */
@Entity
@Table(name = "roles")
public class Rol {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer id;

	/** The descripcion. */
	@Column
	private String descripcion;

	/**
	 * Instantiates a new rol.
	 */
	public Rol() {
	}

	/**
	 * Instantiates a new rol.
	 *
	 * @param descripcion the descripcion
	 */
	public Rol(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Instantiates a new rol.
	 *
	 * @param id          the id
	 * @param descripcion the descripcion
	 */
	public Rol(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Rol [id=" + id + ", descripcion=" + descripcion + "]";
	}

}
