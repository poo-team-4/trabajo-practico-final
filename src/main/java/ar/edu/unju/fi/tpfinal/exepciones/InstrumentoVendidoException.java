package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class InstrumentoVendidoException.
 */
public class InstrumentoVendidoException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 34748485933L;

	/**
	 * Instantiates a new instrumento vendido exception.
	 */
	public InstrumentoVendidoException() {

		super("Error: el instrumento ya se encuentra vendido.");
	}
}
