/**
 * 
 */
package ar.edu.unju.fi.tpfinal.app;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * The Class JPAUtil.
 *
 * @author TEAM 4
 */
public class JPAUtil {

	/** The Constant PERSISTENCE_UNIT_NAME. */
	private static final String PERSISTENCE_UNIT_NAME = "PERSISTENCE";

	/** The factory. */
	private static EntityManagerFactory factory;

	/**
	 * Gets the entity manager factory.
	 *
	 * @return EntityManagerFactory
	 */
	public static EntityManagerFactory getEntityManagerFactory() {
		if (factory == null) {
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		return factory;
	}

	/**
	 * Shutdown.
	 */
	public static void shutdown() {
		if (factory != null) {
			factory.close();
		}
	}

}
