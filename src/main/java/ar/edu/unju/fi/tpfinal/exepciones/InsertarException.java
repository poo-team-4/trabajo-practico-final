package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class InsertarException.
 */
public class InsertarException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 87493503839L;

	/**
	 * Instantiates a new insertar exception.
	 *
	 * @param mensaje the mensaje
	 */
	public InsertarException(String mensaje) {
		super("No se pudo ingresar los datos a la base de datos" + mensaje);
	}

}
