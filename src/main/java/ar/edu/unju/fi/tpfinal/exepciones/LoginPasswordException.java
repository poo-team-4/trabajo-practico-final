package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class LoginPasswordException.
 */
public class LoginPasswordException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 38292848403L;

	/**
	 * Instantiates a new login password exception.
	 */
	public LoginPasswordException() {
		super("Contraseña incorrecta");
	}
}
