/**
 * 
 */
package ar.edu.unju.fi.tpfinal.services;

import ar.edu.unju.fi.tpfinal.exepciones.CuotaDeVentaException;
import ar.edu.unju.fi.tpfinal.exepciones.PagarCuotaException;
import ar.edu.unju.fi.tpfinal.modelo.dto.CuotaDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;

/**
 * The Interface CuotaService.
 *
 * @author TEAM 4
 */
public interface CuotaService {

	/**
	 * Pagar cuota.
	 *
	 * @param cuotaAPagar the cuota A pagar
	 * @return the cuota DTO
	 * @throws PagarCuotaException the pagar cuota exception
	 * @throws Exception the exception
	 */
	CuotaDTO pagarCuota(CuotaDTO cuotaAPagar) throws PagarCuotaException, Exception;

	/**
	 * Obtener proxima cuota de venta.
	 *
	 * @param venta the venta
	 * @return the cuota DTO
	 * @throws CuotaDeVentaException the cuota de venta exception
	 * @throws Exception             the exception
	 */
	CuotaDTO obtenerProximaCuotaDeVenta(VentaDTO venta) throws CuotaDeVentaException, Exception;

}
