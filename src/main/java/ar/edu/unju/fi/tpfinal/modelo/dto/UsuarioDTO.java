package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;

/**
 * The Class UsuarioDTO.
 */
public class UsuarioDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The user. */
	private String user;

	/** The password. */
	private String password;

	/** The nombre apellido. */
	private String nombreApellido;

	/** The email. */
	private String email;

	/** The estado. */
	private String estado;

	/** The rol. */
	private String rolDescripcion;

	/**
	 * Instantiates a new usuario DTO.
	 */
	public UsuarioDTO() {
	}

	/**
	 * Instantiates a new usuario DTO.
	 *
	 * @param user           the user
	 * @param password       the password
	 * @param nombreApellido the nombre apellido
	 * @param email          the email
	 * @param estado         the estado
	 * @param rolDescripcion the rolDescripcion
	 */
	public UsuarioDTO(String user, String password, String nombreApellido, String email, String estado,
			String rolDescripcion) {
		this.user = user;
		this.password = password;
		this.nombreApellido = nombreApellido;
		this.email = email;
		this.estado = estado;
		this.rolDescripcion = rolDescripcion;
	}

	/**
	 * Instantiates a new usuario DTO.
	 *
	 * @param id             the id
	 * @param user           the user
	 * @param password       the password
	 * @param nombreApellido the nombre apellido
	 * @param email          the email
	 * @param estado         the estado
	 * @param rolDescripcion the rolDescripcion
	 */
	public UsuarioDTO(Integer id, String user, String password, String nombreApellido, String email, String estado,
			String rolDescripcion) {
		this.id = id;
		this.user = user;
		this.password = password;
		this.nombreApellido = nombreApellido;
		this.email = email;
		this.estado = estado;
		this.rolDescripcion = rolDescripcion;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the nombre apellido.
	 *
	 * @return the nombreApellido
	 */
	public String getNombreApellido() {
		return nombreApellido;
	}

	/**
	 * Sets the nombre apellido.
	 *
	 * @param nombreApellido the nombreApellido to set
	 */
	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * Gets the rol descripcion.
	 *
	 * @return the rolDescripcion
	 */
	public String getRolDescripcion() {
		return rolDescripcion;
	}

	/**
	 * Sets the rol descripcion.
	 *
	 * @param rolDescripcion the rolDescripcion to set
	 */
	public void setRolDescripcion(String rolDescripcion) {
		this.rolDescripcion = rolDescripcion;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", user=" + user + ", password=" + password + ", nombreApellido=" + nombreApellido
				+ ", email=" + email + ", estado=" + estado + ", rolDescripcion=" + rolDescripcion + "]";
	}

}
