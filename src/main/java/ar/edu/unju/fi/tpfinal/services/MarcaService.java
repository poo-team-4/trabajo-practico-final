/**
 * 
 */
package ar.edu.unju.fi.tpfinal.services;

import java.util.List;

import ar.edu.unju.fi.tpfinal.modelo.dto.MarcaDTO;

/**
 * The Interface MarcaService.
 *
 * @author TEAM 4
 */
public interface MarcaService {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	List<MarcaDTO> get() throws Exception;

	/**
	 * Gets the.
	 *
	 * @param nombre the nombre
	 * @return the marca DTO
	 * @throws Exception the exception
	 */
	MarcaDTO get(String nombre) throws Exception;

	/**
	 * Insert.
	 *
	 * @param marca the marca
	 * @return the marca DTO
	 * @throws Exception the exception
	 */
	MarcaDTO insert(MarcaDTO marca) throws Exception;

	/**
	 * Update.
	 *
	 * @param marca the marca
	 * @return the marca DTO
	 * @throws Exception the exception
	 */
	MarcaDTO update(MarcaDTO marca) throws Exception;

	/**
	 * Delete.
	 *
	 * @param marca the marca
	 * @return the marca DTO
	 * @throws Exception the exception
	 */
	MarcaDTO delete(MarcaDTO marca) throws Exception;

}
