package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class UpdateException.
 */
public class UpdateException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 74843940303L;

	/**
	 * Instantiates a new update exception.
	 *
	 * @param mensaje the mensaje
	 */
	public UpdateException(String mensaje) {
		super("No se pudo actualizar los datos de la base de datos" + mensaje);
	}

	/**
	 * Instantiates a new update exception.
	 */
	public UpdateException() {
		super("se requiere una cantidad de cuotas mayor o igual a uno (1).");
	}

}
