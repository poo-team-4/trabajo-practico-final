/**
 * 
 */
package ar.edu.unju.fi.tpfinal.modelo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpfinal.app.JPAUtil;
import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dao.InstrumentoDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;

/**
 * The Class InstrumentoDaoImpl.
 *
 * @author TEAM 4
 */
@Repository
public class InstrumentoDaoImpl implements InstrumentoDao {

	/** The log. */
	private static Logger log = Logger.getLogger(InstrumentoDaoImpl.class);

	/** The entity. */
	private EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	/**
	 * Gets the.
	 *
	 * @return a list of Instrumentos
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public List<Instrumento> get() throws ObtenerException, Exception {
		log.info("Iniciando la recolección de una lista de Instrumentos");
		List<Instrumento> instrumentos = new ArrayList<Instrumento>();
		CriteriaQuery<Instrumento> criteria = entity.getCriteriaBuilder().createQuery(Instrumento.class);
		criteria.select(criteria.from(Instrumento.class));
		try {
			instrumentos = entity.createQuery(criteria).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(instrumentos);
		return instrumentos;
	}

	/**
	 * Gets the.
	 *
	 * @param instrumento the instrumento
	 * @return an Instrumento if it exists.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Instrumento get(Instrumento instrumento) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de un Instrumento");
		Instrumento instrumentoEncontrado = entity.find(Instrumento.class, instrumento.getId());
		if (instrumentoEncontrado == null) {
			throw new ObtenerException();
		}

		log.debug(instrumentoEncontrado);
		return instrumentoEncontrado;
	}

	/**
	 * Gets the.
	 *
	 * @param numeroSerie the numero serie
	 * @return an Instrumento if it exists.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Instrumento get(String numeroSerie) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de un Instrumento por número de serie: " + numeroSerie);
		Instrumento instrumentoEncontrado = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Instrumento> criteriaQuery = criteriaBuilder.createQuery(Instrumento.class);
		criteriaQuery
				.where(criteriaBuilder.equal(criteriaQuery.from(Instrumento.class).get("numeroSerie"), numeroSerie));
		try {
			instrumentoEncontrado = entity.createQuery(criteriaQuery).getSingleResult();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(instrumentoEncontrado);
		return instrumentoEncontrado;
	}

	/**
	 * Insert.
	 *
	 * @param instrumento a agregar
	 * @return the instrumento
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	public Instrumento insert(Instrumento instrumento) throws InsertarException, Exception {
		log.info("Añadiendo un nuevo instrumento a la base de datos.");
		log.debug(instrumento);
		entity.getTransaction().begin();
		try {
			entity.persist(instrumento);
			entity.flush();
			log.info("Instrumento agregado con éxito.");
		} catch (Exception e) {
			log.error("No se pudo agregar el instrumento a la base de datos: " + e.getMessage());
			throw new InsertarException(e.getMessage());
		}
		entity.getTransaction().commit();
		return instrumento;
	}

	/**
	 * Update.
	 *
	 * @param instrumento a modificar
	 * @return the instrumento
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	public Instrumento update(Instrumento instrumento) throws UpdateException, Exception {
		log.info("Actualizando un instrumento");
		log.debug(instrumento);
		entity.getTransaction().begin();
		try {
			entity.merge(instrumento);
			entity.flush();
			log.info("Instrumento modificado con éxito.");
		} catch (Exception e) {
			log.error("No se pudo actualizar el instrumento en la base de datos: " + e.getMessage());
			throw new UpdateException(e.getMessage());
		}
		entity.getTransaction().commit();
		return instrumento;
	}

	/**
	 * Delete.
	 *
	 * @param instrumento a eliminar
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	public void delete(Instrumento instrumento) throws DeleteException, Exception {
		log.info("Eliminando un instrumento");
		log.debug(instrumento);
		entity.getTransaction().begin();
		try {
			if (entity.contains(instrumento)) {
				entity.remove(instrumento);
			} else {
				Instrumento ee = entity.getReference(instrumento.getClass(), instrumento.getId());
				entity.remove(ee);
			}
			log.info("Instrumento eliminado con éxito.");
		} catch (Exception e) {
			log.error("No se pudo eliminar el instrumento de la base de datos: " + e.getMessage());
			throw new DeleteException(e.getMessage());
		}
		entity.getTransaction().commit();
	}

}
