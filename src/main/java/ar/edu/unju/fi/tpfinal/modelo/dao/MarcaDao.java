package ar.edu.unju.fi.tpfinal.modelo.dao;

import java.util.List;

import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Marca;

/**
 * The Interface MarcaDao.
 *
 * @author TEAM 4
 */
public interface MarcaDao {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	List<Marca> get() throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param marca the marca
	 * @return the marca
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Marca get(Marca marca) throws ObtenerException, Exception;

	/**
	 * Gets the.
	 *
	 * @param nombreMarca the nombre marca
	 * @return the marca
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	Marca get(String nombreMarca) throws ObtenerException, Exception;

	/**
	 * Insert.
	 *
	 * @param marca the marca
	 * @return the marca
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	Marca insert(Marca marca) throws InsertarException, Exception;

	/**
	 * Update.
	 *
	 * @param marca the marca
	 * @return the marca
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	Marca update(Marca marca) throws UpdateException, Exception;

	/**
	 * Delete.
	 *
	 * @param marca the marca
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	void delete(Marca marca) throws DeleteException, Exception;

}
