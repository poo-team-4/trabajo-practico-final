package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;
import java.util.List;

/**
 * The Class VentaInformeDTO.
 */
public class VentaInformeDTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private String id;

	/** The precio. */
	private String precio;
	
	/** The fecha. */
	private String fecha;
	
	/** The descuento. */
	private String descuento;
	
	/** The interes. */
	private String interes;
	
	/** The cuota. */
	private List<CuotaDTO> cuota;
	
	/** The estado. */
	private String estado;

	/** The instrumento descripcion. */
	private String instrumentoDescripcion;
	
	/** The instrumento precio. */
	private String instrumentoPrecio;

	/** The cliente id. */
	private String clienteId;
	
	/** The cliente nombre apellido. */
	private String clienteNombreApellido;
	
	/** The cliente email. */
	private String clienteEmail;

	/**
	 * Instantiates a new venta informe DTO.
	 */
	public VentaInformeDTO() {
	}

	/**
	 * Instantiates a new venta informe DTO.
	 *
	 * @param precio the precio
	 * @param fecha the fecha
	 * @param descuento the descuento
	 * @param interes the interes
	 * @param cuota the cuota
	 * @param estado the estado
	 * @param instrumentoDescripcion the instrumento descripcion
	 * @param instrumentoPrecio the instrumento precio
	 * @param clienteId the cliente id
	 * @param clienteNombreApellido the cliente nombre apellido
	 * @param clienteEmail the cliente email
	 */
	public VentaInformeDTO(String precio, String fecha, String descuento, String interes, List<CuotaDTO> cuota,
			String estado, String instrumentoDescripcion, String instrumentoPrecio, String clienteId,
			String clienteNombreApellido, String clienteEmail) {
		this.precio = precio;
		this.fecha = fecha;
		this.descuento = descuento;
		this.interes = interes;
		this.cuota = cuota;
		this.estado = estado;
		this.instrumentoDescripcion = instrumentoDescripcion;
		this.instrumentoPrecio = instrumentoPrecio;
		this.clienteId = clienteId;
		this.clienteNombreApellido = clienteNombreApellido;
		this.clienteEmail = clienteEmail;
	}

	/**
	 * Instantiates a new venta informe DTO.
	 *
	 * @param id the id
	 * @param precio the precio
	 * @param fecha the fecha
	 * @param descuento the descuento
	 * @param interes the interes
	 * @param cuota the cuota
	 * @param estado the estado
	 * @param instrumentoDescripcion the instrumento descripcion
	 * @param instrumentoPrecio the instrumento precio
	 * @param clienteId the cliente id
	 * @param clienteNombreApellido the cliente nombre apellido
	 * @param clienteEmail the cliente email
	 */
	public VentaInformeDTO(String id, String precio, String fecha, String descuento, String interes,
			List<CuotaDTO> cuota, String estado, String instrumentoDescripcion, String instrumentoPrecio,
			String clienteId, String clienteNombreApellido, String clienteEmail) {
		this.id = id;
		this.precio = precio;
		this.fecha = fecha;
		this.descuento = descuento;
		this.interes = interes;
		this.cuota = cuota;
		this.estado = estado;
		this.instrumentoDescripcion = instrumentoDescripcion;
		this.instrumentoPrecio = instrumentoPrecio;
		this.clienteId = clienteId;
		this.clienteNombreApellido = clienteNombreApellido;
		this.clienteEmail = clienteEmail;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the precio.
	 *
	 * @return the precio
	 */
	public String getPrecio() {
		return precio;
	}

	/**
	 * Sets the precio.
	 *
	 * @param precio the precio to set
	 */
	public void setPrecio(String precio) {
		this.precio = precio;
	}

	/**
	 * Gets the fecha.
	 *
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * Sets the fecha.
	 *
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * Gets the descuento.
	 *
	 * @return the descuento
	 */
	public String getDescuento() {
		return descuento;
	}

	/**
	 * Sets the descuento.
	 *
	 * @param descuento the descuento to set
	 */
	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

	/**
	 * Gets the interes.
	 *
	 * @return the interes
	 */
	public String getInteres() {
		return interes;
	}

	/**
	 * Sets the interes.
	 *
	 * @param interes the interes to set
	 */
	public void setInteres(String interes) {
		this.interes = interes;
	}

	/**
	 * Gets the cuota.
	 *
	 * @return the cuota
	 */
	public List<CuotaDTO> getCuota() {
		return cuota;
	}

	/**
	 * Sets the cuota.
	 *
	 * @param cuota the cuota to set
	 */
	public void setCuota(List<CuotaDTO> cuota) {
		this.cuota = cuota;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * Gets the instrumento descripcion.
	 *
	 * @return the instrumentoDescripcion
	 */
	public String getInstrumentoDescripcion() {
		return instrumentoDescripcion;
	}

	/**
	 * Sets the instrumento descripcion.
	 *
	 * @param instrumentoDescripcion the instrumentoDescripcion to set
	 */
	public void setInstrumentoDescripcion(String instrumentoDescripcion) {
		this.instrumentoDescripcion = instrumentoDescripcion;
	}

	/**
	 * Gets the instrumento precio.
	 *
	 * @return the instrumentoPrecio
	 */
	public String getInstrumentoPrecio() {
		return instrumentoPrecio;
	}

	/**
	 * Sets the instrumento precio.
	 *
	 * @param instrumentoPrecio the instrumentoPrecio to set
	 */
	public void setInstrumentoPrecio(String instrumentoPrecio) {
		this.instrumentoPrecio = instrumentoPrecio;
	}

	/**
	 * Gets the cliente id.
	 *
	 * @return the clienteId
	 */
	public String getClienteId() {
		return clienteId;
	}

	/**
	 * Sets the cliente id.
	 *
	 * @param clienteId the clienteId to set
	 */
	public void setClienteId(String clienteId) {
		this.clienteId = clienteId;
	}

	/**
	 * Gets the cliente nombre apellido.
	 *
	 * @return the clienteNombreApellido
	 */
	public String getClienteNombreApellido() {
		return clienteNombreApellido;
	}

	/**
	 * Sets the cliente nombre apellido.
	 *
	 * @param clienteNombreApellido the clienteNombreApellido to set
	 */
	public void setClienteNombreApellido(String clienteNombreApellido) {
		this.clienteNombreApellido = clienteNombreApellido;
	}

	/**
	 * Gets the cliente email.
	 *
	 * @return the clienteEmail
	 */
	public String getClienteEmail() {
		return clienteEmail;
	}

	/**
	 * Sets the cliente email.
	 *
	 * @param clienteEmail the clienteEmail to set
	 */
	public void setClienteEmail(String clienteEmail) {
		this.clienteEmail = clienteEmail;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "VentaInformeDTO{" + "id='" + id + '\'' + ", precio='" + precio + '\'' + ", fecha='" + fecha + '\''
				+ ", descuento='" + descuento + '\'' + ", interes='" + interes + '\'' + ", cuota=" + cuota
				+ ", estado='" + estado + '\'' + ", instrumentoDescripcion='" + instrumentoDescripcion + '\''
				+ ", instrumentoPrecio='" + instrumentoPrecio + '\'' + ", clienteId='" + clienteId + '\''
				+ ", clienteNombreApellido='" + clienteNombreApellido + '\'' + ", clienteEmail='" + clienteEmail + '\''
				+ '}';
	}
}
