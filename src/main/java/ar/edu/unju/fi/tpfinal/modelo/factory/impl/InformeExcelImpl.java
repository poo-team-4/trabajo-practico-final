package ar.edu.unju.fi.tpfinal.modelo.factory.impl;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.util.List;

import ar.edu.unju.fi.tpfinal.modelo.dto.CuotaDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaInformeDTO;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import ar.edu.unju.fi.tpfinal.exepciones.InformeException;
import ar.edu.unju.fi.tpfinal.modelo.factory.Informe;
import ar.edu.unju.fi.tpfinal.modelo.factory.InformeExcel;
import ar.edu.unju.fi.tpfinal.util.VentaUtils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

/**
 * The Class InformeExcelImpl.
 */
public class InformeExcelImpl extends Informe implements InformeExcel {

	/** The log. */
	private static Logger log = Logger.getLogger(InformeExcelImpl.class);

	/** The Constant PLANTILLA_INFORME_PATH. */
	private static final String PLANTILLA_INFORME_PATH = "/plantillaVenta.xls";
	
	/** The Constant MAX_NUMERO_DE_CUOTAS. */
	private static final Integer MAX_NUMERO_DE_CUOTAS = 12;
	
	/** The work book. */
	private HSSFWorkbook workBook;
	
	/** The hoja activa. */
	private HSSFSheet hojaActiva;
	
	/** The fila inicial de cuotas. */
	private Integer filaInicialDeCuotas = null;

	/**
	 * Instantiates a new informe excel impl.
	 *
	 * @throws InformeException the informe exception
	 */
	InformeExcelImpl() throws InformeException {
		try {
			workBook = new HSSFWorkbook(InformeExcelImpl.class.getResourceAsStream(PLANTILLA_INFORME_PATH));
		} catch (Exception e) {
			e.printStackTrace();
			throw new InformeException("No se pudo abrir el archivo plantilla xlsx");
		}
	}

	/**
	 * Generar excel.
	 *
	 * @param venta the venta
	 * @throws InformeException the informe exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void generarExcel(VentaInformeDTO venta) throws InformeException {
		this.venta = venta;
		this.hojaActiva = workBook.cloneSheet(0);
		workBook.setSheetName(workBook.getSheetIndex(this.hojaActiva), "Venta-" + venta.getId());
		try {
			for (PropertyDescriptor propertyDesc : Introspector.getBeanInfo(VentaInformeDTO.class)
					.getPropertyDescriptors()) {
				Object propertyValue = propertyDesc.getReadMethod().invoke(venta);
				if (propertyDesc.getName().equals("cuota")) {
					this.buscarNumeroDeFilaDeListadoDeCuota((List<CuotaDTO>) propertyValue);
					this.insertarCuotas((List<CuotaDTO>) propertyValue);
					this.eliminarFilasSobrantes();
				} else
					this.reemplazarValor("{" + propertyDesc.getName() + "}",
							propertyValue == null ? "" : propertyValue.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new InformeException("No se pudo reemplazar las celdas del excel.");
		}
	}

	/**
	 * Buscar numero de fila de listado de cuota.
	 *
	 * @param cuotas the cuotas
	 */
	private void buscarNumeroDeFilaDeListadoDeCuota(List<CuotaDTO> cuotas) {
		log.info("Rellenando cuotas");
		filaInicialDeCuotas = null;
		for (Row row : this.hojaActiva) {
			for (Cell cell : row) {
				if (!cell.getStringCellValue().equals("{LISTADO_DE_CUOTAS}"))
					continue;
				filaInicialDeCuotas = row.getRowNum();
			}
			if (filaInicialDeCuotas != null)
				break;
		}
	}

	/**
	 * Insertar cuotas.
	 *
	 * @param cuotas the cuotas
	 */
	private void insertarCuotas(List<CuotaDTO> cuotas) {
		for (int i = 0; i < cuotas.size(); i++) {
			for (Cell cell : this.hojaActiva.getRow(filaInicialDeCuotas + i)) {
				switch (cell.getStringCellValue()) {
				case "{cuotaEstado}":
					cell.setCellValue(cuotas.get(i).getEstado().toString());
					break;
				case "{cuotaFechaPago}":
					cell.setCellValue(VentaUtils.formatDateToString(cuotas.get(i).getFechaPago()));
					break;
				case "{cuotaImporte}":
					cell.setCellFormula(cuotas.get(i).getImporte().toString());
					break;
				case "{cuotaFechaVencimiento}":
					cell.setCellValue(VentaUtils.formatDateToString(cuotas.get(i).getFechaVencimiento()));
					break;
				case "{LISTADO_DE_CUOTAS}":
					cell.setCellValue(cuotas.get(i).getNumeroCuota().toString());
					break;
				default:
					cell.setBlank();
					break;
				}
			}
		}
	}

	/**
	 * Eliminar filas sobrantes.
	 */
	private void eliminarFilasSobrantes() {
		for (int i = venta.getCuota().size(); i < MAX_NUMERO_DE_CUOTAS; i++) {
			for (Cell cell : this.hojaActiva.getRow(filaInicialDeCuotas + i)) {
				cell.setBlank();
			}
		}
	}

	/**
	 * Reemplazar valor.
	 *
	 * @param buscar the buscar
	 * @param reemplazo the reemplazo
	 */
	private void reemplazarValor(final String buscar, final String reemplazo) {
		log.debug(buscar + " => " + reemplazo);
		for (Row row : this.hojaActiva) {
			for (Cell cell : row) {
				if (cell.getCellType().equals(CellType.FORMULA) || !cell.getStringCellValue().equals(buscar))
					continue;
				if (reemplazo.isBlank()) {
					cell.setBlank();
					continue;
				}
				if (NumberUtils.isParsable(reemplazo))
					cell.setCellFormula(reemplazo);
				else
					cell.setCellValue(reemplazo);
			}
		}
	}

	/**
	 * Guardar en disco.
	 *
	 * @throws InformeException the informe exception
	 */
	@Override
	public void guardarEnDisco() throws InformeException {
		workBook.removeSheetAt(0);
		if (this.workBook.getNumberOfSheets() > 1)
			this.setPrefijoNombreDeArchivo("VentasAdeudadas_");
		File fileInstance = new File(System.getProperty("user.dir"), this.getNombreDelArchivo() + ".xls");
		try (FileOutputStream salida = new FileOutputStream(fileInstance)) {
			HSSFFormulaEvaluator.evaluateAllFormulaCells(this.workBook);
			this.workBook.write(salida);
		} catch (IOException e) {
			throw new InformeException("No se pudo escribir el archivo.");
		}
	}

}
