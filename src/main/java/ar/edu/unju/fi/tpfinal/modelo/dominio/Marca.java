package ar.edu.unju.fi.tpfinal.modelo.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Marca.
 */
@Entity
@Table(name = "marcas")
public class Marca {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer id;

	/** The nombre. */
	@Column
	private String nombre;

	/**
	 * Instantiates a new marca.
	 */
	public Marca() {
	}

	/**
	 * Instantiates a new marca.
	 *
	 * @param nombre the nombre
	 */
	public Marca(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Instantiates a new marca.
	 *
	 * @param id     the id
	 * @param nombre the nombre
	 */
	public Marca(Integer id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Marca [id=" + id + ", nombre=" + nombre + "]";
	}

}
