package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class DescuentoException.
 */
public class DescuentoException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7437848483L;

	/**
	 * Instantiates a new descuento exception.
	 */
	public DescuentoException() {
		super("No se encontró el descuento por compra al contado");
	}
}
