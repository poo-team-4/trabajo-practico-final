package ar.edu.unju.fi.tpfinal.modelo.factory;

import java.util.Date;
import java.text.SimpleDateFormat;

import ar.edu.unju.fi.tpfinal.modelo.dto.VentaInformeDTO;

/**
 * The Class Informe.
 */
public abstract class Informe {

	/** The venta. */
	protected VentaInformeDTO venta = null;

	/** The prefijo nombre de archivo. */
	private String prefijoNombreDeArchivo = "Venta_";

	/**
	 * Gets the nombre del archivo.
	 *
	 * @return the nombre del archivo
	 */
	public String getNombreDelArchivo() {
		return prefijoNombreDeArchivo + new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
	}

	/**
	 * Sets the prefijo nombre de archivo.
	 *
	 * @param prefijoNombreDeArchivo the new prefijo nombre de archivo
	 */
	public void setPrefijoNombreDeArchivo(String prefijoNombreDeArchivo) {
		this.prefijoNombreDeArchivo = prefijoNombreDeArchivo;
	}

}
