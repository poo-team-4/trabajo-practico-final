package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class InteresCuotaException.
 */
public class InteresCuotaException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 32474849293L;

	/**
	 * Instantiates a new interes cuota exception.
	 */
	public InteresCuotaException() {
		super("No se encontró el interes equivalete a la cuota");
	}
}
