package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ar.edu.unju.fi.tpfinal.util.EstadoVenta;

/**
 * The Class VentaDTO.
 */
public class VentaDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The instrumento numero serie. */
	private String instrumentoNumeroSerie; // sera usado en la parte de implementacion

	/** The cliente user. */
	private String clienteUser;

	/** The precio. */
	private BigDecimal precio;

	/** The fecha. */
	private Date fecha;

	/** The cuotas. */
	private Integer cuotas; // sera utilizada para la cantidad de cuotas de la ventas

	/** The cuota id. */
	private List<String> cuotaId;

	/** The interes. */
	private Double interes;

	/** The descuento. */
	private Double descuento;

	/** The estado. */
	private EstadoVenta estado;

	/**
	 * Instantiates a new venta DTO.
	 */
	public VentaDTO() {

	}

	/**
	 * Instantiates a new venta DTO.
	 *
	 * @param instrumentoNumeroSerie the instrumento numero serie
	 * @param clienteUser            the cliente user
	 * @param cuotas                 the cuotas
	 */
	public VentaDTO(String instrumentoNumeroSerie, String clienteUser, Integer cuotas) {
		super();
		this.instrumentoNumeroSerie = instrumentoNumeroSerie;
		this.clienteUser = clienteUser;
		this.cuotas = cuotas;
		this.fecha = new Date();
	}

	/**
	 * Instantiates a new venta DTO.
	 *
	 * @param instrumentoNumeroSerie the instrumento numero serie
	 * @param clienteUser            the cliente user
	 * @param precio                 the precio
	 * @param fecha                  the fecha
	 * @param cuotas                 the cuotas
	 */
	public VentaDTO(String instrumentoNumeroSerie, String clienteUser, BigDecimal precio, Date fecha, Integer cuotas) {
		super();
		this.instrumentoNumeroSerie = instrumentoNumeroSerie;
		this.clienteUser = clienteUser;
		this.precio = precio;
		this.fecha = fecha;
		this.cuotas = cuotas;
	}

	/**
	 * Instantiates a new venta DTO.
	 *
	 * @param id                     the id
	 * @param instrumentoNumeroSerie the instrumento numero serie
	 * @param clienteUser            the cliente user
	 * @param precio                 the precio
	 * @param fecha                  the fecha
	 * @param cuotas                 the cuotas
	 */
	public VentaDTO(Integer id, String instrumentoNumeroSerie, String clienteUser, BigDecimal precio, Date fecha,
			Integer cuotas) {
		super();
		this.id = id;
		this.instrumentoNumeroSerie = instrumentoNumeroSerie;
		this.clienteUser = clienteUser;
		this.precio = precio;
		this.fecha = fecha;
		this.cuotas = cuotas;
	}

	/**
	 * Instantiates a new venta DTO.
	 *
	 * @param instrumentoNumeroSerie the instrumento numero serie
	 * @param clienteUser the cliente user
	 * @param precio the precio
	 * @param fecha the fecha
	 * @param cuotas the cuotas
	 * @param cuotaId the cuota id
	 * @param interes the interes
	 * @param descuento the descuento
	 * @param estado the estado
	 */
	public VentaDTO(String instrumentoNumeroSerie, String clienteUser, BigDecimal precio, Date fecha, Integer cuotas,
			List<String> cuotaId, Double interes, Double descuento, EstadoVenta estado) {
		super();
		this.instrumentoNumeroSerie = instrumentoNumeroSerie;
		this.clienteUser = clienteUser;
		this.precio = precio;
		this.fecha = fecha;
		this.cuotas = cuotas;
		this.cuotaId = cuotaId;
		this.interes = interes;
		this.descuento = descuento;
		this.estado = estado;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the instrumento numero serie.
	 *
	 * @return the instrumento numero serie
	 */
	public String getInstrumentoNumeroSerie() {
		return instrumentoNumeroSerie;
	}

	/**
	 * Sets the instrumento numero serie.
	 *
	 * @param instrumentoNumeroSerie the new instrumento numero serie
	 */
	public void setInstrumentoNumeroSerie(String instrumentoNumeroSerie) {
		this.instrumentoNumeroSerie = instrumentoNumeroSerie;
	}

	/**
	 * Gets the precio.
	 *
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}

	/**
	 * Sets the precio.
	 *
	 * @param precio the new precio
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * Gets the fecha.
	 *
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * Sets the fecha.
	 *
	 * @param fecha the new fecha
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * Gets the cuotas.
	 *
	 * @return the cuotas
	 */
	public Integer getCuotas() {
		return cuotas;
	}

	/**
	 * Sets the cuotas.
	 *
	 * @param cuotas the new cuotas
	 */
	public void setCuotas(Integer cuotas) {
		this.cuotas = cuotas;
	}

	/**
	 * Gets the cliente user.
	 *
	 * @return the cliente user
	 */
	public String getClienteUser() {
		return clienteUser;
	}

	/**
	 * Sets the cliente user.
	 *
	 * @param clienteUser the new cliente user
	 */
	public void setClienteUser(String clienteUser) {
		this.clienteUser = clienteUser;
	}

	/**
	 * Gets the cuota id.
	 *
	 * @return the cuotaId
	 */
	public List<String> getCuotaId() {
		return cuotaId;
	}

	/**
	 * Sets the cuota id.
	 *
	 * @param cuotaId the cuotaId to set
	 */
	public void setCuotaId(List<String> cuotaId) {
		this.cuotaId = cuotaId;
	}

	/**
	 * Gets the interes.
	 *
	 * @return the interes
	 */
	public Double getInteres() {
		return interes;
	}

	/**
	 * Sets the interes.
	 *
	 * @param interes the interes to set
	 */
	public void setInteres(Double interes) {
		this.interes = interes;
	}

	/**
	 * Gets the descuento.
	 *
	 * @return the descuento
	 */
	public Double getDescuento() {
		return descuento;
	}

	/**
	 * Sets the descuento.
	 *
	 * @param descuento the descuento to set
	 */
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public EstadoVenta getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoVenta estado) {
		this.estado = estado;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "VentaDTO [id=" + id + ", instrumentoNumeroSerie=" + instrumentoNumeroSerie + ", clienteUser="
				+ clienteUser + ", precio=" + precio + ", fecha=" + fecha + ", cantidadDeCuotas=" + cuotas
				+ ", cuotaId=" + cuotaId + ", interes=" + interes + ", descuento=" + descuento + ", estado=" + estado
				+ "]";
	}

}
