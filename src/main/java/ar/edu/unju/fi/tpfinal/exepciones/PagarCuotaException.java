package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class PagarCuotaException.
 */
public class PagarCuotaException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 74384393020L;

	/**
	 * Instantiates a new pagar cuota exception.
	 */
	public PagarCuotaException() {
		super("La couta ya se encuentra pagada");

	}
}
