package ar.edu.unju.fi.tpfinal.modelo.dto;

import java.io.Serializable;

/**
 * The Class MarcaDTO.
 *
 * @author TEAM 4
 */
public class MarcaDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The nombre. */
	private String nombre;

	/**
	 * Instantiates a new marca DTO.
	 */
	public MarcaDTO() {

	}

	/**
	 * Instantiates a new marca DTO.
	 *
	 * @param nombre the nombre
	 */
	public MarcaDTO(String nombre) {
		super();
		this.nombre = nombre;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "MarcaDTO [id=" + id + ", nombre=" + nombre + "]";
	}

}
