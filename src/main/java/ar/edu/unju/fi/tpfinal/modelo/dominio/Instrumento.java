package ar.edu.unju.fi.tpfinal.modelo.dominio;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.edu.unju.fi.tpfinal.util.EstadoInstrumento;
import ar.edu.unju.fi.tpfinal.util.TipoInstrumento;

/**
 * The Class Instrumento.
 */
@Entity
@Table(name = "instrumentos")
public class Instrumento {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Integer id;

	/** The numero serie. */
	@Column(name = "numero_serie")
	private String numeroSerie;

	/** The descripcion. */
	@Column
	private String descripcion;

	/** The precio. */
	@Column(name = "precio_usd")
	private BigDecimal precio;

	/** The anio. */
	@Column(name = "anio_fabricacion")
	private Integer anio;

	/** The pais. */
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "pais_fabricacion_id")
	private Pais pais;

	/** The marca. */
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Marca marca;

	/** The tipo instrumento. */
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_instrumento", length = 8)
	private TipoInstrumento tipoInstrumento;

	/** The estado. */
	@Enumerated(EnumType.STRING)
	@Column(length = 10)
	private EstadoInstrumento estado;

	/**
	 * Instantiates a new instrumento.
	 */
	public Instrumento() {
	}

	/**
	 * Instantiates a new instrumento.
	 *
	 * @param numeroSerie     the numero serie
	 * @param descripcion     the descripcion
	 * @param precio          the precio
	 * @param anio            the anio
	 * @param pais            the pais
	 * @param marca           the marca
	 * @param tipoInstrumento the tipo instrumento
	 * @param estado          the estado
	 */
	public Instrumento(String numeroSerie, String descripcion, BigDecimal precio, Integer anio, Pais pais, Marca marca,
			TipoInstrumento tipoInstrumento, EstadoInstrumento estado) {
		this.numeroSerie = numeroSerie;
		this.descripcion = descripcion;
		this.precio = precio;
		this.anio = anio;
		this.pais = pais;
		this.marca = marca;
		this.tipoInstrumento = tipoInstrumento;
		this.estado = estado;
	}

	/**
	 * Instantiates a new instrumento.
	 *
	 * @param id              the id
	 * @param numeroSerie     the numero serie
	 * @param descripcion     the descripcion
	 * @param precio          the precio
	 * @param anio            the anio
	 * @param pais            the pais
	 * @param marca           the marca
	 * @param tipoInstrumento the tipo instrumento
	 * @param estado          the estado
	 */
	public Instrumento(Integer id, String numeroSerie, String descripcion, BigDecimal precio, Integer anio, Pais pais,
			Marca marca, TipoInstrumento tipoInstrumento, EstadoInstrumento estado) {
		this.id = id;
		this.numeroSerie = numeroSerie;
		this.descripcion = descripcion;
		this.precio = precio;
		this.anio = anio;
		this.pais = pais;
		this.marca = marca;
		this.tipoInstrumento = tipoInstrumento;
		this.estado = estado;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the numero serie.
	 *
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}

	/**
	 * Sets the numero serie.
	 *
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the precio.
	 *
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}

	/**
	 * Sets the precio.
	 *
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * Gets the anio.
	 *
	 * @return the anio
	 */
	public Integer getAnio() {
		return anio;
	}

	/**
	 * Sets the anio.
	 *
	 * @param anio the anio to set
	 */
	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	/**
	 * Gets the pais.
	 *
	 * @return the pais
	 */
	public Pais getPais() {
		return pais;
	}

	/**
	 * Sets the pais.
	 *
	 * @param pais the pais to set
	 */
	public void setPais(Pais pais) {
		this.pais = pais;
	}

	/**
	 * Gets the marca.
	 *
	 * @return the marca
	 */
	public Marca getMarca() {
		return marca;
	}

	/**
	 * Sets the marca.
	 *
	 * @param marca the marca to set
	 */
	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	/**
	 * Gets the tipo instrumento.
	 *
	 * @return the tipoInstrumento
	 */
	public TipoInstrumento getTipoInstrumento() {
		return tipoInstrumento;
	}

	/**
	 * Sets the tipo instrumento.
	 *
	 * @param tipoInstrumento the tipoInstrumento to set
	 */
	public void setTipoInstrumento(TipoInstrumento tipoInstrumento) {
		this.tipoInstrumento = tipoInstrumento;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public EstadoInstrumento getEstado() {
		return estado;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoInstrumento estado) {
		this.estado = estado;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Instrumento [id=" + id + ", numeroSerie=" + numeroSerie + ", descripcion=" + descripcion + ", precio="
				+ precio + ", anio=" + anio + ", pais=" + pais + ", marca=" + marca + ", tipoInstrumento="
				+ tipoInstrumento.name() + ", estado=" + estado.name() + "]";
	}

}
