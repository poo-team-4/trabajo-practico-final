package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class InformeException.
 */
public class InformeException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8643536091160961966L;

	/**
	 * Instantiates a new informe exception.
	 */
	public InformeException() {
		super("No se pudo generar el informe.");
	}

	/**
	 * Instantiates a new informe exception.
	 *
	 * @param mensaje the mensaje
	 */
	public InformeException(String mensaje) {
		super("No se pudo generar el informe: " + mensaje);
	}
}
