package ar.edu.unju.fi.tpfinal.modelo.dominio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class Contado.
 */
@Entity
@DiscriminatorValue(value = "contado")
public class Contado extends Venta {

	/** The descuento. */
	@Column(name = "descuento")
	private Double descuento;

	/**
	 * Instantiates a new contado.
	 */
	public Contado() {
		super();
	}

	/**
	 * Instantiates a new contado.
	 *
	 * @param instrumento the instrumento
	 * @param precio      the precio
	 * @param fecha       the fecha
	 * @param cliente     the cliente
	 * @param cuota       the cuota
	 * @param descuento   the descuento
	 */
	public Contado(Instrumento instrumento, BigDecimal precio, Date fecha, Usuario cliente, List<Cuota> cuota,
			Double descuento) {
		super(instrumento, precio, fecha, cliente, cuota);
		this.descuento = descuento;
	}

	/**
	 * Instantiates a new contado.
	 *
	 * @param id          the id
	 * @param instrumento the instrumento
	 * @param precio      the precio
	 * @param fecha       the fecha
	 * @param cliente     the cliente
	 * @param cuota       the cuota
	 * @param descuento   the descuento
	 */
	public Contado(Integer id, Instrumento instrumento, BigDecimal precio, Date fecha, Usuario cliente,
			List<Cuota> cuota, Double descuento) {
		super(id, instrumento, precio, fecha, cliente, cuota);
		this.descuento = descuento;
	}

	/**
	 * Gets the descuento.
	 *
	 * @return descuento
	 */
	public Double getDescuento() {
		return descuento;
	}

	/**
	 * Sets the descuento.
	 *
	 * @param descuento the descuento to set
	 */
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Contado [getDescuento()=" + getDescuento() + ", getId()=" + getId() + ", getInstrumento()="
				+ getInstrumento() + ", getPrecio()=" + getPrecio() + ", getFecha()=" + getFecha() + ", getCliente()="
				+ getCliente() + ", getCuota()=" + getCuota() + ", getEstado()=" + getEstado() + "]";
	}
}
