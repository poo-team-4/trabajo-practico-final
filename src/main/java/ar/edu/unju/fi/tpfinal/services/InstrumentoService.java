/**
 * 
 */
package ar.edu.unju.fi.tpfinal.services;

import java.util.List;

import ar.edu.unju.fi.tpfinal.modelo.dto.InstrumentoDTO;

/**
 * The Interface InstrumentoService.
 *
 * @author TEAM 4
 */
public interface InstrumentoService {

	/**
	 * Gets the.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	List<InstrumentoDTO> get() throws Exception;

	/**
	 * Gets the.
	 *
	 * @param numeroSerie the numero serie
	 * @return the instrumento DTO
	 * @throws Exception the exception
	 */
	InstrumentoDTO get(String numeroSerie) throws Exception;

	/**
	 * Insert.
	 *
	 * @param nuevoInstrumento the nuevo instrumento
	 * @return the instrumento DTO
	 * @throws Exception the exception
	 */
	InstrumentoDTO insert(InstrumentoDTO nuevoInstrumento) throws Exception;

	/**
	 * Update.
	 *
	 * @param nuevoInstrumento the nuevo instrumento
	 * @return the instrumento DTO
	 * @throws Exception the exception
	 */
	InstrumentoDTO update(InstrumentoDTO nuevoInstrumento) throws Exception;

	/**
	 * Delete.
	 *
	 * @param nuevoInstrumento the nuevo instrumento
	 * @throws Exception the exception
	 */
	void delete(InstrumentoDTO nuevoInstrumento) throws Exception;

}
