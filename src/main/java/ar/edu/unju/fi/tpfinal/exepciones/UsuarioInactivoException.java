package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class UsuarioInactivoException.
 */
public class UsuarioInactivoException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 374839202L;

	/**
	 * Instantiates a new usuario inactivo exception.
	 */
	public UsuarioInactivoException() {
		super("Contraseña incorrecta");
	}
}
