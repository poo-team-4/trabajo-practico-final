package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class CuotaDeVentaException.
 */
public class CuotaDeVentaException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6781192461808221664L;

	/**
	 * Instantiates a new cuota de venta exception.
	 */
	public CuotaDeVentaException() {
		super("No se encontraron cuotas a adeudadas.");
	}
}
