package ar.edu.unju.fi.tpfinal.modelo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpfinal.app.JPAUtil;
import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dao.CuotaDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.util.EstadoCuota;

/**
 * The Class CuotaDaoImpl.
 */
@Repository
public class CuotaDaoImpl implements CuotaDao {

	/** The log. */
	private static Logger log = Logger.getLogger(CuotaDaoImpl.class);

	/** The entity. */
	private EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	/**
	 * Gets the.
	 *
	 * @return una lista de coutas
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public List<Cuota> get() throws ObtenerException, Exception {
		log.info("Iniciando la recoleccion de una lista de cuotas.");
		List<Cuota> cuotas = new ArrayList<Cuota>();
		CriteriaQuery<Cuota> criteria = entity.getCriteriaBuilder().createQuery(Cuota.class);
		criteria.select(criteria.from(Cuota.class));

		try {
			cuotas = entity.createQuery(criteria).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}

		log.debug(cuotas);
		return cuotas;
	}

	/**
	 * Gets the.
	 *
	 * @param cuota the cuota
	 * @return una marca si existe.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Cuota get(Cuota cuota) throws ObtenerException, Exception {
		log.info("Iniciando la busqueda de una cuota.");
		Cuota cuotalEncontrada = entity.find(Cuota.class, cuota.getId());
		if (cuotalEncontrada == null) {
			throw new ObtenerException();
		}
		log.debug(cuotalEncontrada);
		return cuotalEncontrada;
	}

	/**
	 * Gets the by venta.
	 *
	 * @param idVenta the id venta
	 * @return the by venta
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	@Override
	public List<Cuota> getByVenta(Integer idVenta) throws ObtenerException, Exception {
		log.info("Iniciando la busqueda de una cuota por id de venta: " + idVenta);
		List<Cuota> cuotaEncontrada = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Cuota> criteriaQuery = criteriaBuilder.createQuery(Cuota.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Cuota.class).get("ventaId"), idVenta));
		try {
			cuotaEncontrada = entity.createQuery(criteriaQuery).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(cuotaEncontrada);
		return cuotaEncontrada;
	}

	/**
	 * Gets the cuotas adeudadas.
	 *
	 * @param venta the venta
	 * @return the cuotas adeudadas
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	@Override
	public List<Cuota> getCuotasAdeudadas(Venta venta) throws ObtenerException, Exception {
		log.info("Iniciando la busqueda de una cuota por id de venta: " + venta.getId());
		List<Cuota> cuotaEncontrada = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Cuota> criteriaQuery = criteriaBuilder.createQuery(Cuota.class);
		Root<Cuota> criteriaCuota = criteriaQuery.from(Cuota.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaCuota.get("ventaId"), venta.getId()));
		criteriaQuery.where(criteriaBuilder.equal(criteriaCuota.get("estado"), EstadoCuota.ADEUDADA));

		try {
			cuotaEncontrada = entity.createQuery(criteriaQuery).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}

		log.debug(cuotaEncontrada);
		return cuotaEncontrada;
	}

	/**
	 * Gets the.
	 *
	 * @param numeroDeCuota the numero de cuota
	 * @return una marca si existe.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	public Cuota get(Integer numeroDeCuota) throws ObtenerException, Exception {
		log.info("Iniciando la busqueda de una cuota por numero de cuota: " + numeroDeCuota);
		Cuota cuotaEncontrada = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Cuota> criteriaQuery = criteriaBuilder.createQuery(Cuota.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Cuota.class).get("numeroCuota"), numeroDeCuota));

		try {
			cuotaEncontrada = entity.createQuery(criteriaQuery).getSingleResult();

		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(cuotaEncontrada);
		return cuotaEncontrada;
	}

	/**
	 * Insert.
	 *
	 * @param cuota a agregar.
	 * @return the cuota
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	public Cuota insert(Cuota cuota) throws InsertarException, Exception {
		log.info("Aniadiendo una nueva cuota a la base de datos.");
		log.debug(cuota);
		entity.getTransaction().begin();
		try {
			entity.persist(cuota);
			entity.flush();
			log.info("Cuota agregada exitosamente.");
		} catch (Exception e) {
			log.error("No se pudo agregar la cuota a la base de datos: " + e.getMessage());
			throw new InsertarException(e.getMessage());
		}
		entity.getTransaction().commit();
		return cuota;
	}

	/**
	 * Update.
	 *
	 * @param cuota the cuota
	 * @return the cuota
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	public Cuota update(Cuota cuota) throws UpdateException, Exception {
		log.info("Actualizando una cuota.");
		log.debug(cuota);
		entity.getTransaction().begin();
		try {
			entity.merge(cuota);
			entity.flush();
			log.info("Cuota actualizada con exito.");
		} catch (Exception e) {
			log.error("No se pudo actualizar la cuota en la base de datos: " + e.getMessage());
			throw new UpdateException(e.getMessage());
		}
		entity.getTransaction().commit();
		return cuota;
	}

	/**
	 * Delete.
	 *
	 * @param cuota the cuota
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	public void delete(Cuota cuota) throws DeleteException, Exception {
		log.info("Eliminando una cuota.");
		log.debug(cuota);
		entity.getTransaction().begin();
		try {
			entity.remove(cuota);
			log.info("Cuota eliminada exitosamente.");
		} catch (Exception e) {
			log.error("No se pudo eliminar la cuota de la base de datos: " + e.getMessage());
			throw new DeleteException(e.getMessage());
		}
		entity.getTransaction().commit();
	}

}
