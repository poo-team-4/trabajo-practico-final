package ar.edu.unju.fi.tpfinal.modelo.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.tpfinal.app.JPAUtil;
import ar.edu.unju.fi.tpfinal.exepciones.CalculoAdeudadoException;
import ar.edu.unju.fi.tpfinal.exepciones.DeleteException;
import ar.edu.unju.fi.tpfinal.exepciones.InsertarException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UpdateException;
import ar.edu.unju.fi.tpfinal.modelo.dao.VentaDao;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Usuario;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.util.EstadoCuota;
import ar.edu.unju.fi.tpfinal.util.EstadoVenta;

/**
 * The Class VentaDaoImpl.
 */
@Repository
public class VentaDaoImpl implements VentaDao {

	/** The log. */
	private static Logger log = Logger.getLogger(VentaDaoImpl.class);

	/** The entity. */
	private EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	/**
	 * Gets the.
	 *
	 * @return una lista de ventas.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	@Override
	public List<Venta> get() throws ObtenerException, Exception {
		log.info("Iniciando la recolección de una lista de Ventas.");
		List<Venta> ventas = new ArrayList<Venta>();
		CriteriaQuery<Venta> criteria = (CriteriaQuery<Venta>) entity.getCriteriaBuilder().createQuery(Venta.class);
		criteria.select((Selection<? extends Venta>) criteria.from(Venta.class));
		try {
			ventas = entity.createQuery(criteria).getResultList();

		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}

		log.debug(ventas);
		return ventas;
	}

	/**
	 * Gets the.
	 *
	 * @param venta the venta
	 * @return una venta si existe.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	@Override
	public Venta get(Venta venta) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda según Venta");
		log.debug(venta);
		Venta ventaEncontrada = (Venta) entity.find(Venta.class, ((Venta) venta).getId());
		if (ventaEncontrada == null) {
			throw new ObtenerException();
		}

		log.debug(ventaEncontrada);
		return ventaEncontrada;
	}

	/**
	 * Gets the.
	 *
	 * @param idVenta the id venta
	 * @return una venta si existe.
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	@Override
	public Venta get(Integer idVenta) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de una Venta.");
		entity.clear();
		Venta ventaEncontrada = (Venta) entity.find(Venta.class, idVenta);

		if (ventaEncontrada == null) {
			throw new ObtenerException();
		}
		log.debug(ventaEncontrada);
		return ventaEncontrada;
	}

	/**
	 * Gets the.
	 *
	 * @param instrumento the instrumento
	 * @return the venta
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	@Override
	public Venta get(Instrumento instrumento) throws ObtenerException, Exception {
		Venta ventaInstrumento = null;
		log.info("Iniciando la busqueda de una Venta por su Instrumento: " + instrumento.getNumeroSerie());
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Venta> criteriaQuery = (CriteriaQuery<Venta>) criteriaBuilder.createQuery(Venta.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Venta.class).get("instrumento"), instrumento));
		try {
			ventaInstrumento = entity.createQuery(criteriaQuery).getSingleResult();
		} catch (Exception e) {
			throw new ObtenerException();
		}
		log.debug(ventaInstrumento);
		return ventaInstrumento;
	}

	/**
	 * Gets the.
	 *
	 * @param cliente the cliente
	 * @return the list
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	@Override
	public List<Venta> get(Usuario cliente) throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de una Venta por su cliente: " + cliente.getNombreApellido());
		List<Venta> ventasDelCliente = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Venta> criteriaQuery = (CriteriaQuery<Venta>) criteriaBuilder.createQuery(Venta.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Venta.class).get("cliente"), cliente));

		try {
			ventasDelCliente = entity.createQuery(criteriaQuery).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(ventasDelCliente);
		return ventasDelCliente;
	}

	/**
	 * Calcular adeudado de una venta.
	 *
	 * @param idVenta the id venta
	 * @return total de una venta.
	 * @throws CalculoAdeudadoException the calculo adeudado exception
	 * @throws Exception the exception
	 */
	public BigDecimal calcularAdeudadoDeUnaVenta(Integer idVenta) throws CalculoAdeudadoException, Exception {
		log.info("Iniciando la suma de la deuda de una venta: " + idVenta);
		BigDecimal resultado = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<BigDecimal> criteriaQuery = criteriaBuilder.createQuery(BigDecimal.class);
		Root<Cuota> criteriaCuota = criteriaQuery.from(Cuota.class);

		criteriaQuery.select(criteriaBuilder.sum(criteriaCuota.get("importe")));
		criteriaQuery.where(criteriaBuilder.equal(criteriaCuota.get("estado"), EstadoCuota.ADEUDADA));
		criteriaQuery.where(criteriaBuilder.equal(criteriaCuota.get("ventaId"), idVenta));
		final TypedQuery<BigDecimal> typedQuery = entity.createQuery(criteriaQuery);

		try {
			resultado = typedQuery.getSingleResult();
		} catch (Exception e) {
			throw new CalculoAdeudadoException();
		}
		return resultado;
	}

	/**
	 * Calcular adeudado de todas las ventas.
	 *
	 * @return total de todas las ventas.
	 * @throws CalculoAdeudadoException the calculo adeudado exception
	 * @throws Exception the exception
	 */
	@Override
	public BigDecimal calcularAdeudadoDeTodasLasVentas() throws CalculoAdeudadoException, Exception {
		log.info("Iniciando la suma de la deuda de todas las ventas.");
		BigDecimal resultado = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<BigDecimal> criteriaQuery = criteriaBuilder.createQuery(BigDecimal.class);
		Root<Cuota> criteriaCuota = criteriaQuery.from(Cuota.class);
		criteriaQuery.select(criteriaBuilder.sum(criteriaCuota.get("importe")));
		criteriaQuery.where(criteriaBuilder.equal(criteriaCuota.get("estado"), EstadoCuota.ADEUDADA));
		final TypedQuery<BigDecimal> typedQuery = entity.createQuery(criteriaQuery);

		try {
			resultado = typedQuery.getSingleResult();
		} catch (Exception e) {
			throw new CalculoAdeudadoException();
		}
		return resultado;

	}

	/**
	 * Gets the adeudadas.
	 *
	 * @return the adeudadas
	 * @throws ObtenerException the obtener exception
	 * @throws Exception the exception
	 */
	@Override
	public List<Venta> getAdeudadas() throws ObtenerException, Exception {
		log.info("Iniciando la búsqueda de las ventas adeudadas");
		List<Venta> ventasAdeudadas = null;
		CriteriaBuilder criteriaBuilder = entity.getCriteriaBuilder();
		CriteriaQuery<Venta> criteriaQuery = (CriteriaQuery<Venta>) criteriaBuilder.createQuery(Venta.class);
		criteriaQuery.where(criteriaBuilder.equal(criteriaQuery.from(Venta.class).get("estado"), EstadoVenta.ADEUDADA));
		try {
			ventasAdeudadas = entity.createQuery(criteriaQuery).getResultList();
		} catch (Exception e) {
			throw new ObtenerException(e.getMessage());
		}
		log.debug(ventasAdeudadas);
		return ventasAdeudadas;
	}

	/**
	 * Insert.
	 *
	 * @param venta the venta
	 * @return the venta
	 * @throws InsertarException the insertar exception
	 * @throws Exception the exception
	 */
	@Override
	public Venta insert(Venta venta) throws InsertarException, Exception {
		log.info("Añadiendo una nueva Venta a la base de datos.");
		log.debug(venta);
		entity.getTransaction().begin();
		try {
			entity.persist(venta);
			entity.flush();
			log.info("Venta añadida con éxito.");
		} catch (Exception e) {
			log.error("No se pudo agregar la Venta a la base de datos: " + e.getMessage());
			throw new InsertarException(e.getMessage());
		}
		entity.getTransaction().commit();
		return venta;
	}

	/**
	 * Update.
	 *
	 * @param venta the venta
	 * @return the venta
	 * @throws UpdateException the update exception
	 * @throws Exception the exception
	 */
	@Override
	public Venta update(Venta venta) throws UpdateException, Exception {
		log.info("Actualizando una venta.");
		log.debug(venta);
		entity.getTransaction().begin();
		try {
			entity.merge(venta);
			entity.flush();
			log.info("Venta actualizada con éxito.");
		} catch (Exception e) {
			log.error("No se pudo actualizar la Venta en la base de datos: " + e.getMessage());
			throw new UpdateException(e.getMessage());
		}
		entity.getTransaction().commit();
		return venta;
	}

	/**
	 * Delete.
	 *
	 * @param venta a eliminar.
	 * @throws DeleteException the delete exception
	 * @throws Exception the exception
	 */
	@Override
	public void delete(Venta venta) throws DeleteException, Exception {
		log.info("Eliminando una Venta.");
		log.debug(venta);
		entity.getTransaction().begin();
		try {
			entity.remove(
					entity.contains(venta) ? venta : entity.getReference(venta.getClass(), ((Venta) venta).getId()));
			log.info("Venta eliminada con éxito.");
		} catch (Exception e) {
			log.error("No se pudo eliminar la venta de la base de datos: " + e.getMessage());
			throw new DeleteException(e.getMessage());
		}
		entity.getTransaction().commit();

	}

}
