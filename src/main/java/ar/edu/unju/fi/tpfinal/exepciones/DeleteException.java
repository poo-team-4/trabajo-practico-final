package ar.edu.unju.fi.tpfinal.exepciones;

/**
 * The Class DeleteException.
 */
public class DeleteException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 27382959502L;

	/**
	 * Instantiates a new delete exception.
	 *
	 * @param mensaje the mensaje
	 */
	public DeleteException(String mensaje) {
		super("No se pudo eliminar de la  base de datos:" + mensaje);
	}
}
