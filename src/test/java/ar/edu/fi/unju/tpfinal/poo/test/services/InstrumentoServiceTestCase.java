/**
 * 
 */
package ar.edu.fi.unju.tpfinal.poo.test.services;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.app.AppBean;
import ar.edu.unju.fi.tpfinal.modelo.dao.InstrumentoDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.InstrumentoDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dto.InstrumentoDTO;
import ar.edu.unju.fi.tpfinal.services.InstrumentoService;
import ar.edu.unju.fi.tpfinal.services.impl.InstrumentoServiceImpl;

/**
 * The Class InstrumentoServiceTestCase.
 *
 * @author TEAM 4
 */
public class InstrumentoServiceTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(InstrumentoServiceTestCase.class);

	/** The instrumento service. */
	private InstrumentoService instrumentoService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		instrumentoService = AppBean.getBean(InstrumentoServiceImpl.class);
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			InstrumentoDao instrumento = new InstrumentoDaoImpl();
			instrumento.delete(instrumento.get("TESTASD"));
		} catch (Exception e) {
		}
		instrumentoService = null;
	}

	/**
	 * Gets the todos los instrumentos.
	 *
	 * @return the todos los instrumentos
	 */
	@Test
	public void getTodosLosInstrumentos() {
		log.info("Iniciando test Obtener todos los instrumentos");

		try {
			List<InstrumentoDTO> instrumentos = instrumentoService.get();
			log.debug(instrumentos);
			assertTrue(instrumentos.size() > 0);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Gets the instrumento por numero de serie.
	 *
	 * @return the instrumento por numero de serie
	 */
	@Test
	public void getInstrumentoPorNumeroDeSerie() {
		log.info("Iniciando test Obtener instrumento por numero de serie.");
		InstrumentoDTO instrumentoEncontrado = null;
		try {
			instrumentoEncontrado = instrumentoService.get("1111");
		} catch (Exception e) {
			fail(e.getMessage());
		}
		log.debug(instrumentoEncontrado);
		assertNotNull(instrumentoEncontrado);
	}

	/**
	 * Test insertar.
	 */
	@Test
	public void testInsertar() {
		log.info("Iniciando test de insertar instrumento");

		InstrumentoDTO instrumento = new InstrumentoDTO("TESTASD", "Super Guitarra", new BigDecimal(20000), 2015,
				"Mexico", "Fender", "Guitarra", "DISPONIBLE");
		try {
			instrumento = instrumentoService.insert(instrumento);
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		log.debug(instrumento);
	}

	/**
	 * Test update.
	 */
	@Test
	public void testUpdate() {
		log.info("Iniciando test de actualizar un instrumento");

		InstrumentoDTO instrumento = new InstrumentoDTO("TESTASD", "Super Guitarra", new BigDecimal(20000), 2015,
				"Mexico", "Fender", "Guitarra", "DISPONIBLE");
		try {
			instrumentoService.insert(instrumento);
			instrumento = instrumentoService.get(instrumento.getNumeroSerie());
			instrumento.setDescripcion("Otra Super Guitarra");
			instrumentoService.update(instrumento);
			instrumento = instrumentoService.get(instrumento.getNumeroSerie());

			assertEquals("Otra Super Guitarra", instrumento.getDescripcion());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test delete.
	 */
	@Test
	public void testDelete() {
		log.info("Iniciando test de eliminar un instrumento");

		InstrumentoDTO instrumento = new InstrumentoDTO("TESTASD", "Super Guitarra", new BigDecimal(20000), 2015,
				"Mexico", "Fender", "Guitarra", "DISPONIBLE");
		try {
			log.info("Insertando instrumento de prueba");
			instrumentoService.insert(instrumento);
			instrumento = instrumentoService.get(instrumento.getNumeroSerie());
			assertNotNull(instrumento);
			log.info("Intentando eliminar el instrumento");
			instrumentoService.delete(instrumentoService.get("TESTASD"));
		} catch (Exception e) {
			fail(e.getMessage());
		}

		InstrumentoDTO testInstrumentoBorrado = null;
		try {
			testInstrumentoBorrado = instrumentoService.get("TESTASD");
			fail("No se lanzó la exception");
		} catch (Exception e) {
			assertTrue(true);
		}
		assertNull(testInstrumentoBorrado);
	}

}
