/**
 * 
 */
package ar.edu.fi.unju.tpfinal.poo.test.dao;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.modelo.dao.impl.MarcaDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Marca;

import static org.junit.Assert.*;

/**
 * The Class MarcaDaoTestCase.
 *
 * @author TEAM 4
 */
public class MarcaDaoTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(MarcaDaoTestCase.class);

	/** The target. */
	private MarcaDaoImpl target;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo Test.");
		target = new MarcaDaoImpl();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			target.delete(target.get("MarcaDePrueba"));
		} catch (Exception e) {
		}

		log.info("Finalizando el test.");
		target = null;
	}

	/**
	 * Test agregar una marca.
	 */
	@Test
	public void testAgregarUnaMarca() {
		log.info("Iniciando el test Agregar una Marca");
		try {
			target.insert(new Marca("MarcaDePrueba"));
			Marca marcaATestear = target.get("MarcaDePrueba");
			assertNotNull(marcaATestear.getId());
		} catch (Exception e) {
			fail("Error al realizar el test Agregar una Marca: " + e.getMessage());
		}
	}

	/**
	 * Test obtener una lista de marca.
	 */
	@Test
	public void testObtenerUnaListaDeMarca() {
		log.info("Iniciando el test Obtener una Lista de Marcas");
		int sizeInicial = 0;
		try {
			sizeInicial = target.get().size();
			target.insert(new Marca("MarcaDePrueba"));
			assertEquals(target.get().size(), sizeInicial + 1);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test actualizar una marca.
	 */
	@Test
	public void testActualizarUnaMarca() {
		log.info("Iniciando el test Actualizar una Marca");
		Marca marcaAModificar;
		try {
			target.insert(new Marca("MarcaDePruebaAModificar"));
			marcaAModificar = target.get("MarcaDePruebaAModificar");
			marcaAModificar.setNombre("MarcaDePrueba");
			target.update(marcaAModificar);
			assertSame(target.get("MarcaDePrueba").getId(), marcaAModificar.getId());
		} catch (Exception e) {
			fail("Error inesperado:" + e.getMessage());
		}
	}

	/**
	 * Test eliminar una marca.
	 */
	@Test
	public void testEliminarUnaMarca() {
		log.info("Iniciando el test Eliminar una Marca");
		try {
			target.insert(new Marca("MarcaDePrueba"));
			target.delete(target.get("MarcaDePrueba"));
		} catch (Exception e) {
			fail("Error inesperado:" + e.getMessage());
		}

	}
}
