package ar.edu.fi.unju.tpfinal.poo.test.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.app.AppBean;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;
import ar.edu.unju.fi.tpfinal.services.InstrumentoService;
import ar.edu.unju.fi.tpfinal.services.VentaServicio;
import ar.edu.unju.fi.tpfinal.services.impl.InstrumentoServiceImpl;
import ar.edu.unju.fi.tpfinal.services.impl.VentaServicioImpl;

import static org.junit.Assert.*;

/**
 * The Class VentaContadoServicioTestCase.
 */
public class VentaContadoServicioTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(VentaContadoServicioTestCase.class);

	/** The venta servicio. */
	private VentaServicio ventaServicio;

	/** The instrumento service. */
	private InstrumentoService instrumentoService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo Test.");
		ventaServicio = AppBean.getBean(VentaServicioImpl.class);
		instrumentoService = AppBean.getBean(InstrumentoServiceImpl.class);
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			ventaServicio.delete(ventaServicio.get(instrumentoService.get("9999")));
		} catch (Exception e) {
			log.error("Erro al intentar borrar:");
		}
		ventaServicio = null;
	}

	/**
	 * Test insertar venta contado.
	 */
	@Test
	public void testInsertarVentaContado() {
		log.info("Iniciando test de insertar Venta");
		VentaDTO venta = new VentaDTO("9999", "admin", 3);

		try {
			venta = ventaServicio.insert(venta);
			assertNotNull(venta.getId());
		} catch (Exception e) {
			fail(e.getMessage());
		}
		log.debug(venta);
	}

	/**
	 * Test todos las ventas.
	 */
	@Test
	public void testTodosLasVentas() {
		log.info("Iniciando el test para obtener todas las ventas.");
		List<VentaDTO> ventas = null;
		VentaDTO venta = new VentaDTO("9999", "admin", 3);

		try {
			venta = ventaServicio.insert(venta);
			ventas = ventaServicio.get();
			log.debug(ventas);
			assertFalse(ventas.isEmpty());
		} catch (Exception e) {
			fail("Error inesperado:" + e.getMessage());
		}

		assertTrue(ventas.size() > 0);
	}

	/**
	 * Test calcular total adeudado de una venta.
	 */
	@Test
	public void testCalcularTotalAdeudadoDeUnaVenta() {
		log.info("Iniciando test de calcular deuda de una Venta");
		BigDecimal deudaTotal = null;

		VentaDTO venta = new VentaDTO("9999", "admin", 3);

		try {
			venta = ventaServicio.insert(venta);
			deudaTotal = ventaServicio.getDeudaDeUnaVenta(venta.getId());
			log.debug(deudaTotal);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(deudaTotal);
	}

	/**
	 * Test eliminar una venta.
	 */
	@Test
	public void testEliminarUnaVenta() {
		log.info("Iniciando el test para eliminar una venta.");
		VentaDTO venta = new VentaDTO("9999", "admin", 3);

		try {
			venta = ventaServicio.insert(venta);
		} catch (Exception e1) {
			fail(e1.getMessage());
		}
		log.debug(venta);

		try {
			ventaServicio.delete(venta);
		} catch (Exception e) {
			fail("Erro al intentar borrar:" + e.getMessage());
		}
	}

}
