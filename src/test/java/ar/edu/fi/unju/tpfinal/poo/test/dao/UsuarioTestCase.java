package ar.edu.fi.unju.tpfinal.poo.test.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.modelo.dao.RolDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.UsuarioDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.RolDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.UsuarioDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Usuario;
import ar.edu.unju.fi.tpfinal.util.EstadoUsuario;

/**
 * The Class UsuarioTestCase.
 */
public class UsuarioTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(UsuarioTestCase.class);

	/** The usuario target. */
	private UsuarioDao usuarioTarget;

	/** The rol dao. */
	private RolDao rolDao;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo Test.");
		usuarioTarget = new UsuarioDaoImpl();
		rolDao = new RolDaoImpl();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			usuarioTarget.delete(usuarioTarget.get("UsuarioPrueba"));
		} catch (Exception e) {
		}

		log.info("Finalizando el test.");
		usuarioTarget = null;
		rolDao = null;
	}

	/**
	 * Test buscar todos.
	 */
	@Test
	public void testGetAll() {
		log.info("Iniciando el test Obtener una Lista de Usuarios");
		List<Usuario> lista = null;
		try {
			lista = usuarioTarget.get();
			assertFalse(lista.isEmpty());
		} catch (Exception e) {
			fail("Error inesperado: " + e.getMessage());
		}

	}

	/**
	 * Test Buscar un Usuario por nombre.
	 */
	@Test
	public void testGet() {
		log.info("Iniciando el test: buscar un Usuario por nombre");
		try {
			Usuario usuarioBuscado = usuarioTarget.get("vendedor");
			assertNotNull(usuarioBuscado);
		} catch (Exception e) {
			fail("Error inesperado: " + e.getMessage());
		}
	}

	/**
	 * Test Insertar un Usuario.
	 */
	@Test
	public void testInsertar() {
		log.info("Iniciando el test: insertar un Usuario");
		try {
			Usuario usuarioAInstertar = new Usuario("UsuarioPrueba", "Pass1234", "Un Usuario", "asd@asd.com",
					EstadoUsuario.ACTIVO, rolDao.get("Administrador"));
			usuarioAInstertar = usuarioTarget.insert(usuarioAInstertar);
			assertNotNull(usuarioAInstertar.getId());
		} catch (Exception e) {
			fail("Error inesperado: " + e.getMessage());
		}
	}

	/**
	 * Test Eliminar un Usuario.
	 */
	@Test
	public void testEliminar() {
		log.info("Iniciando el test: Eliminar un Usuario");

		try {
			Usuario usuarioAEliminar = new Usuario("UsuarioPrueba", "Pass1234", "Un Usuario", "asd@asd.com",
					EstadoUsuario.ACTIVO, rolDao.get("Administrador"));
			usuarioTarget.insert(usuarioAEliminar);
			usuarioTarget.delete(usuarioTarget.get(usuarioAEliminar));
		} catch (Exception e) {
			fail("Error inesperado: " + e.getMessage());
		}

	}

	/**
	 * Test Modificar un usuario.
	 */
	@Test
	public void testModificar() {
		log.info("Iniciando el test: Modificar un Usuario");
		try {
			Usuario usuarioAModificar = new Usuario("userToUpdate", "Pass1234", "Un Usuario", "asd@asd.com",
					EstadoUsuario.ACTIVO, rolDao.get("Administrador"));
			usuarioTarget.insert(usuarioAModificar);

			usuarioAModificar = usuarioTarget.get("userToUpdate");
			assertNotNull(usuarioAModificar);

			usuarioAModificar.setUser("UsuarioPrueba");
			usuarioTarget.update(usuarioAModificar);

			assertNotNull(usuarioTarget.get("UsuarioPrueba"));
		} catch (Exception e) {
			fail("Error inesperado: " + e.getMessage());
		}
	}
}
