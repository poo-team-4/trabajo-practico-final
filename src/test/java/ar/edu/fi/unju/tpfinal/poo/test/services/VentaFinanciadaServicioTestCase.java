package ar.edu.fi.unju.tpfinal.poo.test.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.app.AppBean;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;
import ar.edu.unju.fi.tpfinal.services.InstrumentoService;
import ar.edu.unju.fi.tpfinal.services.VentaServicio;
import ar.edu.unju.fi.tpfinal.services.impl.InstrumentoServiceImpl;
import ar.edu.unju.fi.tpfinal.services.impl.VentaServicioImpl;

import static org.junit.Assert.*;

/**
 * The Class VentaFinanciadaServicioTestCase.
 */
public class VentaFinanciadaServicioTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(VentaFinanciadaServicioTestCase.class);

	/** The venta servicio. */
	private VentaServicio ventaServicio;
	private InstrumentoService instrumentoService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo Test.");
		ventaServicio = AppBean.getBean(VentaServicioImpl.class);
		instrumentoService = AppBean.getBean(InstrumentoServiceImpl.class);
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		log.info("Elimino cualquier rastro del" + " test en la base de datos");
		try {
			ventaServicio.delete(ventaServicio.get(instrumentoService.get("3333")));
		} catch (Exception e) {
			log.error("Erro al intentar borrar:");
		}

		ventaServicio = null;
	}

	/**
	 * Test calcular total adeudado de ventas.
	 */
	@Test
	public void testCalcularTotalAdeudadoDeVentas() {
		log.info("Iniciando test de calcular adeudado de todas las Ventas");
		VentaDTO venta = new VentaDTO("3333", "admin", 3);
		BigDecimal deudaTotal = null;
		try {
			venta = ventaServicio.insert(venta);
			deudaTotal = ventaServicio.getDeudaTotal();
			log.debug(deudaTotal);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(deudaTotal);
	}

	/**
	 * Test calcular total adeudado de una venta.
	 */
	@Test
	public void testCalcularTotalAdeudadoDeUnaVenta() {
		log.info("Iniciando test de calcular deuda de una Venta");
		VentaDTO venta = new VentaDTO("3333", "admin", 3);
		BigDecimal deudaTotal = null;

		try {
			venta = ventaServicio.insert(venta);
			deudaTotal = ventaServicio.getDeudaDeUnaVenta(venta.getId());
			log.debug(deudaTotal);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(deudaTotal);
	}

	/**
	 * Test insertar venta.
	 */
	@Test
	public void testInsertarVenta() {
		log.info("Iniciando test de insertar Venta");
		VentaDTO venta = new VentaDTO("3333", "admin", 3);
		try {
			venta = ventaServicio.insert(venta);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		log.debug(venta);
		assertTrue(true);
	}

	/**
	 * Gets the todos las ventas.
	 *
	 * @return the todos las ventas
	 */
	@Test
	public void getTodosLasVentas() {
		log.info("Iniciando el test para obtener todas las ventas.");
		List<VentaDTO> ventas = null;
		VentaDTO venta = new VentaDTO("3333", "admin", 3);

		try {
			venta = ventaServicio.insert(venta);
			ventas = ventaServicio.get();
			log.debug(ventas);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertFalse(ventas.isEmpty());
		assertTrue(ventas.size() > 0);
	}

	/**
	 * Test eliminar una venta.
	 */
	@Test
	public void testEliminarUnaVenta() {
		log.info("Iniciando el test para eliminar una venta.");

		VentaDTO venta = new VentaDTO("3333", "admin", 3);

		try {
			venta = ventaServicio.insert(venta);
		} catch (Exception e1) {
			fail(e1.getMessage());
		}
		log.debug(venta);
		assertNotNull(venta);
		try {
			ventaServicio.delete(venta);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
