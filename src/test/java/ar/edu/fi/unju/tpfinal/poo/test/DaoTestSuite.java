package ar.edu.fi.unju.tpfinal.poo.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ar.edu.fi.unju.tpfinal.poo.test.dao.ContadoDaoTestCase;
import ar.edu.fi.unju.tpfinal.poo.test.dao.CuotaDaoTestCase;
import ar.edu.fi.unju.tpfinal.poo.test.dao.FinanciadaDaoTestCase;
import ar.edu.fi.unju.tpfinal.poo.test.dao.InstrumentoTestCase;
import ar.edu.fi.unju.tpfinal.poo.test.dao.MarcaDaoTestCase;
import ar.edu.fi.unju.tpfinal.poo.test.dao.PaisDaoTestCase;
import ar.edu.fi.unju.tpfinal.poo.test.dao.RolDaoTestCase;
import ar.edu.fi.unju.tpfinal.poo.test.dao.UsuarioTestCase;

/**
 * The Class DaoTestSuite.
 */
@RunWith(Suite.class)
@SuiteClasses({ ContadoDaoTestCase.class, CuotaDaoTestCase.class, FinanciadaDaoTestCase.class,
		InstrumentoTestCase.class, MarcaDaoTestCase.class, PaisDaoTestCase.class, RolDaoTestCase.class,
		UsuarioTestCase.class })
public class DaoTestSuite {

}
