package ar.edu.fi.unju.tpfinal.poo.test.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.modelo.dao.CuotaDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.CuotaDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.util.EstadoCuota;

import static org.junit.Assert.*;

/**
 * The Class CuotaDaoTestCase.
 */
public class CuotaDaoTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(CuotaDaoTestCase.class);

	/** The target. */
	private CuotaDao target;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo test.");
		target = new CuotaDaoImpl();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			target.delete(target.get(1000001));
		} catch (Exception e) {
		}

		target = null;
	}

	/**
	 * Test obtener una lista de cuotas.
	 */
	@Test
	public void testObtenerUnaListaDeCuotas() {
		log.info("Iniciando Test para obtener un listado de cuotas.");
		List<Cuota> cuotas = null;
		try {
			cuotas = target.get();
			log.debug(cuotas);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertNotNull(cuotas);
	}

	/**
	 * Test obtener una cuota por numero de cuota.
	 */
	@Test
	public void testObtenerUnaCuotaPorNumeroDeCuota() {
		log.info("Iniciando el test para modificar una cuota");
		Cuota unaCuota = null;
		try {
			target.insert(new Cuota(1000001, new BigDecimal(1000), new Date(), new Date(), EstadoCuota.ADEUDADA));
			unaCuota = target.get(1000001);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertNotNull(unaCuota);
	}

	/**
	 * Test insertar una cuota.
	 */
	@Test
	public void testInsertarUnaCuota() {
		log.info("Iniciando Test para insertar una cuota");
		Cuota unaCuota = null;
		try {
			target.insert(new Cuota(1000001, new BigDecimal(1000), new Date(), new Date(), EstadoCuota.ADEUDADA));
			unaCuota = target.get(1000001);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(unaCuota);
		assertTrue(unaCuota.getNumeroCuota() == 1000001);
	}

	/**
	 * Test modificar una cuota.
	 */
	@Test
	public void testModificarUnaCuota() {
		log.info("Iniciando el test para modificar una cuota");
		Cuota unaCuota = null;
		try {
			target.insert(new Cuota(1000001, new BigDecimal(1000), new Date(), new Date(), EstadoCuota.ADEUDADA));
			unaCuota = target.get(1000001);

			assertNotNull(unaCuota);

			unaCuota.setEstado(EstadoCuota.PAGADA);
			target.update(unaCuota);
			assertSame(target.get(1000001).getEstado(), EstadoCuota.PAGADA);

		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test eliminar una cuota.
	 */
	@Test
	public void testEliminarUnaCuota() {
		log.info("Iniciando el test para modificar una cuota");
		Cuota unaCuota = null;
		try {
			target.insert(new Cuota(1000001, new BigDecimal(1000), new Date(), new Date(), EstadoCuota.ADEUDADA));
			unaCuota = target.get(1000001);
			assertNotNull(unaCuota);
			target.delete(unaCuota);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		try {
			unaCuota = target.get(1000001);
			fail("No se lanzó la exception");
		} catch (ObtenerException e) {
			log.info(e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
