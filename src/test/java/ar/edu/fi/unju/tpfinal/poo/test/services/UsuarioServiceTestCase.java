package ar.edu.fi.unju.tpfinal.poo.test.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.app.AppBean;
import ar.edu.unju.fi.tpfinal.exepciones.LoginPasswordException;
import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.exepciones.UsuarioInactivoException;
import ar.edu.unju.fi.tpfinal.exepciones.UsuarioNoEncontradoException;
import ar.edu.unju.fi.tpfinal.modelo.dao.UsuarioDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.UsuarioDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dto.UsuarioCredencialDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.UsuarioDTO;
import ar.edu.unju.fi.tpfinal.services.UsuarioService;
import ar.edu.unju.fi.tpfinal.services.impl.UsuarioServiceImpl;

import static org.junit.Assert.*;

/**
 * The Class UsuarioServiceTestCase.
 */
public class UsuarioServiceTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(UsuarioServiceTestCase.class);

	/** The user service. */
	private UsuarioService userService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		userService = AppBean.getBean(UsuarioServiceImpl.class);
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			UsuarioDao usuarioDao = new UsuarioDaoImpl();
			usuarioDao.delete(usuarioDao.get("NombreDeUsuario"));
		} catch (Exception e) {
		}
		userService = null;
	}

	/**
	 * Test obtener todos los usuarios.
	 */
	@Test
	public void testObtenerTodosLosUsuarios() {
		log.info("Iniciando el test para obtener todos los usuarios.");
		List<UsuarioDTO> usuarios = null;

		try {
			usuarios = userService.get();
			log.debug(usuarios);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertTrue(usuarios.size() > 0);
	}

	/**
	 * Test obtener usuario por nombre de usuario.
	 */
	@Test
	public void testObtenerUsuarioPorNombreDeUsuario() {
		log.info("Iniciando el test para obtener un usuario según nombre de usuario");
		UsuarioDTO usuarioEncontrado = null;

		try {
			usuarioEncontrado = userService.get("vendedor");
			log.debug(usuarioEncontrado);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertEquals("vendedor", usuarioEncontrado.getUser());
	}

	/**
	 * Test insertar un usuario.
	 */
	@Test
	public void testInsertarUnUsuario() {
		log.info("Iniciando el test para agregar un usuario");
		UsuarioDTO nuevoUsuario = new UsuarioDTO("NombreDeUsuario", "Contraseña", "NombreYApellido", "un@email.com",
				"ACTIVO", "Vendedor");
		UsuarioDTO usuarioEncontrado = null;
		try {
			userService.insert(nuevoUsuario);
			usuarioEncontrado = userService.get("NombreDeUsuario");
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(usuarioEncontrado);
	}

	/**
	 * Test modificar un usuario.
	 */
	@Test
	public void testModificarUnUsuario() {
		log.info("Iniciando el test para modificar un usuario.");

		UsuarioDTO nuevoUsuario = new UsuarioDTO("NombreDeUsuario", "Contraseña", "NombreYApellido", "un@email.com",
				"ACTIVO", "Vendedor");
		UsuarioDTO usuarioAModificar = null;
		try {
			usuarioAModificar = userService.insert(nuevoUsuario);
			usuarioAModificar.setEmail("otro@email.com");
			userService.update(usuarioAModificar);

			usuarioAModificar = userService.get("NombreDeUsuario");
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}

		assertNotNull(usuarioAModificar);
		assertEquals("otro@email.com", usuarioAModificar.getEmail());
	}

	/**
	 * Test eliminar un usuario.
	 */
	@Test
	public void testEliminarUnUsuario() {
		log.info("Iniciando el test para eliminar un usuario.");
		UsuarioDTO nuevoUsuario = new UsuarioDTO("NombreDeUsuario", "Contraseña", "NombreYApellido", "un@email.com",
				"ACTIVO", "Vendedor");
		UsuarioDTO usuarioEncontrado = null;
		try {
			userService.insert(nuevoUsuario);
			usuarioEncontrado = userService.get("NombreDeUsuario");
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(usuarioEncontrado);

		try {
			userService.delete(usuarioEncontrado);
			usuarioEncontrado = userService.get("NombreDeUsuario");
			fail("No se lanzó la exception");
		} catch (ObtenerException e) {
			log.info(e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test login de un usuario.
	 */
	@Test
	public void testLoginDeUnUsuario() {
		log.info("Iniciando el test para verificar credenciales de un usuario");

		UsuarioCredencialDTO credencial = new UsuarioCredencialDTO("admin", "123456");
		UsuarioDTO userDTO = null;

		try {
			userDTO = userService.login(credencial);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(userDTO);
		assertEquals("admin", userDTO.getUser());
	}

	/**
	 * Test login de un usuario inactivo.
	 */
	@Test
	public void testLoginDeUnUsuarioInactivo() {
		log.info("Iniciando el test para verificar credenciales de un usuario con usuario Inactivo");
		UsuarioDTO nuevoUsuario = new UsuarioDTO("NombreDeUsuario", "Contraseña", "NombreYApellido", "un@email.com",
				"INACTIVO", "Vendedor");

		UsuarioCredencialDTO credencial = new UsuarioCredencialDTO("NombreDeUsuario", "Contraseña");
		UsuarioDTO userDTO = null;

		try {
			userService.insert(nuevoUsuario);
			userDTO = userService.login(credencial);
			fail("No se lanzó la Exception");
		} catch (UsuarioInactivoException e) {
			log.info(e.getMessage());
			assertTrue(true);
		} catch (Exception e) {
			fail("Error inesperado:" + e.getMessage());
		}

		assertNull(userDTO);
	}

	/**
	 * Test login de un usuario contrasenia incorrecta.
	 */
	@Test
	public void testLoginDeUnUsuarioContraseniaIncorrecta() {
		log.info("Iniciando el test para verificar credenciales de un usuario con contraseña incorrecta");

		UsuarioCredencialDTO credencial = new UsuarioCredencialDTO("admin", "ContraseñaIncorrecta");
		UsuarioDTO userDTO = null;

		try {
			userDTO = userService.login(credencial);
			fail("No se lanzó la exception");
		} catch (LoginPasswordException e) {
			log.info(e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNull(userDTO);
	}

	/**
	 * Test login de un usuario incorrecto.
	 */
	@Test
	public void testLoginDeBloquearUnUsuario() {
		log.info("Iniciando el test para verificar el bloqueo de un usuario");
		UsuarioCredencialDTO credencial = new UsuarioCredencialDTO("NombreDeUsuario", "ContraseñaIncorrecta");
		UsuarioDTO userDTO = null;
		try {
			userService.insert(new UsuarioDTO("NombreDeUsuario", "Contraseña", "NombreYApellido", "un@email.com",
					"ACTIVO", "Vendedor"));
		} catch (Exception e) {
			fail(e.getMessage());
		}

		for (int i = 0; i <= 3; i++) {
			try {
				userDTO = userService.login(credencial);
				fail("No se lanzó la exception");
			} catch (LoginPasswordException e) {
				log.info(e.getMessage());
			} catch (UsuarioInactivoException e) {
				fail("Se lanzó la exception antes del 3er intento");
			} catch (Exception e) {
				fail(e.getMessage());
			}
		}

		try {
			userDTO = userService.login(credencial);
			fail("No se lanzó la exception");
		} catch (UsuarioInactivoException e) {
			log.info(e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertNull(userDTO);
	}

	/**
	 * Test login de un usuario incorrecto.
	 */
	@Test
	public void testLoginDeUnUsuarioIncorrecto() {
		log.info("Iniciando el test para verificar credenciales de un usuario con usuario inexistente");

		UsuarioCredencialDTO credencial = new UsuarioCredencialDTO("UsuarioNoExiste", "123456");
		UsuarioDTO userDTO = null;

		try {
			userDTO = userService.login(credencial);
			fail("No se lanzó la exception");
		} catch (UsuarioNoEncontradoException e) {
			log.info(e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNull(userDTO);
	}
}
