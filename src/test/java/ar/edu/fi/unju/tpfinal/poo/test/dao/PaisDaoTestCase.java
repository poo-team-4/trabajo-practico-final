/**
 * 
 */
package ar.edu.fi.unju.tpfinal.poo.test.dao;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.modelo.dao.impl.PaisDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Pais;

import static org.junit.Assert.*;

/**
 * The Class PaisDaoTestCase.
 *
 * @author TEAM 4
 */
public class PaisDaoTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(PaisDaoTestCase.class);

	/** The target. */
	private PaisDaoImpl target;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo Test.");
		target = new PaisDaoImpl();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			target.delete(target.get("PaisDePrueba"));
		} catch (Exception e) {
		}

		target = null;
	}

	/**
	 * Test agregar un pais.
	 */
	@Test
	public void testAgregarUnPais() {
		log.info("Iniciando el test Agregar un País");
		try {
			target.insert(new Pais("PaisDePrueba"));
			Pais paisATestear = target.get("PaisDePrueba");
			assertNotNull(paisATestear.getId());
		} catch (Exception e) {
			fail("Error al realizar el test Agregar un País: " + e.getMessage());
		}
	}

	/**
	 * Test obtener una lista de paises.
	 */
	@Test
	public void testObtenerUnaListaDePaises() {
		log.info("Iniciando el test Obtener una Lista de Países");
		int sizeInicial = 0;
		try {
			sizeInicial = target.get().size();
			target.insert(new Pais("PaisDePrueba"));
			assertEquals(target.get().size(), sizeInicial + 1);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test actualizar un pais.
	 */
	@Test
	public void testActualizarUnPais() {
		log.info("Iniciando el test Actualizar un País");
		Pais paisAModificar;
		try {
			target.insert(new Pais("PaisDePruebaAModificar"));
			paisAModificar = target.get("PaisDePruebaAModificar");
			paisAModificar.setNombre("PaisDePrueba");
			target.update(paisAModificar);
			assertSame(target.get("PaisDePrueba").getId(), paisAModificar.getId());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test eliminar un pais.
	 */
	@Test
	public void testEliminarUnPais() {
		log.info("Iniciando el test Eliminar un País");
		try {
			target.insert(new Pais("PaisDePrueba"));
			target.delete(target.get("PaisDePrueba"));
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}
}
