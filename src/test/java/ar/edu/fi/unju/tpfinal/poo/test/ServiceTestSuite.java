package ar.edu.fi.unju.tpfinal.poo.test;

import ar.edu.fi.unju.tpfinal.poo.test.services.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * The Class ServiceTestSuite.
 */
@RunWith(Suite.class)
@SuiteClasses({ CuotaServiceTestCase.class, InstrumentoServiceTestCase.class, UsuarioServiceTestCase.class,
		VentaContadoServicioTestCase.class, VentaFinanciadaServicioTestCase.class })
public class ServiceTestSuite {

}
