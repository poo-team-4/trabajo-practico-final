package ar.edu.fi.unju.tpfinal.poo.test.dao;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.modelo.dao.InstrumentoDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.MarcaDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.PaisDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.InstrumentoDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.MarcaDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.PaisDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;
import ar.edu.unju.fi.tpfinal.util.EstadoInstrumento;
import ar.edu.unju.fi.tpfinal.util.TipoInstrumento;

import static org.junit.Assert.*;

/**
 * The Class InstrumentoTestCase.
 */
public class InstrumentoTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(InstrumentoTestCase.class);

	/** The instrumento target. */
	private InstrumentoDao instrumentoTarget;

	/** The marca target. */
	private MarcaDao marcaTarget;

	/** The pais target. */
	private PaisDao paisTarget;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo Test.");
		instrumentoTarget = new InstrumentoDaoImpl();
		marcaTarget = new MarcaDaoImpl();
		paisTarget = new PaisDaoImpl();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			instrumentoTarget.delete(instrumentoTarget.get("ASD000001"));
		} catch (Exception e) {
		}

		log.info("Finalizando el test.");
		instrumentoTarget = null;
		marcaTarget = null;
		paisTarget = null;
	}

	/**
	 * Test buscar todos.
	 */
	@Test
	public void testGetAll() {
		log.info("Iniciando el test Obtener una Lista de Instrumentos");
		List<Instrumento> lista = null;
		try {
			lista = instrumentoTarget.get();
			assertFalse(lista.isEmpty());
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}

	/**
	 * Test Buscar un Instrumento por numero de serie.
	 */
	@Test
	public void testGet() {
		log.info("Iniciando el test: buscar un Instrumento por numero de serie");
		try {
			Instrumento instrumentoBuscado = instrumentoTarget.get("1111");
			assertNotNull(instrumentoBuscado);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test Insertar un Instrumento.
	 */
	@Test
	public void testInsertar() {
		log.info("Iniciando el test: insertar un Instrumento");
		try {
			Instrumento instrumento = new Instrumento("ASD000001", "Una super Guitarra", new BigDecimal(1800), 2017,
					paisTarget.get("Mexico"), marcaTarget.get("Gibson"), TipoInstrumento.Guitarra,
					EstadoInstrumento.DISPONIBLE);
			instrumento = instrumentoTarget.insert(instrumento);
			assertNotNull(instrumento);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test Eliminar un instrumento.
	 */
	@Test
	public void testEliminar() {
		log.info("Iniciando el test: Eliminar un Instrumento");

		try {
			Instrumento instrumento = new Instrumento("ASD000001", "Una super Guitarra", new BigDecimal(1800), 2017,
					paisTarget.get("Mexico"), marcaTarget.get("Gibson"), TipoInstrumento.Guitarra,
					EstadoInstrumento.DISPONIBLE);
			instrumentoTarget.insert(instrumento);
			instrumentoTarget.delete(instrumentoTarget.get(instrumento));
		} catch (Exception e) {
			fail(e.getMessage());
		}

	}

	/**
	 * Test Modificar un instrumento.
	 */
	@Test
	public void testModificar() {
		log.info("Iniciando el test: Modificar un Instrumento");
		try {
			Instrumento instrumento = new Instrumento("ASD000001", "Guitarra", new BigDecimal(1800), 2017,
					paisTarget.get("Mexico"), marcaTarget.get("Gibson"), TipoInstrumento.Guitarra,
					EstadoInstrumento.DISPONIBLE);
			instrumentoTarget.insert(instrumento);
			instrumento = instrumentoTarget.get("ASD000001");
			assertNotNull(instrumento);
			instrumento.setTipoInstrumento(TipoInstrumento.Bajo);
			instrumento = instrumentoTarget.update(instrumento);
			assertEquals(instrumento.getTipoInstrumento(), TipoInstrumento.Bajo);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
