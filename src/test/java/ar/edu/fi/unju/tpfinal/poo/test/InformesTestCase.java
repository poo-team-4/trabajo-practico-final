package ar.edu.fi.unju.tpfinal.poo.test;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.fi.unju.tpfinal.poo.test.services.VentaContadoServicioTestCase;
import ar.edu.unju.fi.tpfinal.app.AppBean;
import ar.edu.unju.fi.tpfinal.exepciones.InformeException;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;
import ar.edu.unju.fi.tpfinal.modelo.factory.FabricaInformes;
import ar.edu.unju.fi.tpfinal.modelo.factory.impl.FabricaInformesImpl;
import ar.edu.unju.fi.tpfinal.services.CuotaService;
import ar.edu.unju.fi.tpfinal.services.InstrumentoService;
import ar.edu.unju.fi.tpfinal.services.VentaServicio;
import ar.edu.unju.fi.tpfinal.services.impl.CuotaServiceImpl;
import ar.edu.unju.fi.tpfinal.services.impl.InstrumentoServiceImpl;
import ar.edu.unju.fi.tpfinal.services.impl.VentaServicioImpl;

/**
 * The Class InformesTestCase.
 */
public class InformesTestCase {
	private static Logger log = Logger.getLogger(VentaContadoServicioTestCase.class);

	/** The informes. */
	private FabricaInformes informes;

	/** The venta servicio. */
	private VentaServicio ventaServicio;

	private InstrumentoService instrumentoService;

	/** The cuota service. */
	private CuotaService cuotaService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		informes = AppBean.getBean(FabricaInformesImpl.class);
		instrumentoService = AppBean.getBean(InstrumentoServiceImpl.class);
		ventaServicio = AppBean.getBean(VentaServicioImpl.class);
		cuotaService = AppBean.getBean(CuotaServiceImpl.class);
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			ventaServicio.delete(ventaServicio.get(instrumentoService.get("3333")));
			ventaServicio.delete(ventaServicio.get(instrumentoService.get("4444")));
			ventaServicio.delete(ventaServicio.get(instrumentoService.get("8888")));
			ventaServicio.delete(ventaServicio.get(instrumentoService.get("9999")));
		} catch (Exception e) {
		}
		informes = null;
		instrumentoService = null;
		ventaServicio = null;
		cuotaService = null;
	}

	/**
	 * Test.
	 */
	@Test
	public void testGenerarUnInformeVentaEnPDF() {
		VentaDTO venta = new VentaDTO("3333", "admin", 6);
		try {
			venta = ventaServicio.insert(venta);
			cuotaService.pagarCuota(cuotaService.obtenerProximaCuotaDeVenta(venta));
			informes.exportarVentaPDF(venta);
		} catch (InformeException e) {
			fail("Se produjo un error al generar el informe" + e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test.
	 */
	@Test
	public void testGenerarUnInformeVentaEnExcel() {
		log.info("Iniciando test de Generar un informe de venta en Exel");
		VentaDTO venta = new VentaDTO("3333", "admin", 6);
		try {
			venta = ventaServicio.insert(venta);
			cuotaService.pagarCuota(cuotaService.obtenerProximaCuotaDeVenta(venta));
			informes.exportarVentaExcel(venta.getId());
		} catch (InformeException e) {
			fail("Se produjo un error al generar el informe" + e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test.
	 */
	@Test
	public void testGenerarUnInformeDeVentasAdeudadas() {
		try {
			VentaDTO venta = ventaServicio.insert(new VentaDTO("3333", "admin", 3));
			cuotaService.pagarCuota(cuotaService.obtenerProximaCuotaDeVenta(venta));
			ventaServicio.insert(new VentaDTO("4444", "admin", 6));
			ventaServicio.insert(new VentaDTO("8888", "admin", 12));
			ventaServicio.insert(new VentaDTO("9999", "admin", 1));
			informes.exportarVentasAdeudadasEnExcel();
		} catch (InformeException e) {
			fail("Se produjo un error al generar el informe" + e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
