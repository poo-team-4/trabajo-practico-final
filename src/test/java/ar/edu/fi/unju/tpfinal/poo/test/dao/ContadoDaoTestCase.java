package ar.edu.fi.unju.tpfinal.poo.test.dao;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.modelo.dao.InstrumentoDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.UsuarioDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.VentaDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.InstrumentoDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.UsuarioDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.VentaDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Contado;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.util.EstadoCuota;

/**
 * The Class ContadoDaoTestCase.
 */
public class ContadoDaoTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(ContadoDaoTestCase.class);

	/** The venta dao. */
	@Inject
	private VentaDao ventaDao;

	/** The usuario dao. */
	@Inject
	private UsuarioDao usuarioDao;

	/** The instrumento dao. */
	@Inject
	private InstrumentoDao instrumentoDao;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo test.");
		ventaDao = new VentaDaoImpl();
		usuarioDao = new UsuarioDaoImpl();
		instrumentoDao = new InstrumentoDaoImpl();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			for (Venta venta : ventaDao.get(usuarioDao.get("admin"))) {
				ventaDao.delete(venta);
			}
		} catch (Exception e) {
		}

		ventaDao = null;
		usuarioDao = null;
		instrumentoDao = null;

	}

	/**
	 * Test obtener listado de ventas.
	 */
	@Test
	public void testObtenerListadoDeVentas() {
		log.info("Iniciando el test de obtener una venta");
		List<Venta> ventasAlContado = null;

		try {
			ventasAlContado = ventaDao.get();
			log.debug(ventasAlContado);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventasAlContado);
	}

	/**
	 * Test obtener listado de ventas por cliente.
	 */
	@Test
	public void testObtenerListadoDeVentasPorCliente() {
		log.info("Iniciando el test de obtener una lista de ventas segun usuario");
		List<Venta> ventasAlContado = null;

		try {
			ventasAlContado = ventaDao.get(usuarioDao.get("admin"));
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventasAlContado);
	}

	/**
	 * Test modificar una venta.
	 */
	@Test
	public void testModificarUnaVenta() {
		log.info("Iniciando el test Modificar una Venta.");
		List<Cuota> cuotas = new ArrayList<>();
		cuotas.add(new Cuota(10000002, new BigDecimal(2020), new Date(), new Date(), EstadoCuota.ADEUDADA));
		Venta ventaContado = null;
		Venta ventaModificado = null;
		try {
			Instrumento instrumento = instrumentoDao.get("1111");
			ventaContado = ventaDao.insert(new Contado(instrumento, instrumento.getPrecio(), new Date(),
					usuarioDao.get("admin"), cuotas, 0.5));
			log.debug(ventaContado);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventaContado);
		assertNotNull(ventaContado.getId());

		try {
			ventaContado.setPrecio(new BigDecimal(100.5));
			ventaDao.update(ventaContado);
			ventaModificado = ventaDao.get(ventaContado.getId());
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventaModificado);
		assertEquals(ventaModificado.getPrecio().toString(), "100.50");
	}

	/**
	 * Test insertar venta.
	 */
	@Test
	public void testInsertarVenta() {
		log.info("Iniciando el test Agregar una Venta.");
		List<Cuota> cuotas = new ArrayList<>();
		cuotas.add(new Cuota(10000002, new BigDecimal(2020), new Date(), new Date(), EstadoCuota.ADEUDADA));
		Venta ventaContado = null;
		try {
			Instrumento instrumento = instrumentoDao.get("1111");
			ventaContado = ventaDao.insert(new Contado(instrumento, instrumento.getPrecio(), new Date(),
					usuarioDao.get("admin"), cuotas, 0.5));
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventaContado);
		assertNotNull(ventaContado.getId());
	}

	/**
	 * Test eliminar una venta.
	 */
	@Test
	public void testEliminarUnaVenta() {
		log.info("Iniciando el test Eliminar una Venta.");
		List<Cuota> cuotas = new ArrayList<>();
		cuotas.add(new Cuota(10000002, new BigDecimal(2020), new Date(), new Date(), EstadoCuota.ADEUDADA));
		Venta ventaContado = null;
		Venta ventaEliminada = null;
		try {
			Instrumento instrumento = instrumentoDao.get("1111");
			ventaContado = ventaDao.insert(new Contado(instrumento, instrumento.getPrecio(), new Date(),
					usuarioDao.get("admin"), cuotas, 0.5));
			assertNotNull(ventaContado);
			assertNotNull(ventaContado.getId());
			ventaDao.delete(ventaContado);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		try {
			ventaEliminada = ventaDao.get(ventaContado);
			fail("No se lanzó la exception");
		} catch (ObtenerException e) {
			log.info(e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNull(ventaEliminada);
	}
}
