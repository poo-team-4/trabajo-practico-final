package ar.edu.fi.unju.tpfinal.poo.test.dao;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.modelo.dao.RolDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.RolDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Rol;

/**
 * The Class RolDaoTestCase.
 */
public class RolDaoTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(RolDaoTestCase.class);

	/** The target. */
	private RolDao target;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo test.");
		target = new RolDaoImpl();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			target.delete(target.get("RolDePrueba"));
		} catch (Exception e) {
		}

		target = null;
	}

	/**
	 * Test agregar un rol.
	 */
	@Test
	public void testAgregarUnRol() {
		log.info("Iniciando el test Agregar un Rol");
		try {
			target.insert(new Rol("RolDePrueba"));
			Rol rolATestear = target.get("RolDePrueba");
			assertNotNull(rolATestear.getId());
		} catch (Exception e) {
			fail("Error al realizar el test Agregar un Rol: " + e.getMessage());
		}
	}

	/**
	 * Test obtener una lista de roles.
	 */
	@Test
	public void testObtenerUnaListaDeRoles() {
		log.info("Iniciando el test Obtener una Lista de Roles");
		int sizeInicial = 0;
		try {
			sizeInicial = target.get().size();
			target.insert(new Rol("RolDePrueba"));
			assertEquals(target.get().size(), sizeInicial + 1);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test actualizar un rol.
	 */
	@Test
	public void testActualizarUnRol() {
		log.info("Iniciando el test Actualizar un Rol");
		Rol rolAModificar;
		try {
			target.insert(new Rol("RolDePruebaAModificar"));
			rolAModificar = target.get("RolDePruebaAModificar");
			rolAModificar.setDescripcion("RolDePrueba");
			target.update(rolAModificar);
			assertSame(target.get("RolDePrueba").getId(), rolAModificar.getId());
		} catch (Exception e) {
			fail("Error inesperado:" + e.getMessage());
		}
	}

	/**
	 * Test eliminar un rol.
	 */
	@Test
	public void testEliminarUnRol() {
		log.info("Iniciando el test Eliminar un Rol");
		try {
			target.insert(new Rol("RolDePrueba"));
			target.delete(target.get("RolDePrueba"));
		} catch (Exception e) {
			fail("Error inesperado:" + e.getMessage());
		}

	}

}
