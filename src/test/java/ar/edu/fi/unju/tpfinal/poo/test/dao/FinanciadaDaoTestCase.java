package ar.edu.fi.unju.tpfinal.poo.test.dao;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.exepciones.ObtenerException;
import ar.edu.unju.fi.tpfinal.modelo.dao.InstrumentoDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.UsuarioDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.VentaDao;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.InstrumentoDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.UsuarioDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dao.impl.VentaDaoImpl;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Financiada;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Cuota;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Instrumento;
import ar.edu.unju.fi.tpfinal.modelo.dominio.Venta;
import ar.edu.unju.fi.tpfinal.util.EstadoCuota;

/**
 * The Class FinanciadaDaoTestCase.
 */
public class FinanciadaDaoTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(FinanciadaDaoTestCase.class);

	/** The venta dao. */
	private VentaDao ventaDao;

	/** The usuario dao. */
	private UsuarioDao usuarioDao;

	/** The instrumento dao. */
	private InstrumentoDao instrumentoDao;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		log.info("Iniciando un nuevo test.");
		ventaDao = new VentaDaoImpl();
		usuarioDao = new UsuarioDaoImpl();
		instrumentoDao = new InstrumentoDaoImpl();

	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			log.info("Elimino cualquier rastro de test en la base de datos");
			for (Venta venta : ventaDao.get(usuarioDao.get("admin"))) {
				ventaDao.delete(venta);
			}
		} catch (Exception e) {
		}

		ventaDao = null;
		usuarioDao = null;
		instrumentoDao = null;

	}

	/**
	 * Test obtener listado de ventas.
	 */
	@Test
	public void testObtenerListadoDeVentas() {
		log.info("Iniciando el test de obtener una venta");
		List<Venta> ventasAlFinanciada = null;

		try {
			ventasAlFinanciada = ventaDao.get();
			log.debug(ventasAlFinanciada);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventasAlFinanciada);
	}

	/**
	 * Test obtener listado de ventas por cliente.
	 */
	@Test
	public void testObtenerListadoDeVentasPorCliente() {
		log.info("Iniciando el test de obtener una lista de ventas segun usuario");
		List<Venta> ventasAlFinanciada = null;

		try {
			ventasAlFinanciada = ventaDao.get(usuarioDao.get("admin"));
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventasAlFinanciada);
	}

	/**
	 * Test obtener listado de ventas adeudadas.
	 */
	@Test
	public void testObtenerListadoDeVentasAdeudadas() {
		log.info("Iniciando el test de obtener una lista de ventas adeudadas");
		List<Venta> ventasAlFinanciada = null;

		try {
			ventasAlFinanciada = ventaDao.getAdeudadas();
		} catch (Exception e) {
			fail(e.getMessage());
		}

		log.debug(ventasAlFinanciada);
		assertNotNull(ventasAlFinanciada);
	}

	/**
	 * Test modificar una venta.
	 */
	@Test
	public void testModificarUnaVenta() {
		log.info("Iniciando el test Modificar una Venta.");
		List<Cuota> cuotas = new ArrayList<>();
		cuotas.add(new Cuota(10000002, new BigDecimal(2020), new Date(), new Date(), EstadoCuota.ADEUDADA));
		Venta ventaFinanciada = null;
		Venta ventaModificado = null;
		try {
			Instrumento instrumento = instrumentoDao.get("1111");
			ventaFinanciada = ventaDao.insert(new Financiada(instrumento, instrumento.getPrecio(), new Date(),
					usuarioDao.get("admin"), cuotas, 0.5));
			log.debug(ventaFinanciada);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventaFinanciada);
		assertNotNull(ventaFinanciada.getId());

		try {
			ventaFinanciada.setPrecio(new BigDecimal(100.50));
			ventaDao.update(ventaFinanciada);
			ventaModificado = ventaDao.get(ventaFinanciada.getId());
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventaModificado);
		assertEquals(ventaModificado.getPrecio().toString(), "100.50");
	}

	/**
	 * Test insertar venta.
	 */
	@Test
	public void testInsertarVenta() {
		log.info("Iniciando el test Agregar una Venta.");
		List<Cuota> cuotas = new ArrayList<>();
		cuotas.add(new Cuota(10000002, new BigDecimal(2020), new Date(), new Date(), EstadoCuota.ADEUDADA));
		Venta ventaFinanciada = null;
		try {
			Instrumento instrumento = instrumentoDao.get("1111");
			ventaFinanciada = ventaDao.insert(new Financiada(instrumento, instrumento.getPrecio(), new Date(),
					usuarioDao.get("admin"), cuotas, 0.5));
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertNotNull(ventaFinanciada);
		assertNotNull(ventaFinanciada.getId());
	}

	/**
	 * Test eliminar una venta.
	 */
	@Test
	public void testEliminarUnaVenta() {
		log.info("Iniciando el test Eliminar una Venta.");
		List<Cuota> cuotas = new ArrayList<>();
		cuotas.add(new Cuota(10000002, new BigDecimal(2020), new Date(), new Date(), EstadoCuota.ADEUDADA));
		Venta ventaFinanciada = null;
		try {
			Instrumento instrumento = instrumentoDao.get("1111");
			ventaFinanciada = ventaDao.insert(new Financiada(instrumento, instrumento.getPrecio(), new Date(),
					usuarioDao.get("admin"), cuotas, 0.5));
			assertNotNull(ventaFinanciada);
			assertNotNull(ventaFinanciada.getId());
			ventaDao.delete(ventaFinanciada);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		try {
			ventaDao.get(ventaFinanciada);
			fail("No se lanzó la exception");
		} catch (ObtenerException e) {
			log.info(e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
