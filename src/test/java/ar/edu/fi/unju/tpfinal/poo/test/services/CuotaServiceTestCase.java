/**
 * 
 */
package ar.edu.fi.unju.tpfinal.poo.test.services;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.unju.fi.tpfinal.app.AppBean;
import ar.edu.unju.fi.tpfinal.exepciones.PagarCuotaException;
import ar.edu.unju.fi.tpfinal.modelo.dto.CuotaDTO;
import ar.edu.unju.fi.tpfinal.modelo.dto.VentaDTO;
import ar.edu.unju.fi.tpfinal.services.CuotaService;
import ar.edu.unju.fi.tpfinal.services.InstrumentoService;
import ar.edu.unju.fi.tpfinal.services.VentaServicio;
import ar.edu.unju.fi.tpfinal.services.impl.CuotaServiceImpl;
import ar.edu.unju.fi.tpfinal.services.impl.InstrumentoServiceImpl;
import ar.edu.unju.fi.tpfinal.services.impl.VentaServicioImpl;
import ar.edu.unju.fi.tpfinal.util.EstadoCuota;
import ar.edu.unju.fi.tpfinal.util.EstadoVenta;

/**
 * The Class CuotaServiceTestCase.
 *
 * @author TEAM 4
 */
public class CuotaServiceTestCase {

	/** The log. */
	private static Logger log = Logger.getLogger(UsuarioServiceTestCase.class);

	/** The cuota service. */
	private CuotaService cuotaService;

	/** The venta servicio. */
	private VentaServicio ventaServicio;

	private InstrumentoService instrumentoService;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		cuotaService = AppBean.getBean(CuotaServiceImpl.class);
		ventaServicio = AppBean.getBean(VentaServicioImpl.class);
		instrumentoService = AppBean.getBean(InstrumentoServiceImpl.class);
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			ventaServicio.delete(ventaServicio.get(instrumentoService.get("5555")));
		} catch (Exception e) {
			log.error("Erro al intentar borrar:");
		}
		cuotaService = null;
		ventaServicio = null;
	}

	/**
	 * Test obtener proxima cuota de venta.
	 */
	@Test
	public void testObtenerProximaCuotaDeVenta() {
		log.info("Iniciando test de Proxima cuota de Venta");
		CuotaDTO cuotaAPagar = null;
		VentaDTO venta = new VentaDTO("5555", "admin", 3);

		try {
			venta = ventaServicio.insert(venta);
			cuotaAPagar = cuotaService.obtenerProximaCuotaDeVenta(venta);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		log.debug(cuotaAPagar);
		assertNotNull(cuotaAPagar);
	}

	/**
	 * Test pagar una cuota.
	 */
	@Test
	public void testPagarUnaCuota() {
		log.info("Iniciando test de Pagar una cuota");
		CuotaDTO cuotaAPagar = null;
		VentaDTO venta = new VentaDTO("5555", "admin", 3);
		try {
			venta = ventaServicio.insert(venta);
			cuotaAPagar = cuotaService.obtenerProximaCuotaDeVenta(venta);
			assertNotNull(cuotaAPagar);
			cuotaAPagar = cuotaService.pagarCuota(cuotaAPagar);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		log.debug(cuotaAPagar);

		assertNotNull(cuotaAPagar);
		assertEquals(cuotaAPagar.getEstado(), EstadoCuota.PAGADA);
	}

	@Test
	public void testCambioDeEstadoVenta() {
		log.info("Iniciando test de Cambio de estado de venta");
		CuotaDTO cuotaAPagar = null;
		VentaDTO venta = new VentaDTO("5555", "admin", 3);

		try {
			venta = ventaServicio.insert(venta);
			assertEquals(venta.getEstado(), EstadoVenta.ADEUDADA);
			for (int i = 0; i < 3; i++) {
				cuotaAPagar = cuotaService.obtenerProximaCuotaDeVenta(venta);
				assertNotNull(cuotaAPagar);
				cuotaAPagar = cuotaService.pagarCuota(cuotaAPagar);
				venta = ventaServicio.get(venta.getId());
			}
		} catch (Exception e) {
			fail(e.getMessage());
		}

		log.debug(venta);
		assertNotNull(venta);
		assertEquals(venta.getEstado(), EstadoVenta.FINALIZADA);
	}

	/**
	 * Test pagar una cuota ya pagada.
	 */
	@Test
	public void testPagarUnaCuotaYaPagada() {
		log.info("Iniciando test de Pagar una cuota ya pagada");
		CuotaDTO cuotaAPagar = null;
		VentaDTO venta = new VentaDTO("5555", "admin", 3);
		try {
			venta = ventaServicio.insert(venta);
			cuotaAPagar = cuotaService.obtenerProximaCuotaDeVenta(venta);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertNotNull(cuotaAPagar);
		cuotaAPagar.setEstado(EstadoCuota.PAGADA);

		try {
			cuotaAPagar = cuotaService.pagarCuota(cuotaAPagar);
			fail("No se lanzó el exception");
		} catch (PagarCuotaException e) {
			assertTrue(true);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
